const request = require("supertest");
const app = require("../../index");

//
// [Ankit: 10/11/2019, 06:04pm]
// [Modified - Ankit: 10/11/2019, 06:04pm]
describe("test for Stocks indices, gainers, losers, sectors and industry", () => {
  it("should retrieve all indices information", done => {
    request(app)
      .get("/api/stocks/indices")
      .then(res => {
        expect(res.statusCode).toBe(200);
        expect(res.body).toEqual(expect.any(Object));
        expect(res.body.message).toBe("All indices retrieved successfully");
        done();
      });
  });
  // [Shweta: 13/11/2019, 11:07pm]
  // [Modified - Shweta: 13/11/2019, 04:05pm]
  it("should retrieve all gainer's and loser's overview information", done => {
    request(app)
      .get("/api/stocks/overview")
      .then(res => {
        expect(res.statusCode).toBe(200);
        expect(res.body).toEqual(expect.any(Object));
        expect(res.body.message).toBe(
          "Returned top 10 gainers and losers overview details successfully"
        );
        done();
      });
  });
  it("should retrieve all gainer's and loser's performance information", done => {
    request(app)
      .get("/api/stocks/performance")
      .then(res => {
        expect(res.statusCode).toBe(200);
        expect(res.body).toEqual(expect.any(Object));
        expect(res.body.message).toBe(
          "Returned top 10 gainers and losers performance details successfully"
        );
        done();
      });
  });
  it("should retrieve all gainer's valuation information", done => {
    request(app)
      .post("/api/stocks/gainersvaluation")
      .then(res => {
        expect(res.statusCode).toBe(200);
        expect(res.body).toEqual(expect.any(Object));
        expect(res.body.message).toBe(
          "Returned gainers valuation details successfully"
        );
        done();
      });
  });
  it("should retrieve all gainer's margin information", done => {
    request(app)
      .post("/api/stocks/gainersmargin")
      .then(res => {
        expect(res.statusCode).toBe(200);
        expect(res.body).toEqual(expect.any(Object));
        expect(res.body.message).toBe(
          "Returned gainers margin details successfully"
        );
        done();
      });
  });
  it("should retrieve all loser's valuation information", done => {
    request(app)
      .post("/api/stocks/losersvaluation")
      .then(res => {
        expect(res.statusCode).toBe(200);
        expect(res.body).toEqual(expect.any(Object));
        expect(res.body.message).toBe(
          "Returned losers valuation details successfully"
        );
        done();
      });
  });
  it("should retrieve all loser's margin information", done => {
    request(app)
      .post("/api/stocks/losersmargin")
      .then(res => {
        expect(res.statusCode).toBe(200);
        expect(res.body).toEqual(expect.any(Object));
        expect(res.body.message).toBe(
          "Returned losers margin details successfully"
        );
        done();
      });
  });
  it("should retrieve all sectors information", done => {
    request(app)
      .get("/api/stocks/sectors")
      .then(res => {
        expect(res.statusCode).toBe(200);
        expect(res.body).toEqual(expect.any(Object));
        expect(res.body.message).toBe("Returned sectors details successfully");
        done();
      });
  });
  it("should retrieve all industry information", done => {
    request(app)
      .get("/api/stocks/industry")
      .then(res => {
        expect(res.statusCode).toBe(200);
        expect(res.body).toEqual(expect.any(Object));
        expect(res.body.message).toBe(
          "Returned industries details successfully"
        );
        done();
      });
  });
  it("should retrieve all sectors performance information", done => {
    request(app)
      .get("/api/stocks/sectorperformance")
      .then(res => {
        expect(res.statusCode).toBe(200);
        expect(res.body).toEqual(expect.any(Object));
        expect(res.body.message).toBe(
          "Returned sectors' performance details successfully"
        );
        done();
      });
  });
  it("should retrieve all industry's performance information", done => {
    request(app)
      .get("/api/stocks/industryperformance")
      .then(res => {
        expect(res.statusCode).toBe(200);
        expect(res.body).toEqual(expect.any(Object));
        expect(res.body.message).toBe(
          "Returned industries performance details successfully"
        );
        done();
      });
  });
});
