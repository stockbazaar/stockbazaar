const config = require("config");
const mongoURL = config.get("mongoURL");
const mongoose = require("mongoose");
mongoose.connect(mongoURL, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true
});

var db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.on("connected", function() {
  console.log("connected to data server");
});

const request = require("supertest");

const app = require("../../index");

describe("Testing Register APIs", () => {
  it("should Retrieve  all user information", done => {
    request(app)
      .get("/api/user/all")
      .then(response => {
        expect(response.statusCode).toBe(200);
        expect(response.body).toEqual(expect.any(Object));
        expect(response.body.message).toBe(
          "Retrieved all user information successfully"
        );
        done();
      });
  });
});
