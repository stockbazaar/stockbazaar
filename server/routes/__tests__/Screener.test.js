const request = require("supertest");
const app = require("../../index");

describe("Testing for fetching all the sector data", () => {
  it("checks for the positive call to get and object with status code 200 and a message for route getIndustriesBySector", done => {
    request(app)
      .get("/api/screener/getIndustriesBySector")
      .then(res => {
        expect(res.statusCode).toBe(200);
        expect(res.body).toEqual(expect.any(Object));
        expect(res.body.data).toEqual(expect.any(Object));
        expect(res.body.data[0].sector).toEqual(expect.any(Object));
        expect(res.body.data[0].sector.name).toEqual(expect.any(String));
        expect(res.body.data[0].sector.isChecked).toEqual(expect.any(Boolean));
        expect(res.body.data[0].industries).toEqual(expect.any(Array));
        expect(res.body.message).toBe(
          "Sectors and industries data retrieved successfully"
        );
        done();
      });
  });
});

// Test for api getTickersByFilter which gets the tickers by filtering sectors
// and industries

describe("Testing for fetching all the sector data", () => {
  // test for, if no body data is present from request
  it("gets a 400 status code and an error message if body is empty from request", done => {
    let data = {};
    let payload = JSON.stringify(data);
    request(app)
      .post("/api/screener/getTickersByFilter")
      .send(payload)
      .set("Content-type", "application/json")
      .then(res => {
        expect(res.statusCode).toEqual(400);
        expect(res.text).toBe("No filters to fetch stocks details");
        done();
      });
  });

  // test for, if there are no filter and returns all the tickers
  // it("gets a 200 status code and a message if there are no filter and returns all the tickers", done => {
  //   let data = {
  //     sectors: [],
  //     industries: []
  //   };
  //   let payload = JSON.stringify(data);
  //   request(app)
  //     .post("/api/screener/getTickersByFilter")
  //     .send(payload)
  //     .set("Content-type", "application/json")
  //     .then(res => {
  //       expect(res.statusCode).toEqual(200);
  //       expect(res.body.data).toEqual(expect.any(Array));
  //       expect(res.body.data[0].sector).toEqual(expect.any(String));
  //       expect(res.body.data[0].industry).toEqual(expect.any(String));
  //       expect(res.body.data[0].ticker_data).toEqual(expect.any(Object));
  //       expect(res.body.data[0].ticker_data.ticker_name).toEqual(
  //         expect.any(String)
  //       );
  //       expect(res.body.data[0].ticker_data.secondLast).toEqual(
  //         expect.any(Object)
  //       );
  //       expect(res.body.data[0].ticker_data.last).toEqual(expect.any(Object));
  //       expect(res.body.message).toBe(
  //         "Retrieved companies details successfully for no filter"
  //       );
  //       done();
  //     });
  // });

  // test for, if there are filter and returns the tickers according to filter
  it("gets a 200 status code and a message if there are filters and returns the tickers accordingly", done => {
    let data = {
      sectors: ["Technology"],
      industries: []
    };
    let payload = JSON.stringify(data);
    request(app)
      .post("/api/screener/getTickersByFilter")
      .send(payload)
      .set("Content-type", "application/json")
      .then(res => {
        expect(res.statusCode).toEqual(200);
        expect(res.body.data).toEqual(expect.any(Array));
        expect(res.body.data[0].sector).toEqual(expect.any(String));
        expect(res.body.data[0].industry).toEqual(expect.any(String));
        expect(res.body.data[0].ticker_data).toEqual(expect.any(Object));
        expect(res.body.data[0].ticker_data.ticker_name).toEqual(
          expect.any(String)
        );
        expect(res.body.data[0].ticker_data.secondLast).toEqual(
          expect.any(Object)
        );
        expect(res.body.data[0].ticker_data.last).toEqual(expect.any(Object));
        expect(res.body.message).toBe(
          "Retrieved companies details successfully with filter"
        );
        done();
      });
  });
});
