const request = require("supertest");

const app = require("../../index");

describe("Testing upload APIs", () => {
  it("should upload a file ", done => {
    request(app)
      .send()
      .post("/api/admin/upload")
      .then(res => {
        expect(res.statusCode).toBe(200);
        expect(res.body).toEqual(expect.any(Object));
        expect(res.body.message).toBe("File uploaded sucessfully");
        done();
      });
  });
  it("should Retrieve  all user information", done => {
    request(app)
      .send()
      .post("/api/admin/upload")
      .then(res => {
        expect(res.statusCode).toBe(500);
        expect(res.body).toEqual(expect.any(Object));
        expect(res.body.message).toBe(
          "File upload failed due to some internal server error. Please try again"
        );
        done();
      });
  });
  it("should Retrieve  all user information", done => {
    request(app)
      .get("/api/admin/download/dummydata")
      .then(res => {
        expect(res.statusCode).toBe(200);
        expect(res.body).toEqual(expect.any(Object));
        expect(res.body.message).toBe("Download successful");
        done();
      });
  });
});
