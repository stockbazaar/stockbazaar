const request = require("supertest");

const app = require("../../index");
let id = "";
describe("Testing Register APIs", () => {
  it("should Retrieve  all user information", done => {
    request(app)
      .get("/api/user/all")
      .then(res => {
        expect(res.statusCode).toBe(200);
        expect(res.body).toEqual(expect.any(Object));
        expect(res.body.message).toBe(
          "Retrieved all user information successfully"
        );
        done();
      });
  });

  it("should create user", done => {
    let data = {
      name: "test",
      password: "123456",
      email: "test@test.com"
    };
    let payload = JSON.stringify(data);

    request(app)
      .post("/api/user/new")
      .send(payload)
      .set("Content-type", "application/json")
      .then(res => {
        id = res.body.data._id;
        expect(res.statusCode).toBe(200);
        expect(res.body).toEqual(expect.any(Object));
        expect(res.body.message).toBe("Created 1 user successfully");
        done();
      });
  });
  it("should not allow to create user with registered email", done => {
    let data = {
      name: "test",
      password: "123456",
      email: "test@test.com"
    };
    let payload = JSON.stringify(data);

    request(app)
      .post("/api/user/new")
      .send(payload)
      .set("Content-type", "application/json")
      .then(res => {
        expect(res.statusCode).toBe(404);
        expect(res.body).toEqual(expect.any(Object));
        expect(res.body.message).toBe("Email address is already registered ");
        done();
      });
  });
  it("should  allow a user to login", done => {
    let data = {
      password: "123456",
      email: "test@test.com"
    };
    let payload = JSON.stringify(data);
    request(app)
      .post("/api/user/login/")
      .send(payload)

      .set("Content-type", "application/json")
      .then(res => {
        expect(res.statusCode).toBe(200);
        expect(res.body.message).toBe("Logged In Successfully");
        done();
      });
  });

  it("should not allow a user to login with invalid username", done => {
    let data = {
      email: "t",
      password: "123456"
    };
    let payload = JSON.stringify(data);
    request(app)
      .post("/api/user/login/")
      .send(payload)

      .set("Content-type", "application/json")
      .then(res => {
        expect(res.statusCode).toBe(404);
        expect(res.body.message).toBe("Invalid credentials");
        done();
      });
  });

  it("should not allow a user to login with invalid password", done => {
    let data = {
      email: "test@test.com",
      password: "0888888"
    };
    let payload = JSON.stringify(data);
    request(app)
      .post("/api/user/login/")
      .send(payload)

      .set("Content-type", "application/json")
      .then(res => {
        expect(res.statusCode).toBe(404);
        expect(res.body.message).toBe("Invalid credentials");
        done();
      });
  });
  it("should not send OTP to registered email ", done => {
    let data = {
      email: "test@test.com"
    };
    let payload = JSON.stringify(data);
    request(app)
      .post("/api/user/sendotp/")
      .send(payload)

      .set("Content-type", "application/json")
      .then(res => {
        expect(res.statusCode).toBe(404);
        expect(res.body.message).toBe("Email address is already registered ");
        done();
      });
  });
  it("should  send OTP to a non- registered email ", done => {
    let data = {
      email: "test123@test.com"
    };
    let payload = JSON.stringify(data);
    request(app)
      .post("/api/user/sendotp/")
      .send(payload)

      .set("Content-type", "application/json")
      .then(res => {
        expect(res.statusCode).toBe(200);
        expect(res.body.message).toBe("Email sent for verification");
        done();
      });
  });
  it("should  send OTP to registered email ", done => {
    let data = {
      email: "test@test.com"
    };
    let payload = JSON.stringify(data);
    request(app)
      .post("/api/user/sendOTPtoRegisteredEmail/")
      .send(payload)

      .set("Content-type", "application/json")
      .then(res => {
        expect(res.statusCode).toBe(200);
        expect(res.body.message).toBe("Email sent for verification");
        done();
      });
  });
  it("should not send OTP to a non- registered email ", done => {
    let data = {
      email: "test123@test.com"
    };
    let payload = JSON.stringify(data);
    request(app)
      .post("/api/user/sendOTPtoRegisteredEmail/")
      .send(payload)

      .set("Content-type", "application/json")
      .then(res => {
        expect(res.statusCode).toBe(404);
        expect(res.body.message).toBe(
          "Email address is not linked to any account"
        );
        done();
      });
  });

  it("should  reset password for a registered email ", done => {
    let data = {
      email: "test@test.com",
      password: "test123"
    };
    let payload = JSON.stringify(data);
    request(app)
      .post("/api/user/resetpassword/")
      .send(payload)

      .set("Content-type", "application/json")
      .then(res => {
        expect(res.statusCode).toBe(200);
        expect(res.body.message).toBe("Reset Password Successfully");
        done();
      });
  });
  it("should not reset password for non- registered email ", done => {
    let data = {
      email: "test123@test.com",
      password: "test123"
    };
    let payload = JSON.stringify(data);
    request(app)
      .post("/api/user/resetpassword/")
      .send(payload)

      .set("Content-type", "application/json")
      .then(res => {
        expect(res.statusCode).toBe(404);
        expect(res.body.message).toBe(
          "No account linked to the provided email address"
        );
        done();
      });
  });

  it("should delete a user", done => {
    request(app)
      .delete("/api/user/delete/" + id)

      .set("Content-type", "application/json")
      .then(res => {
        expect(res.statusCode).toBe(200);
        expect(res.body).toEqual(expect.any(Object));
        expect(res.body.message).toBe("Deleted 1 user successfully");
        done();
      });
  });
  it("should delete a user", done => {
    request(app)
      .delete("/api/user/delete/5dc599625aa46f0384c73e24")

      .set("Content-type", "application/json")
      .then(res => {
        expect(res.statusCode).toBe(404);
        expect(res.body).toEqual(expect.any(Object));
        expect(res.body.message).toBe("No user to delete with given id");
        done();
      });
  });
});
