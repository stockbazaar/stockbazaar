const request = require("supertest");
const app = require("../../index");
let ticker_id = 9;

describe("Testing for fetching all the stocks data", () => {
  it("checks for the positive call to get and object with status code 200 and a message for route getStockDetails", done => {
    request(app)
      .get("/api/stockspecific/getstockdetails/" + ticker_id)
      .then(res => {
        expect(res.statusCode).toBe(200);
        expect(res.body).toEqual(expect.any(Object));
        expect(res.body.message).toBe("Returned stock detail successfully");
        done();
      });
  });
});

describe("Testing for fetching all the financial data", () => {
  it("checks for the positive call to get and object with status code 200 and a message for route getfinancialreports", done => {
    request(app)
      .get("/api/stockspecific/getfinancialreports/" + ticker_id)
      .then(res => {
        expect(res.statusCode).toBe(200);
        expect(res.body).toEqual(expect.any(Object));
        expect(res.body.message).toBe("Returned financial detail successfully");
        done();
      });
  });
});
