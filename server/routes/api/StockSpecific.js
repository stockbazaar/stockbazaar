const express = require("express");
const router = express.Router();
const Stocks = require("../../models/stocksModel");

router.get("/getstockdetails/:ticker_id", async (req, res, next) => {
  const ticker_id = req.params.ticker_id;
  try {
    let result = await Stocks.find(
      { ticker_id: +ticker_id },
      { isIndex: false },
      (err, docs) => {
        if (err)
          throw {
            statusCode: 500
          };
        let data = [];
        Object.values(docs).map(value => {
          let profile = value._doc.profile;
          let ticker_logo = value._doc.ticker_logo;
          let sector = value._doc.sector;
          // if (value._doc.ticker_dates) {
          let last_two_date = Object.values(value._doc.ticker_dates).slice(-2);
          console.log(last_two_date);
          let change =
            Math.round(
              (last_two_date[1]["Share Price"] -
                last_two_date[0]["Share Price"]) *
                10000
            ) / 10000;
          console.log(change);
          let changePercentage =
            Math.round(
              (change / last_two_date[1]["Share Price"]) * 100 * 10000
            ) / 10000;
          console.log(changePercentage);
          data.push({
            last_date: last_two_date[1].date,
            last_price: Math.round(last_two_date[1]["Share Price"] * 100) / 100,
            change: change,
            changePercentage: Math.round(changePercentage * 100) / 100,
            ticker_id: value.ticker_id,
            ticker_name: value.ticker_name,
            company_name: value.company_name,
            profile: profile,
            ticker_logo: ticker_logo,
            sector: sector
          });
          // } else {
          //   data.push({
          //     last_date: "NA",
          //     last_price: "NA",
          //     change: "NA",
          //     changePercentage: "NA",
          //     ticker_id: value.ticker_id,
          //     ticker_name: value.ticker_name,
          //     company_name: value.company_name,
          //     profile: profile,
          //     ticker_logo: ticker_logo
          //   });
          // }
        });

        if (!docs)
          return res.status(404).json({
            message: "Cannot find stock with the given ticker id"
          });
        else {
          res.status(200).json({
            data: data,
            message: "Returned stock detail successfully"
          });
        }
      }
    );
  } catch (err) {
    next(err);
  }
});

router.get("/getfinancialreports/:ticker_id", async (req, res, next) => {
  let ticker_id = req.params.ticker_id;
  let datesCF = [];
  const result = await Stocks.aggregate([
    { $unwind: "$ticker_dates" },
    {
      $match: {
        ticker_id: +ticker_id,
        "ticker_dates.date": {
          $lte: new Date("2019-12-31"),
          $gte: new Date("2018-12-25")
        }
      }
    },
    {
      $project: {
        ticker_dates: 1,
        ticker_id,
        // ,"ticker_dates.Market Capitalisation": 1,

        quarter: {
          $cond: [
            {
              $and: [
                { $eq: [{ $month: "$ticker_dates.date" }, 3] },
                { $lte: [{ $dayOfMonth: "$ticker_dates.date" }, 31] },
                { $gt: [{ $dayOfMonth: "$ticker_dates.date" }, 25] }
              ]
            },
            "first",
            {
              $cond: [
                {
                  $and: [
                    { $eq: [{ $month: "$ticker_dates.date" }, 6] },
                    { $lte: [{ $dayOfMonth: "$ticker_dates.date" }, 30] },
                    { $gt: [{ $dayOfMonth: "$ticker_dates.date" }, 25] }
                  ]
                },
                "second",
                {
                  $cond: [
                    {
                      $and: [
                        { $eq: [{ $month: "$ticker_dates.date" }, 9] },
                        { $lte: [{ $dayOfMonth: "$ticker_dates.date" }, 30] },
                        { $gt: [{ $dayOfMonth: "$ticker_dates.date" }, 25] }
                      ]
                    },
                    "third",
                    {
                      $cond: [
                        {
                          $and: [
                            { $eq: [{ $month: "$ticker_dates.date" }, 12] },
                            {
                              $lte: [{ $dayOfMonth: "$ticker_dates.date" }, 31]
                            },
                            { $gt: [{ $dayOfMonth: "$ticker_dates.date" }, 25] }
                          ]
                        },
                        "fourth",
                        "fifth"
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        }
      }
    },
    { $match: { quarter: { $ne: "fifth" } } },
    {
      $group: {
        _id: {
          year: { $year: "$ticker_dates.date" },
          month: { $month: "$ticker_dates.date" }
        },
        date_values: { $push: "$ticker_dates" }
      }
    },
    {
      $sort: {
        "_id.year": -1,
        "_id.month": -1
      }
    }
  ]);
  //  ]).toArray();
  //  console.log(JSON.stringify(res))
  if (!result)
    return res.status(404).json({
      message: "Cannot find stock with the given ticker id"
    });
  else {
    for (let i of result) {
      for (dates of i.date_values) {
        if (dates.hasOwnProperty("Revenues")) {
          dates["Gross Profit"] = dates["Revenues"] - dates["COGS"];
          dates["Other Short Term Assets"] =
            dates["Current Assets"] -
            (dates["Cash, Cash Equivalents & Short Term Investments"] +
              dates["Receivables"]);
          dates["Long Term Investments & Receivables"] =
            dates["Total Noncurrent Assets"] - dates["Net PP&E"];
          datesCF.push(dates);
        }
      }
    }
  }
  let ratios = {};
  let ratiosarray = [];
  for (ratio of result[1].date_values) {
    if (ratio.hasOwnProperty("Revenues")) {
      ratios["EV / EBITDA"] = Number(ratio["EV / EBITDA"]).toFixed(2);
      ratios["Debt to Assets Ratio"] = Number(
        ratio["Debt to Assets Ratio"]
      ).toFixed(2);
      ratios["Liabilities to Equity Ratio"] = Number(
        ratio["Liabilities to Equity Ratio"]
      ).toFixed(2);
      ratios["EPS"] = Number(
        ratio["Retained Earnings"] / ratio["Avg Basic Shares Outstanding"]
      ).toFixed(2);
      ratios["P/E"] =
        201.55 /
        ratio["Retained Earnings"] /
        ratio["Avg Basic Shares Outstanding"];
      ratios["Return on Equity"] = Number(ratio["Return on Equity"]).toFixed(2);
    }
  }
  ratiosarray.push(ratios);

  // datesCF.push(ratios);

  let changepercent = {};
  for (key of Object.keys(datesCF[0])) {
    let changevalue =
      ((datesCF[0][`${key}`] - datesCF[1][`${key}`]) / datesCF[1][`${key}`]) *
      100;
    changepercent[`${key}`] = Math.round(changevalue * 100) / 100;
  }
  changepercent["date"] = "CHG%";
  datesCF.push(changepercent);
  let newDates = {
    datesCF: datesCF,
    ratiosarray: ratiosarray
  };
  res.status(200).json({
    data: newDates,
    message: "Returned financial detail successfully"
  });
});

//[kavita 21-11-2019]
//fetches financial reports for all years of  a specified ticker id
// used only for providing download data
router.get("/download/allreports/:ticker_id", async (req, res, next) => {
  let ticker_id = req.params.ticker_id;
  let datesCF = [];
  const result = await Stocks.aggregate([
    { $unwind: "$ticker_dates" },
    {
      $match: {
        ticker_id: +ticker_id
      }
    },
    {
      $project: {
        ticker_dates: 1,
        ticker_id,
        // ,"ticker_dates.Market Capitalisation": 1,

        quarter: {
          $cond: [
            {
              $and: [
                { $eq: [{ $month: "$ticker_dates.date" }, 3] },
                { $lte: [{ $dayOfMonth: "$ticker_dates.date" }, 31] },
                { $gt: [{ $dayOfMonth: "$ticker_dates.date" }, 25] }
              ]
            },
            "first",
            {
              $cond: [
                {
                  $and: [
                    { $eq: [{ $month: "$ticker_dates.date" }, 6] },
                    { $lte: [{ $dayOfMonth: "$ticker_dates.date" }, 30] },
                    { $gt: [{ $dayOfMonth: "$ticker_dates.date" }, 25] }
                  ]
                },
                "second",
                {
                  $cond: [
                    {
                      $and: [
                        { $eq: [{ $month: "$ticker_dates.date" }, 9] },
                        { $lte: [{ $dayOfMonth: "$ticker_dates.date" }, 30] },
                        { $gt: [{ $dayOfMonth: "$ticker_dates.date" }, 25] }
                      ]
                    },
                    "third",
                    {
                      $cond: [
                        {
                          $and: [
                            { $eq: [{ $month: "$ticker_dates.date" }, 12] },
                            {
                              $lte: [{ $dayOfMonth: "$ticker_dates.date" }, 31]
                            },
                            { $gt: [{ $dayOfMonth: "$ticker_dates.date" }, 25] }
                          ]
                        },
                        "fourth",
                        "fifth"
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        }
      }
    },
    { $match: { quarter: { $ne: "fifth" } } },
    {
      $group: {
        _id: {
          year: { $year: "$ticker_dates.date" },
          month: { $month: "$ticker_dates.date" }
        },
        date_values: { $push: "$ticker_dates" }
      }
    },
    {
      $sort: {
        "_id.year": -1,
        "_id.month": -1
      }
    }
  ]);

  if (!result)
    return res.status(404).json({
      message: "Cannot find stock with the given ticker name"
    });
  else {
    for (let i of result) {
      for (dates of i.date_values) {
        if (dates.hasOwnProperty("Revenues")) {
          dates["Gross Profit"] = dates["Revenues"] - dates["COGS"];
          dates["Other Short Term Assets"] =
            dates["Current Assets"] -
            (dates["Cash, Cash Equivalents & Short Term Investments"] +
              dates["Receivables"]);
          dates["Long Term Investments & Receivables"] =
            dates["Total Noncurrent Assets"] - dates["Net PP&E"];

          datesCF.push(dates);
        }
      }
    }
  }

  res.status(200).json({
    data: datesCF,
    message: "Returned stock detail successfully"
  });
});

module.exports = router;
