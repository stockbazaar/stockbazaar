const express = require("express");
const router = express.Router();
const bcrypt = require("bcrypt");
const mailsender = require("../../middleware/email");
const User = require("../../models/user");
const validateUser = require("../../middleware/validation");
const token = require("../../middleware/token");
//gobal variable declared for use in different routes i.e. send Otp and verify otp
let otp;
//returns all user data as response
router.get("/all", async (req, res, next) => {
  try {
    const result = await User.find();
    if (!result)
      return res.status(404).json({
        message: "No record found"
      });
    else {
      return res.status(200).json({
        data: result,
        message: "Retrieved all user information successfully"
      });
    }
  } catch (err) {
    next(err);
  }
});
//provides a token for registered user
router.post("/login", async (req, res, next) => {
  try {
    //checks if the user is registered with the email provided
    let user = await User.findOne({ email: req.body.email });
    if (!user) {
      return res.status(404).json({
        message: "Invalid credentials"
      });
    }
    //checks whether the password is correct
    else if (!(await bcrypt.compare(req.body.password, user.password))) {
      return res.status(404).json({
        message: "Invalid credentials"
      });
    } else {
      //  if email and password are correct, generates a token with payload of username, email and isAdmin
      global.usertoken = token.generate({
        username: user.username,
        email: user.email,
        isAdmin: user.isAdmin
      });
      //sends the token  and success message
      res.status(200).json({
        message: "Logged In Successfully",
        token: usertoken,
        isAdmin: user.isAdmin
      });
    }
  } catch (err) {
    console.log(err);
    next(err);
  }
});

//creates a new user
router.post("/new", async (req, res, next) => {
  let new_user = {
    name: req.body.name,
    password: req.body.password,
    email: req.body.email,
    isAdmin: req.body.isAdmin
  };
  //checks whether the provided email address is already registered and sends reponse for the same
  let result = await User.findOne({ email: req.body.email });

  if (result != null)
    return res.status(404).json({
      message: "Email address is already registered "
    });
  //validates the user data with joi validation and creates one if validated
  else
    validateUser(new_user)
      .then(async () => {
        const data = new User({
          name: req.body.name,
          //encrypts the password with bcrypt and stores the encrypted password
          password: bcrypt.hashSync(req.body.password, 10),
          email: req.body.email,
          isAdmin: req.body.isAdmin
        });
        //save user data into database
        await data.save();
        //sends success response along with newly added data
        res.status(200).json({
          data: data,
          message: "Registered successfully"
        });
      })

      .catch(err => {
        err.customMessage = err.details[0].message;
        console.log(err);
        next(err);
      });
});

//deletes the user with given id
//used for testing purpose only...to delete the test user after testing the create user api
router.delete("/delete/:id", async (req, res, next) => {
  const id = req.params.id;
  try {
    let result = await User.findById(id);

    if (!result)
      return res.status(404).json({
        message: "No user to delete with given id"
      });

    await User.findByIdAndDelete(id);

    res.status(200).json({
      message: " User Deleted successfully"
    });
  } catch (err) {
    next(err);
  }
});

//sends otp to new user while registering
router.post("/sendotp", async (req, res, next) => {
  const emailid = req.body.email;
  try {
    // checks if the user is already registered
    let result = await User.findOne({ email: emailid });
    //does not send an OTP to a registered email id and sends reponse for the same
    if (result != null)
      return res.status(404).json({
        message: "Email address is already registered "
      });
    else {
      //generates a new random number to be sent as OTP
      otp = Math.floor(Math.random() * 9999 + 1000);

      // call a middleware which sends email to the user email and otp  provided
      mailsender(otp, emailid)
        .then(() => {
          res.status(200).json({
            message: "Email sent for verification"
          });
        })

        .catch(err => {
          err.customMessage = "Some error occurred. Please try again";
          console.log(err);
          next(err);
        });
    }
  } catch (err) {
    next(err);
  }
});
//sends otp to registered  user only....used for reset of password
router.post("/sendOTPtoRegisteredEmail", async (req, res, next) => {
  const emailid = req.body.email;
  try {
    // checks if the user is  registered
    let result = await User.findOne({ email: emailid });

    //does not send an OTP to a non-registered email id and sends reponse for the same
    if (!result)
      return res.status(404).json({
        message: "Email address is not linked to any account"
      });
    else {
      //generates a new random number to be sent as OTP
      otp = Math.floor(Math.random() * 9999 + 1000);

      // call a middleware which sends email to the user email and otp  provided
      mailsender(otp, emailid)
        .then(() => {
          res.status(200).json({
            message: "Email sent for verification"
          });
        })

        .catch(err => {
          err.customMessage = "Some error occurred. Please try again";
          console.log(err);
          next(err);
        });
    }
  } catch (err) {
    next(err);
  }
});
//resets password for a registered user
router.post("/resetpassword/", async (req, res, next) => {
  try {
    // checks if the user is  registered  and sends reponse for the same
    let result = await User.findOne({ email: req.body.email });
    if (!result)
      return res.status(404).json({
        message: "No account linked to the provided email address"
      });
    else {
      // updates the user data with new password
      await User.updateOne(
        { email: req.body.email },
        {
          $set: {
            name: result.name,
            password: bcrypt.hashSync(req.body.password, 10),
            email: result.email,
            isAdmin: result.isAdmin
          }
        }
      );
      res.status(200).json({
        message: "Reset Password Successfully"
      });
    }
  } catch (err) {
    next(err);
    console.log(err);
  }
});
//verifies otp
router.post("/otpverification/", async (req, res, next) => {
  try {
    let userotp = req.body.otp;

    if (!userotp) {
      return res.status(404).json({
        message: "Please provide an OTP"
      });
    }
    if (userotp == otp) {
      return res.status(200).json({
        message: "OTP verified"
      });
    } else {
      return res.status(500).json({
        message: "Incorrect OTP"
      });
    }
  } catch (err) {
    next(err);
  }
});

module.exports = router;
