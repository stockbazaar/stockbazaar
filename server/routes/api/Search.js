const express = require("express");
const router = express.Router();
//Using the pre-defined Database Model to Query the Database - Eli
const Stocks = require("../../models/stocksModel");

// Query to return the expected User-input - Eli
router.post("/all", async (req, res) => {
  let result = await Stocks.find(
    {
      // The Filters used in the user SearchInput - Eli
      $or: [
        {
          ticker_name: { $regex: req.body.searchInput, $options: "i" },
          isIndex: false
        },
        {
          sector: { $regex: req.body.searchInput, $options: "i" },
          isIndex: false
        },
        {
          industry: { $regex: req.body.searchInput, $options: "i" },
          isIndex: false
        },
        {
          company_name: { $regex: req.body.searchInput, $options: "i" },
          isIndex: false
        }
      ]
    },
    // Projection of only what is to show in the FrontEnd - Eli
    {
      ticker_id: 1,
      ticker_name: 1,
      ticker_logo: 1,
      sector: 1,
      industry: 1,
      company_name: 1,
      _id: 0
    }
  );

  // Checking the Negative Case if Nothing is Found - Eli
  if (result < 0) {
    res.status(400).json({
      status: 400,
      data: null,
      message: "No Data Found"
    });
  }

  // Checking for Positive Response and Sending the Data to the Search Action on the FrontEnd - Eli
  res.status(200).json({
    status: 200,
    data: result,
    message: "Retrieved Search Result successfully"
  });
});

// Query to return the expected User-input - Eli
router.post("/sectorsearch", async (req, res) => {
  console.log(req.body.searchInput2);
  // console.log("Sector is", req.body.sector);
  let result = await Stocks.find(
    {
      // The Filters used in the user SearchInput - Eli
      $or: [
        {
          ticker_name: /^req.body.searchInput2$/i,
          isIndex: false
        },
        {
          sector: req.body.sector,
          isIndex: false
        }
        // {
        //   industry: { $regex: req.body.searchInput, $options: "i" },
        //   isIndex: false
        // },
        // {
        //   company_name: { $regex: req.body.searchInput, $options: "i" },
        //   isIndex: false
        // }
      ]
    },
    // Projection of only what is to show in the FrontEnd - Eli
    {
      ticker_id: 1,
      ticker_name: 1,
      ticker_logo: 1,
      sector: 1,
      industry: 1,
      company_name: 1,
      _id: 0
    }
  );

  // Checking the Negative Case if Nothing is Found - Eli
  if (result < 0) {
    res.status(400).json({
      status: 400,
      data: null,
      message: "No Data Found"
    });
  }

  // Checking for Positive Response and Sending the Data to the Search Action on the FrontEnd - Eli
  res.status(200).json({
    status: 200,
    data: result,
    message: "Retrieved Compare Search Result successfully"
  });
});

module.exports = router;
