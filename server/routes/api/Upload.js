const express = require("express");
const router = express.Router();
const admin = require("../../middleware/admin");
//kavita 13-11-2019 5:00 p.m.
const path = require("path");
var fs = require("fs");

//script to upload the file in database i.e. initially to postgres and then to mongodb
var uploadToDb = require("../../db-upload/CsvToMongo");

//middleware to upload files
var multer = require("multer");
//provides destination and name in  storage for uploaded file
var storage = multer.diskStorage({
  destination: function(req, file, callback) {
    callback(null, "./uploadedfiles");
  },
  filename: function(req, file, callback) {
    callback(null, file.originalname);
  }
});

//instance of multer  to store the uploaded file with a filter
var upload = multer({
  storage: storage,
  //applies a file filter which allows only .csv  files
  fileFilter: function(req, file, callback) {
    if (path.extname(file.originalname) !== ".csv") {
      return callback(new Error("Only csv files allowed!"));
    }
    callback(null, true);
  }
}).single("file"); //accepts a single file named "file"

router.post("/upload", admin, function(req, res, next) {
  //stores the uploaded file in the  destination folder
  upload(req, res, function(err) {
    //the file is stored in req.file
    console.log("file :", req.file);
    if (err) {
      return res.status(500).json({
        message:
          "File upload failed due to some internal server error. Please try again"
      });
    } else {
      console.log("FIle uploaded");
      //
      var csvFilePath = path.join(
        __dirname,
        "../../uploadedfiles/" + req.file.filename
      );
      var csvinput = fs.readFileSync(csvFilePath).toString();
      var lines = csvinput.split("\n");
      //check if the file is in correct format
      var headers = lines[0].split(",");
      if (
        headers.includes("ticker") &&
        headers.includes("simfinid") &&
        headers.includes("companyid") &&
        headers.includes("indictorname") &&
        headers.includes("dates") &&
        headers.includes("indicatorvalue")
      ) {
        //updates  database with uploaded data
        uploadToDb(req.file.filename, next)
          .then(result => {
            console.log("FIle uploaded");
            return res
              .status(200)
              .json({ message: "File uploaded sucessfully" });
          })
          .catch(err => {
            console.log(err);
            return res.status(500).json({
              message: err
            });
          });
      } else {
        res.status(500).json({ message: "Invalid format " });
      }
    }
  });
});


module.exports = router;
