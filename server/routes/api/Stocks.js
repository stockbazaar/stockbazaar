const express = require("express");
const router = express.Router();
const StocksData = require("../../models/stocksModel");

//
// [Ankit: 10/11/2019, 06:04pm]
// [Modified - Ankit: 10/11/2019, 06:04pm]

router.get("/indices", async (req, res, next) => {
  try {
    let final_data = [];

    const result = StocksData.find(
      { isIndex: true },
      { ticker_dates: 1, ticker_id: 1, ticker_name: 1 },
      (err, docs) => {
        // throws error if occurred
        if (err)
          throw {
            statusCode: 500,
            customMessage: "Error on server"
          };

        // data array which stores the object of each ticker
        let data = [];

        // Maps through retrived data from the query to stores the
        // values of each ticker in required format having tickerId,
        // tickerName, tickerValue with last, change and changePercentage
        Object.values(docs).map(value => {
          // lastTwoDate -> stores only the last two dates of each ticker
          // from the queried result
          let lastTwoDate = Object.values(value.ticker_dates).slice(-2);
          // console.log("LAST", lastTwoDate);
          // console.log("VAlue", value.ticker_dates);
          // calculates change for the ticker
          let change =
            Math.round(
              (lastTwoDate[1].closing - lastTwoDate[0].closing) * 10000
            ) / 10000;

          // calculates change percentage for the ticker
          let changePercentage =
            Math.round((change / lastTwoDate[1].closing) * 100 * 10000) / 10000;

          // Pushes the data of each ticker as object to data array
          data.push({
            tickerId: value.ticker_id,
            tickerName: value.ticker_name,
            tickerValue: {
              last: Math.round(lastTwoDate[1].closing * 100) / 100,
              change: change,
              changePercentage: changePercentage
            }
          });
          var sorted = data;
          sorted = sorted.sort(function(obj1, obj2) {
            return (
              obj2.tickerValue.changePercentage -
              obj1.tickerValue.changePercentage
            );
          });
          final_data = sorted;
        });

        // sends response to the client with the required data
        res.status(200).json({
          data: final_data,
          message: "All indices retrieved successfully"
        });
      }
    );
  } catch (err) {
    next(err);
  }
});
// [Shweta: 13/11/2019, 06:04pm]
// [Modified - Shweta: 13/11/2019, 02:04pm]

router.get("/overview", async (req, res, next) => {
  //  this query is used to fetch the top gainers and losers based on their change percentage in the share price.
  try {
    var data = { tickerValue: {} };
    let result = await StocksData.aggregate(
      [
        {
          // match works similar to find query. Since the tickers are not any indices so we are specifying the isIndex parameter false
          $match: {
            isIndex: false,
            "ticker_dates.1": { $exists: true }
          }
        },
        {
          $project: {
            ticker_name: 1,
            ticker_id: 1,
            ticker_data: {
              //   sice we want share prices of the last 2 days we are passing -1 and -2
              secondLast: { $arrayElemAt: ["$ticker_dates", -2] },
              last: { $arrayElemAt: ["$ticker_dates", -1] }
            }
          }
        },
        {
          //  we are grouping the results based on their ticker id's and names's
          $group: {
            _id: { tickerName: "$ticker_name", tickerId: "$ticker_id" },

            tickers_data: { $addToSet: "$ticker_data" }
          }
        }
      ],
      (err, docs) => {
        if (err) {
          console.log(err);
        }
        if (!docs)
          return res.status(404).json({
            message: "Cannot find any stock"
          });
        else {
          var overview = [];
          for (const each_ticker in docs) {
            let total_market_cap = 0;
            let total_change_last = 0;
            let total_change_second_last = 0;
            let total_change_percent = 0;
            for (const each_date of docs[each_ticker].tickers_data) {
              //  if the ticker date has market cap only then it will append and add the values and will be stored
              //  in total market cap

              if (each_date.last.hasOwnProperty("Market Capitalisation")) {
                total_market_cap += each_date.last["Market Capitalisation"];
              }
              if (
                //  if the ticker date of last and second last day has share price only then it will append and add the values and
                // will be stored in total_change_last and total_change_second_last respectively

                each_date.last.hasOwnProperty("Share Price") &&
                each_date.secondLast.hasOwnProperty("Share Price")
              ) {
                total_change_last += each_date.last["Share Price"];
                total_change_second_last += each_date.secondLast["Share Price"];
              }
            }
            //  we calculate the total change bu subtracting the second last from last days's share price
            let total_change =
              (Math.round(total_change_last - total_change_second_last) *
                10000) /
              10000;
            total_change_percent =
              Math.round((total_change / total_change_last) * 100 * 10000) /
              10000;

            overview.push({
              tickerId: docs[each_ticker]._id.tickerId,
              tickerName: docs[each_ticker]._id.tickerName,
              tickerValue: {
                last: total_change_last,
                marketCap: (Math.round(total_market_cap) * 100) / 100,
                change: total_change,
                changePercentage: total_change_percent
              }
            });
          }
          // we are sorting the data in descending order based on their change percentages
          var sorted_sectors = overview;
          sorted_sectors = sorted_sectors.sort(function(obj1, obj2) {
            return (
              obj2.tickerValue.changePercentage -
              obj1.tickerValue.changePercentage
            );
          });
          var final_data = {};
          final_data["gainers"] = sorted_sectors.slice(0, 10);
          final_data["losers"] = sorted_sectors.slice(-11, -1).reverse();

          res.status(200).json({
            data: final_data,
            message:
              "Returned top 10 gainers and losers overview details successfully"
          });
        }
      }
    );
  } catch (err) {
    next(err);
  }
});
router.get("/performance", async (req, res, next) => {
  //  this query is used to get the performance of the top gainers and losers in 3 months, 6 months and 1 year
  try {
    var data = { tickerValue: {} };
    let result = await StocksData.aggregate(
      [
        {
          // match works similar to find query. Since the tickers are not any indices so we are specifying the isIndex parameter false

          $match: {
            isIndex: false,
            "ticker_dates.1": { $exists: true }
          }
        },
        {
          $project: {
            ticker_name: 1,
            ticker_id: 1,
            ticker_data: {
              secondLast: { $arrayElemAt: ["$ticker_dates", -2] },
              last: { $arrayElemAt: ["$ticker_dates", -1] },
              quarter: { $arrayElemAt: ["$ticker_dates", -10] },
              halfYear: { $arrayElemAt: ["$ticker_dates", -15] },
              Year: { $arrayElemAt: ["$ticker_dates", -25] }
            }
          }
        },
        {
          $group: {
            //  we are grouing them based on their ticker ids' and names'
            _id: { tickerName: "$ticker_name", tickerId: "$ticker_id" },

            tickers_data: { $addToSet: "$ticker_data" }
          }
        }
      ],
      (err, docs) => {
        if (err) {
          console.log(err);
        }
        if (!docs)
          return res.status(404).json({
            message: "Cannot find any stock"
          });
        else {
          var overview = [];
          for (const each_ticker in docs) {
            let total_market_cap = 0;
            let total_change_last = 0;
            let total_change_second_last = 0;
            let total_change_percent = 0;
            for (const each_date of docs[each_ticker].tickers_data) {
              //  if the ticker date of last day has market cap only then it will append and add the values and
              // will be stored in total_market cap

              if (each_date.last.hasOwnProperty("Market Capitalisation")) {
                total_market_cap += each_date.last["Market Capitalisation"];
              }

              if (
                //  if the ticker date of last and second last day has share price only then it will append and add the values and
                // will be stored in total_change_last and total_change_second_last respectively

                each_date.last.hasOwnProperty("Share Price") &&
                each_date.secondLast.hasOwnProperty("Share Price")
              ) {
                total_change_last += each_date.last["Share Price"];
                total_change_second_last += each_date.secondLast["Share Price"];
              }
            }
            let total_change =
              (Math.round(total_change_last - total_change_second_last) *
                10000) /
              10000;
            total_change_percent =
              Math.round((total_change / total_change_last) * 100 * 10000) /
              10000;
            // *************************************************************************************************************//
            //  similarly we will calculate the change and change percentage for 3 Months (Quaterly Performance)
            let total_last = 0;
            let total_change_quarter = 0;
            let total_quarter_percent = 0;
            for (const each_date of docs[each_ticker].tickers_data) {
              if (
                each_date.last["Share Price"] &&
                each_date.quarter["Share Price"] != undefined
              ) {
                total_last += each_date.last["Share Price"];
                total_change_quarter += each_date.quarter["Share Price"];
              } else {
                total_last = 0;
                total_change_quarter = 0;
              }
            }
            if (total_change_quarter && total_last == null) {
              total_change_quarter = 0;
              total_last = 0;
            }
            let totalChangeForQuarter =
              (Math.round(total_last - total_change_quarter) * 10000) / 10000;
            total_quarter_percent =
              Math.round((totalChangeForQuarter / total_last) * 100 * 10000) /
              10000;
            // ******************************************6 MONTHS************************************************
            // here we are calculating the change and change percentage for 6 Months (Half yearly Performance)

            let total_half_yearly_last = 0;
            let total_change_halfYear = 0;
            let total_halfYear_percent = 0;
            for (const each_date of docs[each_ticker].tickers_data) {
              if (
                each_date.last["Share Price"] &&
                each_date.halfYear["Share Price"] != undefined
              ) {
                total_half_yearly_last += each_date.last["Share Price"];
                total_change_halfYear += each_date.halfYear["Share Price"];
              } else {
                total_half_yearly_last = 0;
                total_change_halfYear = 0;
              }
            }
            if (total_change_halfYear && total_half_yearly_last == null) {
              total_change_halfYear = 0;
              total_half_yearly_last = 0;
            }
            let totalChangeForhalfYear =
              (Math.round(total_half_yearly_last - total_change_halfYear) *
                10000) /
              10000;
            total_halfYear_percent =
              Math.round(
                (totalChangeForhalfYear / total_half_yearly_last) * 100 * 10000
              ) / 10000;

            // ******************************************** ONE YEAR ******************************************
            // here we are calculating the change and change percentage for 1 year (Yearly Performance)

            let total_yearly_last = 0;
            let total_change_Year = 0;
            let total_Year_percent = 0;
            for (const each_date of docs[each_ticker].tickers_data) {
              if (
                each_date.last["Share Price"] &&
                each_date.Year["Share Price"] != undefined
              ) {
                total_yearly_last += each_date.last["Share Price"];
                total_change_Year += each_date.Year["Share Price"];
              } else {
                total_yearly_last = 0;
                total_change_Year = 0;
              }
            }
            if (total_change_Year && total_yearly_last == null) {
              total_change_Year = 0;
              total_yearly_last = 0;
            }
            let totalChangeForYear =
              (Math.round(total_yearly_last - total_change_Year) * 10000) /
              10000;
            total_Year_percent =
              Math.round(
                (totalChangeForYear / total_yearly_last) * 100 * 10000
              ) / 10000;
            overview.push({
              // isIndex:docs[],
              tickerId: docs[each_ticker]._id.tickerId,
              tickerName: docs[each_ticker]._id.tickerName,
              tickerValue: {
                changePercentage: total_change_percent,
                total_quarter_percent: total_quarter_percent,
                total_halfYear_percent: total_halfYear_percent,
                total_Year_percent: total_Year_percent
              }
            });
          }

          //  sorting the data based on their changePercentage in descending order
          var sorted_sectors = overview;
          sorted_sectors = sorted_sectors.sort(function(obj1, obj2) {
            return (
              obj2.tickerValue.changePercentage -
              obj1.tickerValue.changePercentage
            );
          });
          var final_data = {};
          final_data["gainers"] = sorted_sectors.slice(0, 10);
          final_data["losers"] = sorted_sectors.slice(-11, -1).reverse();

          res.status(200).json({
            data: final_data,
            message:
              "Returned top 10 gainers and losers performance details successfully"
          });
        }
      }
    );
  } catch (err) {
    next(err);
  }
});

router.post("/gainersvaluation", async (req, res, next) => {
  let datesCF = [];
  let filterData = [];
  filterData.push({
    ticker_name: {
      // here we are passing an array of our top 10 gainers. we need to find theirquaterly valuations
      // these involve p/e ratio, eps, ev, ev/ebitda, total assets, market cap
      $in: [
        "AVXL",
        "PRPO",
        "CRVS",
        "HPJ",
        "CSU",
        "SHLO",
        "IDT",
        "PPSI",
        "EMMS",
        "ESYS"
      ]
    }
  });
  const result = await StocksData.aggregate([
    { $unwind: "$ticker_dates" },
    {
      // match works similar to find query. Since the tickers are not any indices
      // so we are specifying the isIndex parameter false

      $match: {
        isIndex: false,
        $and: filterData,

        "ticker_dates.date": {
          //  here we are passing the dates in which we want to search for the values of ev,ebitda ,etc.
          // these values are generated on a quaterly basis
          // as most of the tickers had all the required values so we have selected this quater
          $lte: new Date("2018-04-01"),
          $gte: new Date("2018-03-25")
        }
      }
    },
    {
      $project: {
        ticker_dates: 1,
        ticker_id: "$ticker_id",
        ticker_name: "$ticker_name",

        quarter: {
          $cond: [
            {
              $and: [
                { $eq: [{ $month: "$ticker_dates.date" }, 3] },
                { $lte: [{ $dayOfMonth: "$ticker_dates.date" }, 31] },
                { $gt: [{ $dayOfMonth: "$ticker_dates.date" }, 25] }
              ]
            },
            "first",
            // first specifies the first quarter of the year that is from jan to march
            {
              $cond: [
                {
                  $and: [
                    { $eq: [{ $month: "$ticker_dates.date" }, 6] },
                    { $lte: [{ $dayOfMonth: "$ticker_dates.date" }, 30] },
                    { $gt: [{ $dayOfMonth: "$ticker_dates.date" }, 25] }
                  ]
                },
                "second",
                //  second specifies the second quarter of the year that is from april to june
                {
                  $cond: [
                    {
                      $and: [
                        { $eq: [{ $month: "$ticker_dates.date" }, 9] },
                        { $lte: [{ $dayOfMonth: "$ticker_dates.date" }, 30] },
                        { $gt: [{ $dayOfMonth: "$ticker_dates.date" }, 25] }
                      ]
                    },
                    "third",
                    //  third specifies the tthird quarter of the year ( july to september)
                    {
                      $cond: [
                        {
                          $and: [
                            { $eq: [{ $month: "$ticker_dates.date" }, 12] },
                            {
                              $lte: [{ $dayOfMonth: "$ticker_dates.date" }, 31]
                            },
                            { $gt: [{ $dayOfMonth: "$ticker_dates.date" }, 25] }
                          ]
                        },
                        "fourth",
                        // fourth quarter is from october to december
                        "fifth"
                        //  fifth is passed because $cond requires 3 parameters and we had only 2.
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        }
      }
    },
    {
      $match: {
        // since fifth is not required wematch it with $not equal to.
        // thus, it is not considered while grouping the results
        quarter: { $ne: "fifth" }
      }
    },
    {
      $group: {
        _id: {
          //  grouping the results based on the year, month, ticker id and name
          year: { $year: "$ticker_dates.date" },
          month: { $month: "$ticker_dates.date" },
          ticker_name: "$ticker_name",
          ticker_id: "$ticker_id"
        },
        date_values: { $push: "$ticker_dates" }
      }
    },
    {
      $sort: {
        "_id.year": -1,
        "_id.month": -1
      }
    }
  ]);
  if (!result)
    return res.status(404).json({
      message: "Cannot find stock with the given ticker name"
    });
  else {
    for (let i of result) {
      for (dates of i.date_values) {
        if (dates.hasOwnProperty("Dividends")) {
          // if the dates has dividends only then following commands will be run
          if (
            // if the values of Net Income are not null or undefined  then the values are stored in netIncome else set to 0
            dates["Net Income from Discontinued Op"] != null ||
            dates["Net Income from Discontinued Op"] != undefined
          ) {
            var netIncome = dates["Net Income from Discontinued Op"];
          } else {
            netIncome = 0;
          }

          // if the values of Dividends are not null or undefined  then the values are stored in dividends else set to 0

          if (dates["Dividends"] != null || dates["Dividends"] != undefined) {
            var dividends = dates["Dividends"];
          } else {
            dividends = 0;
          }

          // if the values of avg Outstanding are not null or undefined  then the values are stored in avg Outstanding else set to 0

          if (
            dates["Avg Basic Shares Outstanding"] != null ||
            dates["Avg Basic Shares Outstanding"] != undefined
          ) {
            var avgOut = dates["Avg Basic Shares Outstanding"];
          } else {
            avgOut = 0;
          }

          var eps = netIncome - dividends / avgOut;
          var marketCap = dates["Market Capitalisation"];
          var totalAssets = dates["Total Assets"];
          totalAssets = Math.round(totalAssets * 10000) / 10000;
          var ebitda = dates["EBITDA"];
          var evByEbitda = dates["EV / EBITDA"];

          // since we didn't have the value of ev, we calculated it bu multiplying the evByEbitda ab=nd ebitda
          var ev = evByEbitda * ebitda;
          ev = Math.round(ev * 10000) / 10000;

          if (totalAssets === undefined || totalAssets === null) {
            totalAssets = 0;
          }
          if (dates["Share Price"] != null) {
            var sharePrice = dates["Share Price"];
          } else {
            sharePrice = "--";
          }
          if (sharePrice == "--" || eps == 0) {
            var pToE = "--";
          } else {
            pToE = sharePrice / eps;
          }

          datesCF.push({
            ticker_id: i._id.ticker_id,
            tickerName: i._id.ticker_name,
            tickerValue: {
              sharePrice: sharePrice,
              changePercentage: marketCap,
              totalAssets: totalAssets,
              pToE: pToE,
              eps: eps,
              ev: ev,
              evByEbitda: evByEbitda
            }
          });
          var sorted = datesCF;
          sorted = sorted.sort(function(obj1, obj2) {
            return (
              obj2.tickerValue.changePercentage -
              obj1.tickerValue.changePercentage
            );
          });
          var final_data = {};
          final_data = sorted;
        }
      }
      // }
    }
  }

  res.status(200).json({
    data: final_data,
    message: "Returned gainers valuation details successfully"
  });
});

router.post("/gainersmargin", async (req, res, next) => {
  // this query is used to get the gross, operating and net profit margin for the top gainers
  let datesCF = [];
  let filterData = [];
  filterData.push({
    ticker_name: {
      $in: [
        "AVXL",
        "PRPO",
        "CRVS",
        "HPJ",
        "CSU",
        "SHLO",
        "IDT",
        "PPSI",
        "EMMS",
        "ESYS"
      ]
    }
  });
  const result = await StocksData.aggregate([
    { $unwind: "$ticker_dates" },
    {
      // match works similar to find query.
      // Since the tickers are not any indices so we are specifying the isIndex parameter false
      $match: {
        isIndex: false,
        $and: filterData,

        "ticker_dates.date": {
          //  here we are passing the dates in which we want to search for the values of gross, operating and net profit margin.
          // these values are generated on a quaterly basis
          // as most of the tickers had all the required values so we have selected this quater
          $lte: new Date("2018-04-01"),
          $gte: new Date("2018-03-25")
        }
      }
    },
    {
      $project: {
        ticker_dates: 1,
        ticker_id: "$ticker_id",
        ticker_name: "$ticker_name",

        quarter: {
          $cond: [
            {
              $and: [
                { $eq: [{ $month: "$ticker_dates.date" }, 3] },
                { $lte: [{ $dayOfMonth: "$ticker_dates.date" }, 31] },
                { $gt: [{ $dayOfMonth: "$ticker_dates.date" }, 25] }
              ]
            },
            "first",
            // first => Jan to March
            {
              $cond: [
                {
                  $and: [
                    { $eq: [{ $month: "$ticker_dates.date" }, 6] },
                    { $lte: [{ $dayOfMonth: "$ticker_dates.date" }, 30] },
                    { $gt: [{ $dayOfMonth: "$ticker_dates.date" }, 25] }
                  ]
                },
                "second",
                // second quarter => Apr to June
                {
                  $cond: [
                    {
                      $and: [
                        { $eq: [{ $month: "$ticker_dates.date" }, 9] },
                        { $lte: [{ $dayOfMonth: "$ticker_dates.date" }, 30] },
                        { $gt: [{ $dayOfMonth: "$ticker_dates.date" }, 25] }
                      ]
                    },
                    "third",
                    //  third quarter => July to September
                    {
                      $cond: [
                        {
                          $and: [
                            { $eq: [{ $month: "$ticker_dates.date" }, 12] },
                            {
                              $lte: [{ $dayOfMonth: "$ticker_dates.date" }, 31]
                            },
                            { $gt: [{ $dayOfMonth: "$ticker_dates.date" }, 25] }
                          ]
                        },
                        "fourth",
                        // fourth quarter => Oct to December
                        "fifth"
                        //  fifth is passed because $cond requires 3 parameters and we had only 2.
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        }
      }
    },
    {
      $match: {
        // since fifth is not required we match it with $not equal to.
        // thus, it is not considered while grouping the results
        quarter: { $ne: "fifth" }
      }
    },
    {
      $group: {
        _id: {
          // grouping the results based on their year, month and ticker id and name
          year: { $year: "$ticker_dates.date" },
          month: { $month: "$ticker_dates.date" },
          ticker_name: "$ticker_name",
          ticker_id: "$ticker_id"
        },
        date_values: { $push: "$ticker_dates" }
      }
    },
    {
      $sort: {
        "_id.year": -1,
        "_id.month": -1
      }
    }
  ]);
  if (!result)
    return res.status(404).json({
      message: "Cannot find stock with the given ticker name"
    });
  else {
    for (let i of result) {
      for (dates of i.date_values) {
        // if the dates has operating margin
        // then, their values will be stored in their respective variables and then push into datesCF
        if (dates.hasOwnProperty("Operating Margin")) {
          var operatingMargin = dates["Operating Margin"];
          var netMargin = dates["Net Profit Margin"];
          var grossMargin = dates["Gross Margin"];
          datesCF.push({
            ticker_id: i._id.ticker_id,
            tickerName: i._id.ticker_name,
            tickerValue: {
              grossMargin: grossMargin,
              operatingMargin: operatingMargin,
              changePercentage: netMargin
            }
          });
          // sorting the data based on the chnage percentage and in descending order
          var sorted = datesCF;
          sorted = sorted.sort(function(obj1, obj2) {
            return (
              obj2.tickerValue.changePercentage -
              obj1.tickerValue.changePercentage
            );
          });
          var final_data = {};
          final_data = sorted;
        }
      }
    }
  }

  res.status(200).json({
    data: final_data,
    message: "Returned gainers margin details successfully"
  });
});
router.post("/losersvaluation", async (req, res, next) => {
  let datesCF = [];
  let filterData = [];
  filterData.push({
    ticker_name: {
      // here we are passing an array of our top 10 losers. we need to find their quaterly valuations
      // these involve p/e ratio, eps, ev, ev/ebitda, total assets, market cap
      $in: [
        "SRNE",
        "GTXI",
        "COTY",
        "MRAM",
        "NSSC",
        "PIR",
        "MIK",
        "FOSL",
        "CC",
        "FNKO"
      ]
    }
  });
  const result = await StocksData.aggregate([
    { $unwind: "$ticker_dates" },
    {
      // match works similar to find query. Since the tickers are not any indices
      // so we are specifying the isIndex parameter false

      $match: {
        isIndex: false,
        $and: filterData,

        "ticker_dates.date": {
          //  here we are passing the dates in which we want to search for the values of ev,ebitda ,etc.
          // these values are generated on a quaterly basis
          // as most of the tickers had all the required values so we have selected this quater
          $lte: new Date("2018-04-01"),
          $gte: new Date("2018-03-25")
        }
      }
    },
    {
      $project: {
        ticker_dates: 1,
        ticker_id: "$ticker_id",
        ticker_name: "$ticker_name",

        quarter: {
          $cond: [
            {
              $and: [
                { $eq: [{ $month: "$ticker_dates.date" }, 3] },
                { $lte: [{ $dayOfMonth: "$ticker_dates.date" }, 31] },
                { $gt: [{ $dayOfMonth: "$ticker_dates.date" }, 25] }
              ]
            },
            "first",
            // first specifies the first quarter of the year that is from jan to march
            {
              $cond: [
                {
                  $and: [
                    { $eq: [{ $month: "$ticker_dates.date" }, 6] },
                    { $lte: [{ $dayOfMonth: "$ticker_dates.date" }, 30] },
                    { $gt: [{ $dayOfMonth: "$ticker_dates.date" }, 25] }
                  ]
                },
                "second",
                //  second specifies the second quarter of the year that is from april to june
                {
                  $cond: [
                    {
                      $and: [
                        { $eq: [{ $month: "$ticker_dates.date" }, 9] },
                        { $lte: [{ $dayOfMonth: "$ticker_dates.date" }, 30] },
                        { $gt: [{ $dayOfMonth: "$ticker_dates.date" }, 25] }
                      ]
                    },
                    "third",
                    //  third specifies the tthird quarter of the year ( july to september)
                    {
                      $cond: [
                        {
                          $and: [
                            { $eq: [{ $month: "$ticker_dates.date" }, 12] },
                            {
                              $lte: [{ $dayOfMonth: "$ticker_dates.date" }, 31]
                            },
                            { $gt: [{ $dayOfMonth: "$ticker_dates.date" }, 25] }
                          ]
                        },
                        "fourth",
                        // fourth quarter is from october to december
                        "fifth"
                        //  fifth is passed because $cond requires 3 parameters and we had only 2.
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        }
      }
    },
    {
      $match: {
        // since fifth is not required wematch it with $not equal to.
        // thus, it is not considered while grouping the results
        quarter: { $ne: "fifth" }
      }
    },
    {
      $group: {
        _id: {
          //  grouping the results based on the year, month, ticker id and name
          year: { $year: "$ticker_dates.date" },
          month: { $month: "$ticker_dates.date" },
          ticker_name: "$ticker_name",
          ticker_id: "$ticker_id"
        },
        date_values: { $push: "$ticker_dates" }
      }
    },
    {
      $sort: {
        "_id.year": -1,
        "_id.month": -1
      }
    }
  ]);
  if (!result)
    return res.status(404).json({
      message: "Cannot find stock with the given ticker name"
    });
  else {
    for (let i of result) {
      for (dates of i.date_values) {
        if (dates.hasOwnProperty("Dividends")) {
          // if the dates has dividends only then following commands will be run
          if (
            // if the values of Net Income are not null or undefined  then the values are stored in netIncome else set to 0
            dates["Net Income from Discontinued Op"] != null ||
            dates["Net Income from Discontinued Op"] != undefined
          ) {
            var netIncome = dates["Net Income from Discontinued Op"];
          } else {
            netIncome = 0;
          }

          // if the values of Dividends are not null or undefined  then the values are stored in dividends else set to 0

          if (dates["Dividends"] != null || dates["Dividends"] != undefined) {
            var dividends = dates["Dividends"];
          } else {
            dividends = 0;
          }

          // if the values of avg Outstanding are not null or undefined  then the values are stored in avg Outstanding else set to 0

          if (
            dates["Avg Basic Shares Outstanding"] != null ||
            dates["Avg Basic Shares Outstanding"] != undefined
          ) {
            var avgOut = dates["Avg Basic Shares Outstanding"];
          } else {
            avgOut = 0;
          }

          var eps = netIncome - dividends / avgOut;
          eps = Math.round(eps * 10000) / 10000;
          var marketCap = dates["Market Capitalisation"];
          var totalAssets = dates["Total Assets"];
          var ebitda = dates["EBITDA"];
          var evByEbitda = dates["EV / EBITDA"];

          // since we didn't have the value of ev, we calculated it bu multiplying the evByEbitda ab=nd ebitda
          var ev = evByEbitda * ebitda;
          ev = Math.round(ev * 10000) / 10000;

          if (totalAssets === undefined || totalAssets === null) {
            totalAssets = 0;
          }
          if (dates["Share Price"] != null) {
            var sharePrice = dates["Share Price"];
          } else {
            sharePrice = "--";
          }
          if (sharePrice == "--" || eps == 0) {
            var pToE = "--";
          } else {
            pToE = sharePrice / eps;
          }

          datesCF.push({
            ticker_id: i._id.ticker_id,
            tickerName: i._id.ticker_name,
            tickerValue: {
              sharePrice: sharePrice,
              changePercentage: marketCap,
              totalAssets: totalAssets,
              pToE: pToE,
              eps: eps,
              ev: ev,
              evByEbitda: evByEbitda
            }
          });
          var sorted = datesCF;
          sorted = sorted.sort(function(obj1, obj2) {
            return (
              obj2.tickerValue.changePercentage -
              obj1.tickerValue.changePercentage
            );
          });
          var final_data = {};
          final_data = sorted;
        }
      }
      // }
    }
  }

  res.status(200).json({
    data: final_data,
    message: "Returned losers valuation details successfully"
  });
});

router.post("/losersmargin", async (req, res, next) => {
  // this query is used to get the gross, operating and net profit margin for the top losers
  let datesCF = [];
  let filterData = [];
  filterData.push({
    ticker_name: {
      $in: [
        "SRNE",
        "GTXI",
        "COTY",
        "MRAM",
        "NSSC",
        "PIR",
        "MIK",
        "FOSL",
        "CC",
        "FNKO"
      ]
    }
  });
  const result = await StocksData.aggregate([
    { $unwind: "$ticker_dates" },
    {
      // match works similar to find query.
      // Since the tickers are not any indices so we are specifying the isIndex parameter false
      $match: {
        isIndex: false,
        $and: filterData,

        "ticker_dates.date": {
          //  here we are passing the dates in which we want to search for the values of gross, operating and net profit margin.
          // these values are generated on a quaterly basis
          // as most of the tickers had all the required values so we have selected this quater
          $lte: new Date("2018-04-01"),
          $gte: new Date("2018-03-25")
        }
      }
    },
    {
      $project: {
        ticker_dates: 1,
        ticker_id: "$ticker_id",
        ticker_name: "$ticker_name",

        quarter: {
          $cond: [
            {
              $and: [
                { $eq: [{ $month: "$ticker_dates.date" }, 3] },
                { $lte: [{ $dayOfMonth: "$ticker_dates.date" }, 31] },
                { $gt: [{ $dayOfMonth: "$ticker_dates.date" }, 25] }
              ]
            },
            "first",
            // first => Jan to March
            {
              $cond: [
                {
                  $and: [
                    { $eq: [{ $month: "$ticker_dates.date" }, 6] },
                    { $lte: [{ $dayOfMonth: "$ticker_dates.date" }, 30] },
                    { $gt: [{ $dayOfMonth: "$ticker_dates.date" }, 25] }
                  ]
                },
                "second",
                // second quarter => Apr to June
                {
                  $cond: [
                    {
                      $and: [
                        { $eq: [{ $month: "$ticker_dates.date" }, 9] },
                        { $lte: [{ $dayOfMonth: "$ticker_dates.date" }, 30] },
                        { $gt: [{ $dayOfMonth: "$ticker_dates.date" }, 25] }
                      ]
                    },
                    "third",
                    //  third quarter => July to September
                    {
                      $cond: [
                        {
                          $and: [
                            { $eq: [{ $month: "$ticker_dates.date" }, 12] },
                            {
                              $lte: [{ $dayOfMonth: "$ticker_dates.date" }, 31]
                            },
                            { $gt: [{ $dayOfMonth: "$ticker_dates.date" }, 25] }
                          ]
                        },
                        "fourth",
                        // fourth quarter => Oct to December
                        "fifth"
                        //  fifth is passed because $cond requires 3 parameters and we had only 2.
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        }
      }
    },
    {
      $match: {
        // since fifth is not required we match it with $not equal to.
        // thus, it is not considered while grouping the results
        quarter: { $ne: "fifth" }
      }
    },
    {
      $group: {
        _id: {
          // grouping the results based on their year, month and ticker id and name
          year: { $year: "$ticker_dates.date" },
          month: { $month: "$ticker_dates.date" },
          ticker_name: "$ticker_name",
          ticker_id: "$ticker_id"
        },
        date_values: { $push: "$ticker_dates" }
      }
    },
    {
      $sort: {
        "_id.year": -1,
        "_id.month": -1
      }
    }
  ]);
  if (!result)
    return res.status(404).json({
      message: "Cannot find stock with the given ticker name"
    });
  else {
    for (let i of result) {
      for (dates of i.date_values) {
        // if the dates has operating margin
        // then, their values will be stored in their respective variables and then push into datesCF
        if (dates.hasOwnProperty("Operating Margin")) {
          var operatingMargin = dates["Operating Margin"];
          var netMargin = dates["Net Profit Margin"];
          var grossMargin = dates["Gross Margin"];
          datesCF.push({
            ticker_id: i._id.ticker_id,
            tickerName: i._id.ticker_name,
            tickerValue: {
              grossMargin: grossMargin,
              operatingMargin: operatingMargin,
              changePercentage: netMargin
            }
          });
          // sorting the data based on the chnage percentage and in descending order
          var sorted = datesCF;
          sorted = sorted.sort(function(obj1, obj2) {
            return (
              obj2.tickerValue.changePercentage -
              obj1.tickerValue.changePercentage
            );
          });
          var final_data = {};
          final_data = sorted;
        }
      }
    }
  }

  res.status(200).json({
    data: final_data,
    message: "Returned losers' margin details successfully"
  });
});
// ****************************************************************************************************************
// *****************************************************SECTORS****************************************************

router.get("/sectors", async (req, res, next) => {
  // this query is used to get all the sectors present in the database
  try {
    let companies = [];
    var data = { tickerValue: {} };
    let result = await StocksData.aggregate(
      [
        {
          $match: {
            // since the sectors are not indices, we specify their isIndex as false
            isIndex: false,
            "ticker_dates.1": { $exists: true }
          }
        },
        {
          $project: {
            sector: 1,
            industry: 1,
            ticker_data: {
              ticker_name: "$ticker_name",
              ticker_id: "$ticker_id",
              //  we need their last 2 days's share price and market cap values
              // thus we are traversing to their last and second last day
              secondLast: { $arrayElemAt: ["$ticker_dates", -2] },
              last: { $arrayElemAt: ["$ticker_dates", -1] }
            }
          }
        },
        {
          $group: {
            //  grouping the results based on their sectors
            _id: "$sector",
            //  since we also need to find the industries we are passing industries
            industries: { $addToSet: "$industry" },
            tickers_data: { $addToSet: "$ticker_data" }
          }
        }
      ],
      (err, docs) => {
        if (err) {
          console.log(err);
        }
        if (!docs)
          return res.status(404).json({
            message: "Cannot find any stock"
          });
        else {
          var sector_details = [];
          for (const each_sector in docs) {
            let total_market_cap = 0;
            let total_change_last = 0;
            let total_change_second_last = 0;
            let total_change_percent = 0;
            for (const each_date of docs[each_sector].tickers_data) {
              // if the last date has market cap only then its value will be appended and stored in total market cap
              if (each_date.last.hasOwnProperty("Market Capitalisation")) {
                total_market_cap += each_date.last["Market Capitalisation"];
              }

              if (
                // if the last date has share price
                // only then its value will be appended and stored in total last and second last
                each_date.last.hasOwnProperty("Share Price") &&
                each_date.secondLast.hasOwnProperty("Share Price")
              ) {
                total_change_last += each_date.last["Share Price"];
                total_change_second_last += each_date.secondLast["Share Price"];
              }
            }
            //calculating the change and change percentage
            let total_change =
              (Math.round(total_change_last - total_change_second_last) *
                10000) /
              10000;
            total_change_percent =
              Math.round((total_change / total_change_last) * 100 * 10000) /
              10000;

            sector_details.push({
              tickerName: docs[each_sector]._id,
              tickerValue: {
                industry_count: docs[each_sector].industries.length,
                //  to get the number of industries in a particular sector, we are finding the length of the industries
                marketCap: (Math.round(total_market_cap) * 100) / 100,
                change: total_change,
                changePercentage: total_change_percent
              }
            });
          }
          // sorting the sectors based on their change percentage
          var sorted_sectors = sector_details;
          sorted_sectors = sorted_sectors.sort(function(obj1, obj2) {
            return (
              obj2.tickerValue.changePercentage -
              obj1.tickerValue.changePercentage
            );
          });
          res.status(200).json({
            data: sorted_sectors,
            message: "Returned sectors details successfully"
          });
        }
      }
    );
  } catch (err) {
    next(err);
  }
});

router.get("/sectorperformance", async (req, res, next) => {
  //  this query is used to get the perfromance of a sector in 3 months, 6 months and in a year
  try {
    let companies = [];
    var data = { tickerValue: {} };
    let result = await StocksData.aggregate(
      [
        {
          $match: {
            isIndex: false,
            "ticker_dates.1": { $exists: true }
          }
        },
        {
          $project: {
            sector: 1,
            industry: 1,
            ticker_data: {
              ticker_name: "$ticker_name",
              ticker_id: "$ticker_id",
              secondLast: { $arrayElemAt: ["$ticker_dates", -2] },
              last: { $arrayElemAt: ["$ticker_dates", -1] },
              quarter: { $arrayElemAt: ["$ticker_dates", -10] },
              halfYear: { $arrayElemAt: ["$ticker_dates", -15] },
              Year: { $arrayElemAt: ["$ticker_dates", -25] }
            }
          }
        },
        {
          $group: {
            // we are grouping the results based on sectors and industries of that sector
            _id: "$sector",
            industries: { $addToSet: "$industry" },
            tickers_data: { $addToSet: "$ticker_data" }
          }
        }
      ],
      (err, docs) => {
        if (err) {
          console.log(err);
        }
        if (!docs)
          return res.status(404).json({
            message: "Cannot find any stock"
          });
        else {
          var sector_details = [];
          for (const each_sector in docs) {
            let total_market_cap = 0;
            let total_change_last = 0;
            let total_change_second_last = 0;
            let total_change_percent = 0;
            for (const each_date of docs[each_sector].tickers_data) {
              if (each_date.last.hasOwnProperty("Market Capitalisation")) {
                total_market_cap += each_date.last["Market Capitalisation"];
              }

              if (
                each_date.last.hasOwnProperty("Share Price") &&
                each_date.secondLast.hasOwnProperty("Share Price")
              ) {
                total_change_last += each_date.last["Share Price"];
                total_change_second_last += each_date.secondLast["Share Price"];
              }
            }
            let total_change =
              (Math.round(total_change_last - total_change_second_last) *
                10000) /
              10000;
            total_change_percent =
              Math.round((total_change / total_change_last) * 100 * 10000) /
              10000;
            // **************************************************QUATERLY PERFORMANCE***************************************************
            let total_last = 0;
            let total_change_quarter = 0;
            let total_quarter_percent = 0;
            for (const each_date of docs[each_sector].tickers_data) {
              if (
                // if the dates has share price it will be stored in total_last and total_change_quarter
                // if share price is null or undefined total_last and total_change_quarter =0
                each_date.last["Share Price"] &&
                each_date.quarter["Share Price"] != undefined
              ) {
                total_last += each_date.last["Share Price"];
                total_change_quarter += each_date.quarter["Share Price"];
              } else {
                total_last = 0;
                total_change_quarter = 0;
              }
            }
            //  total change for that quarter and its percentage is calculated by the following formula
            if (total_change_quarter && total_last == null) {
              total_change_quarter = 0;
              total_last = 0;
            }
            let totalChangeForQuarter =
              (Math.round(total_last - total_change_quarter) * 10000) / 10000;
            if (totalChangeForQuarter != null || total_last != null) {
              total_quarter_percent =
                Math.round((totalChangeForQuarter / total_last) * 100 * 10000) /
                10000;
            } else {
              total_quarter_percent = "--";
            }

            // ******************************************6 MONTHS************************************************
            let total_half_yearly_last = 0;
            let total_change_halfYear = 0;
            let total_halfYear_percent = 0;
            for (const each_date of docs[each_sector].tickers_data) {
              if (
                //  if the dates has share price it will be stored in total_halfyearly_last and total_change_halfYear
                // if share price is null or undefined total_halfyearly_last and total_change_halfYear =0
                each_date.last["Share Price"] &&
                each_date.halfYear["Share Price"] != undefined
              ) {
                total_half_yearly_last += each_date.last["Share Price"];
                total_change_halfYear += each_date.halfYear["Share Price"];
              } else {
                total_half_yearly_last = 0;
                total_change_halfYear = 0;
              }
            }
            //  total change for 6 months and its percentage is calculated by the following formula

            if (total_change_halfYear && total_half_yearly_last == null) {
              total_change_halfYear = 0;
              total_half_yearly_last = 0;
            }
            let totalChangeForhalfYear =
              (Math.round(total_half_yearly_last - total_change_halfYear) *
                10000) /
              10000;
            if (
              totalChangeForhalfYear != null ||
              total_half_yearly_last != null
            ) {
              total_halfYear_percent =
                Math.round(
                  (totalChangeForhalfYear / total_half_yearly_last) *
                    100 *
                    10000
                ) / 10000;
            } else {
              total_halfYear_percent = "--";
            }

            // ******************************************** ONE YEAR ******************************************
            let total_yearly_last = 0;
            let total_change_Year = 0;
            let total_Year_percent = 0;
            for (const each_date of docs[each_sector].tickers_data) {
              if (
                //  if the dates has share price it will be stored in total_yearly_last and total_change_Year
                // if share price is null or undefined total_yearly_last and total_change_Year =0
                each_date.last["Share Price"] &&
                each_date.Year["Share Price"] != undefined
              ) {
                total_yearly_last += each_date.last["Share Price"];
                total_change_Year += each_date.Year["Share Price"];
              } else {
                total_yearly_last = 0;
                total_change_Year = 0;
              }
            }
            //  total change for an year and its percentage is calculated by the following formula

            if (total_change_Year && total_yearly_last == null) {
              total_change_Year = 0;
              total_yearly_last = 0;
            }
            let totalChangeForYear =
              (Math.round(total_yearly_last - total_change_Year) * 10000) /
              10000;
            if (totalChangeForYear != null || total_yearly_last != null) {
              total_Year_percent =
                Math.round(
                  (totalChangeForYear / total_yearly_last) * 100 * 10000
                ) / 10000;
            } else {
              total_Year_percent = "--";
            }

            sector_details.push({
              // isIndex:docs[],
              tickerName: docs[each_sector]._id,
              tickerValue: {
                changePercentage: total_change_percent,
                total_quarter_percent: total_quarter_percent,
                total_halfYear_percent: total_halfYear_percent,
                total_Year_percent: total_Year_percent
              }
            });
          }
          var sorted_sectors = sector_details;
          sorted_sectors = sorted_sectors.sort(function(obj1, obj2) {
            return (
              obj2.tickerValue.changePercentage -
              obj1.tickerValue.changePercentage
            );
          });

          res.status(200).json({
            data: sorted_sectors,
            message: "Returned sectors' performance details successfully"
          });
        }
      }
    );
  } catch (err) {
    next(err);
  }
});
router.get("/industry", async (req, res, next) => {
  try {
    let companies = [];
    var data = { tickerValue: {} };
    let result = await StocksData.aggregate(
      [
        {
          $match: {
            isIndex: false,
            "ticker_dates.1": { $exists: true }
          }
        },
        {
          $project: {
            sector: 1,
            industry: 1,
            ticker_data: {
              ticker_name: "$ticker_name",
              ticker_id: "$ticker_id",
              secondLast: { $arrayElemAt: ["$ticker_dates", -2] },
              last: { $arrayElemAt: ["$ticker_dates", -1] }
            }
          }
        },
        {
          $group: {
            _id: "$industry",
            sectors: { $addToSet: "$sector" },
            tickers_data: { $addToSet: "$ticker_data" }
          }
        }
      ],
      (err, docs) => {
        if (err) {
          console.log(err);
        }
        if (!docs)
          return res.status(404).json({
            message: "Cannot find any stock"
          });
        else {
          var industry_details = [];
          for (const each_industry in docs) {
            let total_market_cap = 0;
            let total_change_last = 0;
            let total_change_second_last = 0;
            let total_change_percent = 0;
            for (const each_date of docs[each_industry].tickers_data) {
              if (each_date.last.hasOwnProperty("Market Capitalisation")) {
                total_market_cap += each_date.last["Market Capitalisation"];
              }

              if (
                each_date.last.hasOwnProperty("Share Price") &&
                each_date.secondLast.hasOwnProperty("Share Price")
              ) {
                total_change_last += each_date.last["Share Price"];
                total_change_second_last += each_date.secondLast["Share Price"];
              }
            }
            let total_change =
              (Math.round(total_change_last - total_change_second_last) *
                10000) /
              10000;
            total_change_percent =
              Math.round((total_change / total_change_last) * 100 * 10000) /
              10000;

            industry_details.push({
              tickerName: docs[each_industry]._id,
              tickerValue: {
                marketCap: (Math.round(total_market_cap) * 100) / 100,
                change: total_change,
                changePercentage: total_change_percent,
                sectorName: docs[each_industry].sectors[0]
              }
            });
            var sorted_industries = industry_details;
            sorted_industries = sorted_industries.sort(function(obj1, obj2) {
              return (
                obj2.tickerValue.changePercentage -
                obj1.tickerValue.changePercentage
              );
            });
          }
          sorted_industries = sorted_industries.slice(0, 10);

          // companies.push(industry_details);
          res.status(200).json({
            data: sorted_industries,
            message: "Returned industries details successfully"
          });
        }
      }
    );
  } catch (err) {
    next(err);
  }
});

router.get("/industryperformance", async (req, res, next) => {
  try {
    let companies = [];
    var data = { tickerValue: {} };
    let result = await StocksData.aggregate(
      [
        {
          $match: {
            isIndex: false,
            "ticker_dates.1": { $exists: true }
          }
        },
        {
          $project: {
            sector: 1,
            industry: 1,
            ticker_data: {
              ticker_name: "$ticker_name",
              ticker_id: "$ticker_id",
              secondLast: { $arrayElemAt: ["$ticker_dates", -2] },
              last: { $arrayElemAt: ["$ticker_dates", -1] },
              quarter: { $arrayElemAt: ["$ticker_dates", -10] },
              halfYear: { $arrayElemAt: ["$ticker_dates", -15] },
              Year: { $arrayElemAt: ["$ticker_dates", -25] }
            }
          }
        },
        {
          $group: {
            _id: "$industry",
            sectors: { $addToSet: "$sector" },
            tickers_data: { $addToSet: "$ticker_data" }
          }
        }
      ],
      (err, docs) => {
        if (err) {
          console.log(err);
        }
        if (!docs)
          return res.status(404).json({
            message: "Cannot find any stock"
          });
        else {
          var industry_details = [];
          for (const each_industry in docs) {
            // **************************************************OVERVIEW***************************************************
            let total_market_cap = 0;
            let total_change_last = 0;
            let total_change_second_last = 0;
            let total_change_percent = 0;
            for (const each_date of docs[each_industry].tickers_data) {
              if (each_date.last.hasOwnProperty("Market Capitalisation")) {
                total_market_cap += each_date.last["Market Capitalisation"];
              }

              if (
                each_date.last.hasOwnProperty("Share Price") &&
                each_date.secondLast.hasOwnProperty("Share Price")
              ) {
                total_change_last += each_date.last["Share Price"];
                total_change_second_last += each_date.secondLast["Share Price"];
              }
            }
            let total_change =
              (Math.round(total_change_last - total_change_second_last) *
                10000) /
              10000;
            total_change_percent =
              Math.round((total_change / total_change_last) * 100 * 10000) /
              10000;
            // **************************************************PERFORMANCE***************************************************
            let total_last = 0;
            let total_change_quarter = 0;
            let total_quarter_percent = 0;
            for (const each_date of docs[each_industry].tickers_data) {
              if (
                each_date.last["Share Price"] &&
                each_date.quarter["Share Price"] != undefined
              ) {
                total_last += each_date.last["Share Price"];
                total_change_quarter += each_date.quarter["Share Price"];
              } else {
                total_last = 0;
                total_change_quarter = 0;
              }
            }
            if (total_change_quarter && total_last == null) {
              total_change_quarter = 0;
              total_last = 0;
            }
            let totalChangeForQuarter =
              (Math.round(total_last - total_change_quarter) * 10000) / 10000;
            total_quarter_percent =
              Math.round((totalChangeForQuarter / total_last) * 100 * 10000) /
              10000;
            // ******************************************6 MONTHS************************************************
            let total_half_yearly_last = 0;
            let total_change_halfYear = 0;
            let total_halfYear_percent = 0;
            for (const each_date of docs[each_industry].tickers_data) {
              if (
                each_date.last["Share Price"] &&
                each_date.halfYear["Share Price"] != undefined
              ) {
                total_half_yearly_last += each_date.last["Share Price"];
                total_change_halfYear += each_date.halfYear["Share Price"];
              } else {
                total_half_yearly_last = 0;
                total_change_halfYear = 0;
              }
            }
            if (total_change_halfYear && total_half_yearly_last == null) {
              total_change_halfYear = 0;
              total_half_yearly_last = 0;
            }
            let totalChangeForhalfYear =
              (Math.round(total_half_yearly_last - total_change_halfYear) *
                10000) /
              10000;
            total_halfYear_percent =
              Math.round(
                (totalChangeForhalfYear / total_half_yearly_last) * 100 * 10000
              ) / 10000;

            // ******************************************** ONE YEAR ******************************************
            let total_yearly_last = 0;
            let total_change_Year = 0;
            let total_Year_percent = 0;
            for (const each_date of docs[each_industry].tickers_data) {
              if (
                each_date.last["Share Price"] &&
                each_date.Year["Share Price"] != undefined
              ) {
                total_yearly_last += each_date.last["Share Price"];
                total_change_Year += each_date.Year["Share Price"];
              } else {
                total_yearly_last = 0;
                total_change_Year = 0;
              }
            }
            if (total_change_Year && total_yearly_last == null) {
              total_change_Year = 0;
              total_yearly_last = 0;
            }
            let totalChangeForYear =
              (Math.round(total_yearly_last - total_change_Year) * 10000) /
              10000;
            total_Year_percent =
              Math.round(
                (totalChangeForYear / total_yearly_last) * 100 * 10000
              ) / 10000;
            industry_details.push({
              tickerName: docs[each_industry]._id,
              tickerValue: {
                changePercentage: total_change_percent,
                total_quarter_percent: total_quarter_percent,
                total_halfYear_percent: total_halfYear_percent,
                total_Year_percent: total_Year_percent
              }
            });
            var sorted_industries = industry_details;
            sorted_industries = sorted_industries.sort(function(obj1, obj2) {
              return (
                obj2.tickerValue.changePercentage -
                obj1.tickerValue.changePercentage
              );
            });
          }
          sorted_industries = sorted_industries.slice(0, 10);

          res.status(200).json({
            data: sorted_industries,
            message: "Returned industries performance details successfully"
          });
        }
      }
    );
  } catch (err) {
    next(err);
  }
});

module.exports = router;
