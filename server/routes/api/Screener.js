const express = require("express");
const router = express.Router();
const Stocks = require("../../models/stocksModel");

router.get("/getIndustriesBySector", async (req, res, next) => {
  try {
    let result = await Stocks.aggregate([
      {
        $match: { isIndex: false }
      },
      {
        $project: {
          sector: 1,
          industry: 1
        }
      },
      {
        $group: {
          _id: "$sector",
          industries: { $addToSet: "$industry" }
        }
      }
    ]);
    if (result.length === 0)
      throw {
        statusCode: 404,
        customMessage: "No sectors or industries present"
      };
    let industriesBySector = [];
    for (let i = 0; i < result.length; i++) {
      let industryObj = { sector: {}, industries: [] };
      industryObj.sector.name = result[i]._id;
      industryObj.sector.isChecked = false;
      // console.log(result[i]);
      for (let j = 0; j < result[i].industries.length; j++) {
        industryObj.industries.push({
          name: result[i].industries[j],
          isChecked: false
        });
      }
      industriesBySector.push(industryObj);
    }
    res.json({
      status: 200,
      data: industriesBySector,
      message: "Sectors and industries data retrieved successfully"
    });
  } catch (err) {
    next(err);
  }
});

router.post("/getTickersByFilter", async (req, res, next) => {
  try {
    // checks if required params are present or not, if not throws an error
    // with status code of 400 and a message as No filters to fetch stocks details.
    if (!req.body.sectors || !req.body.industries)
      throw {
        statusCode: "400",
        customMessage: "No filters to fetch stocks details"
      };

    let match = {
      $match: { isIndex: false, "ticker_dates.1": { $exists: true } }
    };
    let project = {
      $project: {
        sector: 1,
        industry: 1,
        ticker_data: {
          ticker_name: "$ticker_name",
          ticker_id: "$ticker_id",
          secondLast: { $arrayElemAt: ["$ticker_dates", -2] },
          last: { $arrayElemAt: ["$ticker_dates", -1] }
        }
      }
    };
    // checks if required params length is 0, if yes then fetch all the
    // tickers/companies from the database if not then works by filter
    if (req.body.sectors.length === 0 && req.body.industries.length === 0) {
      filterMessage = "Retrieved companies details successfully for no filter";
    } else {
      let filterData = [];
      req.body.sectors.length !== 0 &&
        filterData.push({ sector: { $in: req.body.sectors } });
      req.body.industries.length !== 0 &&
        filterData.push({ industry: { $in: req.body.industries } });
      match["$match"]["$and"] = filterData;
      filterMessage = "Retrieved companies details successfully with filter";
    }
    let query = [];
    query.push(match);
    // query.push(limit);
    query.push(project);

    // runs the query with given params
    let result = await Stocks.aggregate(query);

    // checks if result length is 0, if yes then throws an error with status
    // code of 404 and message of No data found for companies
    if (result.length === 0)
      throw {
        statusCode: "404",
        customMessage: "No data found for companies"
      };

    // adds change and changePercentage in result array inside ticker_data
    for (let i = 0; i < result.length; i++) {
      let change =
        result[i].ticker_data.last["Share Price"] -
        result[i].ticker_data.secondLast["Share Price"];
      result[i].ticker_data.change = Math.round(change * 100) / 100;
      result[i].ticker_data.changePercentage =
        Math.round(
          (change / result[i].ticker_data.last["Share Price"]) * 100 * 100
        ) / 100;
    }

    // sends the response to client with status code of 200, data same as result
    res.json({
      status: 200,
      data: result,
      message: filterMessage
    });
  } catch (err) {
    console.log(err);
    next(err);
  }
});

// router.post("/getTickersByFilter", async (req, res, next) => {
//   try {
//     let maxevebitda = req.body.evebitda >= 95 ? 100 : req.body.evebitda + 5;
//     let maxgrowth = (req.body.growth >= 95 ? 100 : req.body.growth + 5) * 1000;
//     let maxmktcap = (req.body.mktcap >= 95 ? 100 : req.body.mktcap + 5) * 10000;
//     let maxprofit = (req.body.profit >= 95 ? 100 : req.body.profit + 5) * 125;
//     let maxassets = (req.body.assets >= 95 ? 100 : req.body.assets + 5) * 10000;

//     let lessevebitda = req.body.evebitda <= 10 ? 5 : req.body.evebitda - 5;
//     let lessgrowth = (req.body.growth <= 10 ? 5 : req.body.growth - 5) * 1000;
//     let lessmktcap = (req.body.mktcap <= 10 ? 5 : req.body.mktcap - 5) * 10000;
//     let lessprofit = (req.body.profit <= 10 ? 5 : req.body.profit - 5) * 125;
//     let lessassets = (req.body.assets <= 10 ? 5 : req.body.assets - 5) * 10000;

//     let currentYear = new Date().getFullYear().toString();

//     console.log("maxmktcap", maxmktcap);
//     console.log("minmktcap", lessmktcap);
//     // checks if required params are present or not, if not throws an error
//     // with status code of 400 and a message as No filters to fetch stocks details.
//     if (!req.body.sectors || !req.body.industries)
//       throw {
//         statusCode: "400",
//         customMessage: "No filters to fetch stocks details"
//       };

//     let match = {
//       $match: {
//         sector: { $in: ["Technology"] },

//         "ticker_dates.date": {
//           $gte: new Date(currentYear + "-01-01"),
//           $lte: new Date(currentYear + "-12-01")
//         },
//         // "ticker_dates.Revenues": {
//         //   $gte: lessgrowth,
//         //   $lte: maxgrowth
//         // },
//         // "ticker_dates.Total Assets": {
//         //   $gte: lessassets,
//         //   $lte: maxassets
//         // },
//         // "ticker_dates.EV / EBITDA": {
//         //   $gte: lessevebitda,
//         //   $lte: maxevebitda
//         // },
//         // "ticker_dates.Net Profit": {
//         //   $gte: lessprofit,
//         //   $lte: maxprofit
//         // },
//         "ticker_dates.Market Capitalisation": {
//           $gte: lessmktcap,
//           $lte: maxmktcap
//         }
//       }
//     };
//     let project = {
//       $project: {
//         sector: 1,
//         industry: 1,
//         ticker_dates: 1,
//         ticker_name: 1,
//         ticker_id: 1,
//         company_name: 1
//       }
//     };
//     // checks if required params length is 0, if yes then fetch all the
//     // tickers/companies from the database if not then works by filter
//     if (req.body.sectors.length === 0 && req.body.industries.length === 0) {
//       filterMessage = "Retrieved companies details successfully for no filter";
//     } else {
//       let filterData = [];
//       req.body.sectors.length !== 0 &&
//         filterData.push({ sector: { $in: req.body.sectors } });
//       req.body.industries.length !== 0 &&
//         filterData.push({ industry: { $in: req.body.industries } });
//       match["$match"]["$and"] = filterData;
//       filterMessage = "Retrieved companies details successfully with filter";
//     }
//     let query = [];
//     query.push({ $unwind: "$ticker_dates" });
//     query.push(match);
//     query.push(project);

//     // runs the query with given params
//     let result = await Stocks.aggregate(query);

//     // checks if result length is 0, if yes then throws an error with status
//     // code of 404 and message of No data found for companies
//     if (result.length === 0)
//       throw {
//         statusCode: "404",
//         customMessage: "No data found for companies"
//       };

//     // let tickerData = [];
//     // // adds change and changePercentage in result array inside ticker_data
//     // for (let i = 0; i < result.length; i++) {
//     //   let growthCount = 0;
//     //   let profitCount = 0;
//     //   let evebitdaCount = 0;
//     //   let mktcapCount = 0;
//     //   let assetsCount = 0;
//     //   let growth = 0;
//     //   let profit = 0;
//     //   let evebitda = 0;
//     //   let mktcap = 0;
//     //   let assets = 0;
//     //   let priceCount = 2;
//     //   let lastPrice;
//     //   let secondPrice;
//     //   for (let j = result[i].ticker_dates.length - 1; j >= 0; j--) {
//     //     if (
//     //       result[i].ticker_dates[j].hasOwnProperty("Market Capitalisation") &&
//     //       result[i].ticker_dates[j]["Market Capitalisation"] > mktcap
//     //     ) {
//     //       mktcap = result[i].ticker_dates[j]["Market Capitalisation"];
//     //       // mktcapCount += 1;
//     //     }
//     //     if (result[i].ticker_dates[j].hasOwnProperty("Total Assets")) {
//     //       assets += result[i].ticker_dates[j]["Total Assets"];
//     //       assetsCount += 1;
//     //     }
//     //     if (result[i].ticker_dates[j].hasOwnProperty("Revenues")) {
//     //       growth += result[i].ticker_dates[j]["Revenues"];
//     //       growthCount += 1;
//     //     }
//     //     if (result[i].ticker_dates[j].hasOwnProperty("EV / EBITDA")) {
//     //       evebitda += result[i].ticker_dates[j]["EV / EBITDA"];
//     //       evebitdaCount += 1;
//     //     }
//     //     if (result[i].ticker_dates[j].hasOwnProperty("Net Profit")) {
//     //       profit += result[i].ticker_dates[j]["Net Profit"];
//     //       profitCount += 1;
//     //     }
//     //     if (
//     //       priceCount === 2 &&
//     //       result[i].ticker_dates[j].hasOwnProperty("Share Price")
//     //     ) {
//     //       lastPrice = result[i].ticker_dates[j]["Share Price"];
//     //       priceCount--;
//     //     } else if (
//     //       priceCount === 1 &&
//     //       result[i].ticker_dates[j].hasOwnProperty("Share Price")
//     //     ) {
//     //       secondPrice = result[i].ticker_dates[j]["Share Price"];
//     //       priceCount--;
//     //     }
//     //   }
//     //   mktcap = Math.round(mktcap / 1000);
//     //   assets = Math.round(assets / assetsCount / 1000);
//     //   growth = Math.round(growth / growthCount / 1000);
//     //   profit = Math.round(profit / profitCount / 125);
//     //   evebitda = Math.round(evebitda / evebitdaCount);
//     //   let dataPointsArray = [evebitda, growth, mktcap, profit, assets];
//     //   let change = lastPrice - secondPrice;

//     //   change = Math.round(change * 100) / 100;
//     //   let changePercentage = Math.round((change / lastPrice) * 100 * 100) / 100;
//     //   tickerData.push({
//     //     tickerId: result[i].ticker_id,
//     //     tickerName: result[i].ticker_name,
//     //     companyName: result[i].company_name,
//     //     sector: result[i].sector,
//     //     industry: result[i].industry,
//     //     change: change,
//     //     changePercentage: changePercentage,
//     //     dataPointsArray: dataPointsArray
//     //   });
//     // }

//     console.log("found " + result.length + " companies");
//     // sends the response to client with status code of 200, data same as result
//     res.json({
//       status: 200,
//       data: result,
//       message: filterMessage
//     });
//   } catch (err) {
//     console.log(err);
//     next(err);
//   }
// });

module.exports = router;
