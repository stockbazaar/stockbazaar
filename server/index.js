const express = require("express");
const cors = require("cors");
const logger = require("morgan");
const error = require("./middleware/error");
const bodyParser = require("body-parser");
const config = require("config");
const fs = require("fs");
const path = require("path");
const user = require("./routes/api/User");
const stockspecific = require("./routes/api/StockSpecific");
const stocks = require("./routes/api/Stocks");
const search = require("./routes/api/Search");
const upload = require("./routes/api/Upload");
const screener = require("./routes/api/Screener");

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use((err, req, res, next) => {
  next(error(err, req, res, next));
});
//storing logs in file system
// app.use(
//   logger("common", {
//     stream: fs.createWriteStream(path.join(__dirname, "./logs/access.log"), {
//       flags: "a"
//     })
//   })
// );
app.use(logger("common"));
const mongoURL = config.get("mongoURL");
const mongoose = require("mongoose");
const mg = "mongodb://localhost:27017/stocks_dummy";
mongoose
  .connect(mongoURL, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true
  })
  .catch(err => {
    console.error("connection error", err);
  });

var db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.on("connected", function() {
  console.log("connected to data server");
});

app.use("/api/user/", user);
app.use("/api/stockslanding/", stocks);
app.use("/api/stockspecific/", stockspecific);
app.use("/api/admin/", upload);
app.use("/api/stocks/", stocks);
app.use("/api/stockspecific/", stockspecific);
app.use("/api/search/", search);
app.use("/api/screener/", screener);
app.use(error);

const port = process.env.PORT || 5000;
if (process.env.NODE_ENV !== "test")
  app.listen(port, () => console.log(`server is listening on port ${port}`));

module.exports = app;
