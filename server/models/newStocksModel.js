var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var newStocks = new Schema(
  {
    ticker_id: Number,
    ticker_name: String,
    company_name: String,
    simfin_Id: Number,
    ticker_date: Date,
    isIndex: Boolean
  },
  { collection: "new_tickers_data" }
);

var newStocksData = mongoose.model("new_tickers_data", newStocks);
module.exports = newStocksData;
