const mongoose = require("mongoose");
//mongoose schema for user
var UserSchema = new mongoose.Schema({
  name: { type: String, required: true },
  password: { type: String, required: true },
  email: { type: String, required: true, unique: true },
  isAdmin: { type: Boolean, required: true, default: false }
});
//model User created with UserSchema and collection Name as User
var User = mongoose.model("User", UserSchema);
module.exports = User;
