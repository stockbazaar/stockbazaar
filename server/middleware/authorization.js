const jwttoken = require("./token");
module.exports = (req, res, next) => {
  // retrieve token from header

  let bearerheader =
    req.headers["x-access-token"] || req.headers["authorization"];

  //   check if there is a token present or not
  if (!bearerheader)
    return res.status(400).json({
      login: "failed",
      message: " Token not found"
    });
  // if a token is found then verify if the token is valid

  const usertoken = bearerheader.split(" ")[1];
  try {
    jwttoken.verify(usertoken);
    req.user = jwttoken.decode(usertoken);

    next();
  } catch (err) {
    res.json({
      status: 400,
      message: "Invalid token"
    });
    next(err);
  }
};
