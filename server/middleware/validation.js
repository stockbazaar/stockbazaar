const Joi = require("joi");

module.exports = userinfo => {
  const UserSchema = {
    name: Joi.string()
      .min(3)
      .max(30)
      .required(),

    password: Joi.string()
      .min(6)
      .max(20)
      .required(),

    email: Joi.string()
      .min(3)
      .max(30)
      .required()
      .email(),
    isAdmin: Joi.boolean().default(false)
  };
  return Joi.validate(userinfo, UserSchema);
};
