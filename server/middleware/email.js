const nodemailer = require("nodemailer");
const config = require("config");
const stockmailID = config.get("mailID");
const mailpass = config.get("mailpass");

const emailContent = (otp, emailid) => {
  return (
    "<b>Verify your account using OTP. Do not share it with anyone </b>" + otp
  );
};

module.exports = async function(otp, emailid) {
  let transporter = nodemailer.createTransport({
    service: "gmail",
    secure: false,
    auth: {
      user: stockmailID,
      pass: mailpass
    }
  });
  const content = emailContent(otp, emailid);
  console.log(emailid);
  await transporter.sendMail({
    from: "Stockbazaar",
    to: emailid,
    subject: "Email Verification for stockbazaar",
    text: " Verification OTP",
    html: content
  });
  console.log(otp);
};
