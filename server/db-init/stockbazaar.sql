drop database if exists stockbazaar;
create database stockbazaar;


\c stockbazaar;

-- Creating the structure [Eli]
create table simfin
(
    ticker varchar(50),
    simfinid int,
    companyid integer,
    indicatorname varchar(500),
    dates date,
    indicatorvalue double precision
);

--Loading the csv file on the created table
-- Note: Change the path location of your csv file
-- in the FROM section [Eli]
COPY simfin
(ticker,simfinid,companyid,indicatorname, dates, indicatorvalue) 
FROM 'C:\Users\Eliyahoo\Downloads\finalstocks\finalstocks.csv' DELIMITER ';' CSV HEADER;

-- Creating Sectors Column [Eli]
ALTER TABLE simfin ADD COLUMN sector varchar
(255);

-- Creating Company Column [Eli]
ALTER TABLE simfin ADD COLUMN industry varchar
(255);

 
 -- Updated ticker name to NA where the ticker value is null [Eli] 
UPDATE simFin set ticker='NA' where ticker is null;

UPDATE simfin SET sector = 'Industrials' WHERE CAST(companyid AS TEXT) like '100%';
UPDATE simfin SET sector = 'Technology' WHERE CAST(companyid AS TEXT) like '101%';
UPDATE simfin SET sector = 'Consumer Defensive' WHERE CAST(companyid AS TEXT) like '102%';
UPDATE simfin SET sector = 'Consumer Cyclical' WHERE CAST(companyid AS TEXT) like '103%';
UPDATE simfin SET sector = 'Financial Services' WHERE CAST(companyid AS TEXT) like '104%';
UPDATE simfin SET sector = 'Utilities' WHERE CAST(companyid AS TEXT) like '105%';
UPDATE simfin SET sector = 'Healthcare' WHERE CAST(companyid AS TEXT) like '106%';
UPDATE simfin SET sector = 'Energy' WHERE CAST(companyid AS TEXT) like '107%';
UPDATE simfin SET sector = 'Business Services' WHERE CAST(companyid AS TEXT) like '108%';
UPDATE simfin SET sector = 'Real Estate' WHERE CAST(companyid AS TEXT) like '109%';
UPDATE simfin SET sector = 'Basic Materials' WHERE CAST(companyid AS TEXT) like '110%';
UPDATE simfin SET sector = 'Other' WHERE CAST(companyid AS TEXT) like '0%';
UPDATE simfin SET sector = 'Other' WHERE CAST(companyid AS TEXT) like '111%';
UPDATE simfin SET sector = 'Other' WHERE CAST(companyid AS TEXT) like '112%';


-- Creating Indexes on ticker, indicatorname, companyid and indicatorvalues [Eli]
create index ticker_index on simfin (ticker);
create index indicatorname_index on simfin (indicatorname);
create index dates_index on simfin (dates);
create index companyid_index on simfin (companyid)
create index simfinid_index on simfin (simfinid)


-- --  Loading data of Industry -> Industrials[Eli]
UPDATE simfin SET industry = 'Industrial Products' WHERE companyid = 100001;
UPDATE simfin SET industry = 'Business Services' WHERE companyid = 100002;
UPDATE simfin SET industry = 'Engineering & Construction' WHERE companyid = 100003;
UPDATE simfin SET industry = 'Waste Management' WHERE companyid = 100004;
UPDATE simfin SET industry = 'Industrial Distribution' WHERE companyid = 100005;
UPDATE simfin SET industry = 'Airlines' WHERE companyid = 100006;
UPDATE simfin SET industry = 'Consulting & Outsourcing' WHERE companyid = 100007;
UPDATE simfin SET industry = 'Aerospace & Defense' WHERE companyid = 100008;
UPDATE simfin SET industry = 'Farm & Construction Machinery' WHERE companyid = 100009;
UPDATE simfin SET industry = 'Transportation & Logistics' WHERE companyid = 100010;
UPDATE simfin SET industry = 'Employment Services' WHERE companyid = 100011;
UPDATE simfin SET industry = 'Truck Manufacturing' WHERE companyid = 100012;
UPDATE simfin SET industry = 'Conglomerates' WHERE companyid = 100013;

-- -- Loading data of Industry -> Technology[Eli]
UPDATE simfin SET industry = 'Computer Hardware' WHERE companyid = 101001;
UPDATE simfin SET industry = 'Online Media' WHERE companyid = 101002;
UPDATE simfin SET industry = 'Application Software' WHERE companyid = 101003;
UPDATE simfin SET industry = 'Semiconductors' WHERE companyid = 101004;
UPDATE simfin SET industry = 'Communication Equipment' WHERE companyid = 101005;

-- -- Loading data of Industry ->Consumer Defensive [Eli]
UPDATE simfin SET industry = 'Retail - Defensive' WHERE companyid = 102001;
UPDATE simfin SET industry = 'Consumer Packaged Goods' WHERE companyid = 102002;
UPDATE simfin SET industry = 'Tobacoo Products' WHERE companyid = 102003;
UPDATE simfin SET industry = 'Beverages - Alocholic' WHERE companyid = 102004;
UPDATE simfin SET industry = 'Beverages - Non-Alcoholic' WHERE companyid = 102005;
UPDATE simfin SET industry = 'Education' WHERE companyid = 102006;

-- Loading data of Industry -> Consumer Cyclical(103)[Eli] 
UPDATE simfin SET industry = 'Entertainment' WHERE companyid = 103001;
UPDATE simfin SET industry = 'Retail - Apparel & Specialty' WHERE companyid = 103002;
UPDATE simfin SET industry = 'Restaurants' WHERE companyid = 103003;
UPDATE simfin SET industry = 'Manufacturing - Apparel & Furniture' WHERE companyid = 103004;
UPDATE simfin SET industry = 'Autos' WHERE companyid = 103005;
UPDATE simfin SET industry = 'Advertising & Marketing Services' WHERE companyid = 103011;
UPDATE simfin SET industry = 'Homebuilding & Construction' WHERE companyid = 103013;
UPDATE simfin SET industry = 'Travel & Leisure' WHERE companyid = 103015;
UPDATE simfin SET industry = 'Packaging & Containers' WHERE companyid = 103018;
UPDATE simfin SET industry = 'Personal Services' WHERE companyid = 103020;
UPDATE simfin SET industry = 'Publishing' WHERE companyid = 103026;

--  Loading data of Industry -> Financial Services(104) [Eli]
UPDATE simfin SET industry = 'Asset Management' WHERE companyid = 104001;
UPDATE simfin SET industry = 'Banks' WHERE companyid = 104002;
UPDATE simfin SET industry = 'Brokers & Exchanges' WHERE companyid = 104003;
UPDATE simfin SET industry = 'Insurance - Life' WHERE companyid = 104004;
UPDATE simfin SET industry = 'Insurance' WHERE companyid = 104005;
UPDATE simfin SET industry = 'Insurance - Property & Casualty' WHERE companyid = 104006;
UPDATE simfin SET industry = 'Credit Services' WHERE companyid = 104007;
UPDATE simfin SET industry = 'Insurance - Specialty' WHERE companyid = 104013;

--  Loading data of Industry -> Utilities(105)[Eli]
UPDATE simfin SET industry = 'Utilities - Regulated' WHERE companyid = 105001;
UPDATE simfin SET industry = 'Utilities - Independent Power Pr' WHERE companyid = 105002;


-- -- Loading data of Industry -> Healthcare[Eli]
UPDATE simfin SET industry = 'Medical Diagnostics & Research' WHERE companyid = 106001;
UPDATE simfin SET industry = 'Biotechnology' WHERE companyid = 106002;
UPDATE simfin SET industry = 'Medical Instruments & Equipment' WHERE companyid = 106003;
UPDATE simfin SET industry = 'Medical Devices' WHERE companyid = 106004;
UPDATE simfin SET industry = 'Drug Manufacturers' WHERE companyid = 106005;
UPDATE simfin SET industry = 'Health Care Plans' WHERE companyid = 106006;
UPDATE simfin SET industry = 'Health Care Providers' WHERE companyid = 106011;
UPDATE simfin SET industry = 'Medical Distribution' WHERE companyid = 106014;

--  Loading data of Industry -> Energy(107)[Eli] 
UPDATE simfin SET industry = 'Oil & Gas - Refining & Marketing' WHERE companyid = 107001;
UPDATE simfin SET industry = 'Oil & Gas - E&P' WHERE companyid = 107002;
UPDATE simfin SET industry = 'Oil & Gas - Midstream' WHERE companyid = 107003;
UPDATE simfin SET industry = 'Oil & Gas - Services' WHERE companyid = 107004;
UPDATE simfin SET industry = 'Oil & Gas - Integrated' WHERE companyid = 107005;
UPDATE simfin SET industry = 'Oil & Gas - Drilling' WHERE companyid = 107006;

--  Loading data of Industry -> Business Services(108)
UPDATE simfin SET industry = 'Communication Services' WHERE companyid = 108001;

-- Loading data of Industry -> Real Estate(109)[Eli] 
UPDATE simfin SET industry = 'REITs' WHERE companyid = 109001;
UPDATE simfin SET industry = 'Real Estate Services' WHERE companyid = 109002;

--  Loading data of Industry -> Basic Materials(110) [Eli] 
UPDATE simfin SET industry = 'Chemicals' WHERE companyid = 110001;
UPDATE simfin SET industry = 'Forest Products' WHERE companyid = 110002;
UPDATE simfin SET industry = 'Agriculture' WHERE companyid = 110003;
UPDATE simfin SET industry = 'Metals & Mining' WHERE companyid = 110004;
UPDATE simfin SET industry = 'Building Materials' WHERE companyid = 110005;
UPDATE simfin SET industry = 'Coal' WHERE companyid = 110006;
UPDATE simfin SET industry = 'Steel' WHERE companyid = 110007;


--  Loading data of Industry -> Others(112) [Eli] 
UPDATE simfin SET industry = 'Others' WHERE companyid = 112001;

