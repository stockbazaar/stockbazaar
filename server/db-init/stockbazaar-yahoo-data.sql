

\c stockbazaar;

--[Eli] Creating the structure
create table yahoodata
(
    dates date,
    opening double precision,
    high double precision,
    low double precision,
    closing double precision,
    adjclose double precision,
    volume bigint,
    ticker varchar(50),
    isIndex boolean default False
);

COPY yahoodata
(dates,opening,high,low, closing, adjclose, volume) 
FROM 'C:\Users\Eliyahoo\Downloads\^DJI.csv' CSV HEADER;

UPDATE yahoodata SET ticker = 'DJI' WHERE ticker is null;
UPDATE yahoodata SET isindex='true' WHERE ticker = 'DJI';

COPY yahoodata
(dates,opening,high,low, closing, adjclose, volume) 
FROM 'C:\Users\Eliyahoo\Downloads\^GSPC.csv' CSV HEADER;

UPDATE yahoodata SET ticker = 'GSPC' WHERE ticker is null;
UPDATE yahoodata SET isindex='true' WHERE ticker = 'GSPC';

COPY yahoodata
(dates,opening,high,low, closing, adjclose, volume) 
FROM 'C:\Users\Eliyahoo\Downloads\^NDX.csv' CSV HEADER;

UPDATE yahoodata SET ticker = 'NDX' WHERE ticker is null;
UPDATE yahoodata SET isindex='true' WHERE ticker = 'NDX';

COPY yahoodata
(dates,opening,high,low, closing, adjclose, volume) 
FROM 'C:\Users\Eliyahoo\Downloads\^OEX.csv' CSV HEADER;

UPDATE yahoodata SET ticker = 'OEX' WHERE ticker is null;
UPDATE yahoodata SET isindex='true' WHERE ticker = 'OEX';

COPY yahoodata
(dates,opening,high,low, closing, adjclose, volume) 
FROM 'C:\Users\Eliyahoo\Downloads\^RUA.csv' CSV HEADER;

UPDATE yahoodata SET ticker = 'RUA' WHERE ticker is null;
UPDATE yahoodata SET isindex='true' WHERE ticker = 'RUA';

COPY yahoodata
(dates,opening,high,low, closing, adjclose, volume) 
FROM 'C:\Users\Eliyahoo\Downloads\^RUT.csv' CSV HEADER;

UPDATE yahoodata SET ticker = 'RUT' WHERE ticker is null;
UPDATE yahoodata SET isindex='true' WHERE ticker = 'RUT';

COPY yahoodata
(dates,opening,high,low, closing, adjclose, volume) 
FROM 'C:\Users\Eliyahoo\Downloads\ALGN.csv' CSV HEADER;

UPDATE yahoodata SET ticker = 'ALGN' WHERE ticker is null;

COPY yahoodata
(dates,opening,high,low, closing, adjclose, volume) 
FROM 'C:\Users\Eliyahoo\Downloads\ANTM.csv' CSV HEADER;

UPDATE yahoodata SET ticker = 'ANTM' WHERE ticker is null;

COPY yahoodata
(dates,opening,high,low, closing, adjclose, volume) 
FROM 'C:\Users\Eliyahoo\Downloads\BDX.csv' CSV HEADER;

UPDATE yahoodata SET ticker = 'BDX' WHERE ticker is null;

COPY yahoodata
(dates,opening,high,low, closing, adjclose, volume) 
FROM 'C:\Users\Eliyahoo\Downloads\BMRN.csv' CSV HEADER;

UPDATE yahoodata SET ticker = 'BMRN' WHERE ticker is null;

COPY yahoodata
(dates,opening,high,low, closing, adjclose, volume) 
FROM 'C:\Users\Eliyahoo\Downloads\CLR.csv' CSV HEADER;

UPDATE yahoodata SET ticker = 'CLR' WHERE ticker is null;

COPY yahoodata
(dates,opening,high,low, closing, adjclose, volume) 
FROM 'C:\Users\Eliyahoo\Downloads\CNC.csv' CSV HEADER;

UPDATE yahoodata SET ticker = 'CNC' WHERE ticker is null;

COPY yahoodata
(dates,opening,high,low, closing, adjclose, volume) 
FROM 'C:\Users\Eliyahoo\Downloads\CVS.csv' CSV HEADER;

UPDATE yahoodata SET ticker = 'CVS' WHERE ticker is null;

COPY yahoodata
(dates,opening,high,low, closing, adjclose, volume) 
FROM 'C:\Users\Eliyahoo\Downloads\EW.csv' CSV HEADER;

UPDATE yahoodata SET ticker = 'EW' WHERE ticker is null;

COPY yahoodata
(dates,opening,high,low, closing, adjclose, volume) 
FROM 'C:\Users\Eliyahoo\Downloads\ICLR.csv' CSV HEADER;

UPDATE yahoodata SET ticker = 'ICLR' WHERE ticker is null;

COPY yahoodata
(dates,opening,high,low, closing, adjclose, volume) 
FROM 'C:\Users\Eliyahoo\Downloads\JNJ.csv' CSV HEADER;

UPDATE yahoodata SET ticker = 'JNj' WHERE ticker is null;

COPY yahoodata
(dates,opening,high,low, closing, adjclose, volume) 
FROM 'C:\Users\Eliyahoo\Downloads\AAPL.csv' CSV HEADER;

UPDATE yahoodata SET ticker = 'AAPL' WHERE ticker is null;

COPY yahoodata
(dates,opening,high,low, closing, adjclose, volume) 
FROM 'C:\Users\Eliyahoo\Downloads\ADBE.csv' CSV HEADER;

UPDATE yahoodata SET ticker = 'ADBE' WHERE ticker is null;

COPY yahoodata
(dates,opening,high,low, closing, adjclose, volume) 
FROM 'C:\Users\Eliyahoo\Downloads\CRM.csv' CSV HEADER;

UPDATE yahoodata SET ticker = 'CRM' WHERE ticker is null;

COPY yahoodata
(dates,opening,high,low, closing, adjclose, volume) 
FROM 'C:\Users\Eliyahoo\Downloads\CSCO.csv' CSV HEADER;

UPDATE yahoodata SET ticker = 'CSCO' WHERE ticker is null;

COPY yahoodata
(dates,opening,high,low, closing, adjclose, volume) 
FROM 'C:\Users\Eliyahoo\Downloads\HP.csv' CSV HEADER;

UPDATE yahoodata SET ticker = 'HP' WHERE ticker is null;

COPY yahoodata
(dates,opening,high,low, closing, adjclose, volume) 
FROM 'C:\Users\Eliyahoo\Downloads\IBM.csv' CSV HEADER;

UPDATE yahoodata SET ticker = 'IBM' WHERE ticker is null;

COPY yahoodata
(dates,opening,high,low, closing, adjclose, volume) 
FROM 'C:\Users\Eliyahoo\Downloads\INTC.csv' CSV HEADER;

UPDATE yahoodata SET ticker = 'INTC' WHERE ticker is null;

COPY yahoodata
(dates,opening,high,low, closing, adjclose, volume) 
FROM 'C:\Users\Eliyahoo\Downloads\MSFT.csv' CSV HEADER;

UPDATE yahoodata SET ticker = 'MSFT' WHERE ticker is null;

COPY yahoodata
(dates,opening,high,low, closing, adjclose, volume) 
FROM 'C:\Users\Eliyahoo\Downloads\ORA.csv' CSV HEADER;

UPDATE yahoodata SET ticker = 'ORA' WHERE ticker is null;

COPY yahoodata
(dates,opening,high,low, closing, adjclose, volume) 
FROM 'C:\Users\Eliyahoo\Downloads\ORCL.csv' CSV HEADER;

UPDATE yahoodata SET ticker = 'ORCL' WHERE ticker is null;

create index yahoo_ticker_index on yahoodata (ticker)