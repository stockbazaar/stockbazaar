var MongoClient = require("mongodb").MongoClient;

MongoClient.connect("mongodb://localhost:27017").then(client => {
  async function mongoLoad(skipDoc) {
    let database = client.db("stockbazaar");
    let result = await database
      .collection("stocks_data_2")
      .aggregate([
        { $match: { isIndex: false } },
        { $skip: skipDoc },
        { $limit: 10 },
        { $unwind: "$ticker_dates" },
        {
          $project: {
            ticker_id: 1,
            ticker_name: 1,
            sector: 1,
            industry: 1,
            ticker_dates: 1,
            company_name: 1,
            ohlc_dates: 1,
            isIndex: 1,
            _id: 0
          }
        }
      ])
      .toArray();
    // console.log("result length", result.length);
    for (let i = 0; i < result.length; i++) {
      tObj = {};
      tObj.ticker_id = result[i].ticker_id;
      tObj.ticker_name = result[i].ticker_name;
      tObj.sector = result[i].sector;
      tObj.industry = result[i].industry;
      tObj.isIndex = result[i].isIndex;
      if (tObj.hasOwnProperty("company_name")) {
        tObj.company_name = result[i].company_name;
      }
      if (tObj.hasOwnProperty("company_name")) {
        tObj.company_name = result[i].company_name;
      }
      for (let key in result[i].ticker_dates) {
        tObj[`${key}`] = result[i].ticker_dates[`${key}`];
      }
      if (result[i].hasOwnProperty("ohlc_dates")) {
        for (let key in result[i].ohlc_dates) {
          tObj[`${key}`] = result[i].ohlc_dates[`${key}`];
        }
      }
      database.collection("new_data_2").insertOne(tObj, (err, res) => {
        if (err) {
          console.log(err);
        }
      });
    }
  }

  let q = 0;
  let count = 200;
  async function loadStarter() {
    if (q >= 2305) {
      return;
    }
    await mongoLoad(q);
    q += 10;
    if (count === q) {
      count += 200;
      console.log(count, "tickers added");
    }
    loadStarter();
  }

  loadStarter();
});
