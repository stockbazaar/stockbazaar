const path = require("path");
var fs = require("fs");

const Stocks = require("../models/stocksModel");

const sectors = {
  "100": "Industrials",
  "101": "Technology",
  "102": "Consumer Defensive",
  "103": "Consumer Cyclical",
  "104": "Financial Services",
  "105": "Utilities",
  "106": "Healthcare",
  "107": "Energy",
  "108": "Business Services",
  "109": "Real Estate",
  "110": "Basic Materials",
  "111": "Other",
  "112": "Other",
  "0": "Other"
};
const industries = {
  "100001": "Industrial Products",
  "100002": "Business Services",
  "100003": "Engineering & Construction",
  "100004": "Waste Management",
  "100005": "Industrial Distribution",
  "100006": "Airlines",
  "100007": "Consulting & Outsourcing",
  "100008": "Aerospace & Defense",
  "100009": "Farm & Construction Machinery",
  "100010": "Transportation & Logistics",
  "100011": "Employment Services",
  "100012": "Truck Manufacturing",
  "100013": "Conglomerates",
  "101001": "Computer Hardware",
  "101002": "Online Media",
  "101003": "Application Software",
  "101004": "Semiconductors",
  "101005": "Communication Equipment",
  "102001": "Retail - Defensive",
  "102002": "Consumer Packaged Goods",
  "102003": "Tobacoo Products",
  "102004": "Beverages - Alocholic",
  "102005": "Beverages - Non-Alcoholic",
  "102006": "Education",
  "103001": "Entertainment",
  "103002": "Retail - Apparel & Specialty",
  "103003": "Restaurants",
  "103004": "Manufacturing - Apparel & Furniture",
  "103005": "Autos",
  "103011": "Advertising & Marketing Services",
  "103013": "Homebuilding & Construction",
  "103015": "Travel & Leisure",
  "103018": "Packaging & Containers",
  "103020": "Personal Services",
  "103026": "Publishing",
  "104001": "Asset Management",
  "104002": "Banks",
  "104003": "Brokers & Exchanges",
  "104004": "Insurance - Life'",
  "104005": "Insurance",
  "104006": "Insurance - Property & Casualty",
  "104007": "Credit Services",
  "104013": "Insurance - Specialty",
  "105001": "Utilities - Regulated'",
  "105002": "Utilities - Independent Power Pr",
  "106001": "Medical Diagnostics & Research",
  "106002": "Biotechnology",
  "106003": "Medical Instruments & Equipment",
  "106004": "Medical Devices",
  "106005": "Drug Manufacturers",
  "106006": "Health Care Plans",
  "106011": "Health Care Providers",
  "106014": "Medical Distribution",
  "107001": "Oil & Gas - Refining & Marketing",
  "107002": "Oil & Gas - E&P",
  "107003": "Oil & Gas - Midstream",
  "107004": "Oil & Gas - Services",
  "107005": "Oil & Gas - Integrated",
  "107006": "Oil & Gas - Drilling",
  "108001": "Communication Services",
  "109001": "REITs",
  "109002": "Real Estate Services",
  "110001": "Chemicals",
  "110002": "Forest Products",
  "110003": "Agriculture",
  "110004": "Metals & Mining",
  "110005": "Building Materials",
  "110006": "Coal",
  "110007": "Steel",
  "112001": "Others"
};
// const mongoose = require("mongoose");
// const mg = "mongodb://localhost:27017/stockbazaar";
// mongoose
//   .connect(mg, {
//     useNewUrlParser: true,
//     useCreateIndex: true,
//     useUnifiedTopology: true
//   })
//   .catch(err => {
//     console.error("connection error", err);
//   });

module.exports = async function(filename) {
  try {
    //path to the uploaded csv file
    var csvFilePath = path.join(__dirname, "../uploadedfiles/" + filename);
    // var csvFilePath = path.join(__dirname, "../uploadedfiles/AAPL.csv");
    var csvinput = fs.readFileSync(csvFilePath).toString();

    var tickerNames = [];
    var jsonObj = {};

    function csvJSON(csv) {
      var lines = csv.split("\n");

      var result = [];

      var headers = lines[0].split(",");

      for (var i = 1; i < lines.length; i++) {
        var obj = {};
        var currentline = lines[i].split(",");

        for (var j = 0; j < headers.length; j++) {
          obj[headers[j].replace("\r", "")] = currentline[j].replace("\r", "");
        }

        result.push(obj);
      }
      console.log("headers", headers);
      return result; //JSON

      //return result; //JavaScript object
    }
    jsonObj = csvJSON(csvinput);
    // console.log(jsonObj);

    //converts csv to json object
    // console.log(jsonObj.length);
    // console.log(jsonObj);
    //takes unique ticker names from the json object

    for (var i = 0; i < jsonObj.length; i++) {
      if (tickerNames.indexOf(jsonObj[i]["ticker"]) !== -1) {
        continue;
      } else {
        tickerNames.push(jsonObj[i]["ticker"]);
      }
    }

    for (let ticker of tickerNames) {
      //gets data for the ticker if present
      let ticker_data = await Stocks.findOne({
        ticker_name: jsonObj[0]["ticker"]
      });
      console.log("Data fetched");
      var newly_created = false;

      //   console.log("database ", ticker_data.ticker_dates);
      // console.log(tickerNames);
      let tickerObject = new Stocks();
      tickerObject.ticker_dates = [];
      // creating a new object of the model Stocks
      if (!ticker_data) {
        newly_created = true;
        console.log("Object created");
        //assigning id , name and isIndex property to the tickerObject
        tickerObject.ticker_id = 1;
        tickerObject.ticker_name = ticker;
        tickerObject.isIndex = false;
        tickerObject.simfin_Id = jsonObj[0]["simfinid"];
        //stores first three digits of the companyid to match with id's of sector nan them sector respectively
        var companyid = jsonObj[0]["companyid"].substring(0, 3);

        // assigns industry name to the tickerObject based on the comapnyid
        tickerObject.sector = sectors[`${companyid}`];

        // assigns industry name to the tickerObject based on the comapnyid
        tickerObject.industry = industries[`${jsonObj[0]["companyid"]}`];
      }
      console.log("newly created ", newly_created);
      //stores all unique dates corresponding to the ticker in csv
      let indicator_date = [];
      for (var i = 0; i < jsonObj.length; i++) {
        if (
          indicator_date.indexOf(
            new Date(jsonObj[i]["dates"]).toISOString().substring(0, 10)
          ) !== -1 &&
          jsonObj[i]["ticker"] === ticker
        ) {
          continue;
        } else {
          indicator_date.push(
            new Date(jsonObj[i]["dates"]).toISOString().substring(0, 10)
          );
        }
      }
      console.log("indicator dates fetched from csv");
      let csv_data = [];
      for (var i = 0; i < jsonObj.length; i++) {
        if (jsonObj[i]["ticker"] === ticker) {
          csv_data.push(jsonObj[i]);
        }
      }

      // console.log("indicator dates", indicator_date);
      console.log("csv data according to ticker");
      let ticker_dates = [];
      if (!newly_created) {
        console.log("Newly created ", newly_created);

        //gets existing ticker dates from database

        for (var i = 0; i < ticker_data.ticker_dates.length; i++) {
          ticker_dates.push(ticker_data.ticker_dates[i].date);
        }
        // console.log("ticker dates ", ticker_dates);
      }
      var hasDate = false;
      var iname, ivalue;

      //stores all indicator name and value pairs w.r.t date
      for (var k = 0; k < indicator_date.length; k++) {
        var current_date = new Date(indicator_date[k]);
        console.log("current indicator date in csv", current_date);
        let obj = {};
        console.log("Newly created ", newly_created);
        if (!newly_created) {
          for (var j = 0; j < ticker_dates.length; j++) {
            if (ticker_dates[j].valueOf() == current_date.valueOf()) {
              hasDate = true;
              break;
            } else hasDate = false;
          }
        }
        console.log("checked if the date is present ", hasDate);
        for (var i = 0; i < csv_data.length; i++) {
          let csv_date = new Date(csv_data[i]["dates"])
            .toISOString()
            .substring(0, 10);
          if (new Date(csv_date).valueOf() == current_date.valueOf()) {
            obj[
              `${csv_data[i]["indicatorname"].replace(/\./g, "")}`
            ] = parseFloat(csv_data[i]["indicatorvalue"]);
            iname = csv_data[i]["indicatorname"].replace(/\./g, "");
            ivalue = parseFloat(csv_data[i]["indicatorvalue"]);
            console.log("Object: ", obj);
            console.log("Name", iname);
            console.log("Value", ivalue);
          }
        }

        if (!newly_created) {
          let key = "ticker_dates.$." + iname;
          //   db.stocks_data_2.updateOne({ ticker_id: 1, "ticker_dates.date": '2009-01-22' }, { $set: { "ticker_dates.$.Share Price": '2000' } })
          console.log("hasdate ", hasDate);
          if (hasDate) {
            console.log("Update");
            const result1 = await Stocks.updateOne(
              {
                ticker_name: ticker,
                "ticker_dates.date": current_date
              },
              {
                $set: {
                  key: ivalue
                }
              }
            );
            console.log(result1);
          } else {
            console.log("ADD");

            obj.date = current_date;
            const result = await Stocks.updateOne(
              {
                ticker_name: ticker
              },
              {
                $push: {
                  ticker_dates: obj
                }
              }
            );
            console.log(result);
          }
        }
        obj.date = current_date;
        tickerObject.ticker_dates.push(obj);
      }

      if (newly_created) {
        console.log("Newly crated ", newly_created);

        const result = await tickerObject.save();
        console.log(result);
        console.log("created");
      }
    }
  } catch (err) {
    console.log(" csvtomongo catch", err);
  }
};
// convert();
