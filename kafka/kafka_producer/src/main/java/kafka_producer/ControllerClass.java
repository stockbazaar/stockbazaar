package kafka_producer;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*")
public class ControllerClass {
    @GetMapping("/aa")
    public String greetingWithJavaconfig() {
        return "reached me";
    }
}
