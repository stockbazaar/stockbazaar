package kafka_producer;

import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import kafka_producer.service.indicesService;

@CrossOrigin(origins = "*", maxAge = 3600)
@Component
public class schedulerController {

    @Autowired
    private indicesService is;

    int i = 0;
    @Value("${stocksKafkaTopic}")
    String ktopic;

    @Value("${stocksKafkaUsername}")
    String username;

    @Value("${stocksKafkaPassword}")
    String password;

    @Value("${stocksKafkaServer}")
    String server;

    @Scheduled(fixedRate = 5000)

    public void scheduleTaskWithFixedRate() throws JsonProcessingException {

        if (i == 352) {
            i = 0;
        }

        List<HashMap<String, Object>> value = is.getOhlc(true, i);
        // System.out.println(value);
        i = i + 1;

        ObjectMapper oMapper = new ObjectMapper();
        String map = oMapper.writeValueAsString(value);
        System.out.println(map);
        String topicName = "w108oz8p-new";
        String jaasTemplate = "org.apache.kafka.common.security.scram.ScramLoginModule required username=\"" + username
                + "\" password=\"" + password + "\";";
        String jaasCfg = String.format(jaasTemplate, username, password);
        System.out.println("jaasCfg url " + jaasCfg);

        Properties props = new Properties();
        props.put("max.in.flight.requests.per.connection", 20);
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("bootstrap.servers", server);
        props.put("session.timeout.ms", "30000");
        props.put("security.protocol", "SASL_SSL");
        props.put("sasl.mechanism", "SCRAM-SHA-256");
        props.put("sasl.jaas.config", jaasCfg);

        Producer<String, Object> producer = new KafkaProducer<>(props);
        // for (int i = 1; i < 4; i++) {
        ProducerRecord<String, Object> record = new ProducerRecord<String, Object>(topicName, null, map);
        producer.send(record, new MyProducerCallback());
        // }
        System.out.println(ktopic);
        producer.close();
        return;
    }

}

class MyProducerCallback implements Callback {
    @Override
    public void onCompletion(RecordMetadata recordMetadata, Exception e) {
        if (e != null)
            e.printStackTrace();
        else
            System.out.println("AsynchronousProducer call Success. Offset " + recordMetadata.offset() + "topic "
                    + recordMetadata.topic() + " partition " + recordMetadata.partition());
    }
}