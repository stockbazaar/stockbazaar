package kafka_consumer;

import java.util.Arrays;
import java.util.Properties;
// import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

@Component
public class consumer1 {
    @Autowired
    private SimpMessagingTemplate smt;

    @Value("${stocksKafkaTopic}")
    String ktopic;

    @Value("${stocksKafkaUsername}")
    String username;

    @Value("${stocksKafkaPassword}")
    String password;

    @Value("${stocksKafkaServer}")
    String server;

    public void runSingleWorker() {

        String topicName = ktopic;
        String jaasTemplate = "org.apache.kafka.common.security.scram.ScramLoginModule required username=\"" + username
                + "\" password=\"" + password + "\";";
        String jaasCfg = String.format(jaasTemplate, username, password);

        Properties props = new Properties();
        props.put("max.in.flight.requests.per.connection", 20);
        props.put("group.id", "newer");
        props.put("bootstrap.servers", server);
        props.put("session.timeout.ms", "30000");
        props.put("security.protocol", "SASL_SSL");
        props.put("sasl.mechanism", "SCRAM-SHA-256");
        props.put("sasl.jaas.config", jaasCfg);
        props.put("enable.auto.commit", "true");
        props.put("auto.commit.interval.ms", "1000");
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");

        KafkaConsumer<String, Object> consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Arrays.asList(topicName));

        while (true) {
            @SuppressWarnings("deprecation")
            ConsumerRecords<String, Object> records = consumer.poll(100);
            for (ConsumerRecord<String, Object> record : records) {
                System.out.println("Topic offset: " + record.topic() + " " + record.offset());
                JSONParser parser = new JSONParser();
                try {
                    JSONArray array = (JSONArray) parser.parse((String) record.value());
                    ;
                    if (array != null) {
                        smt.convertAndSend("/topic/public", array);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();

                }
            }

        }

    }
}