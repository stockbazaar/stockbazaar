# StockBazaar

StockBazaar is a web based application for viewing stocks, their current prices, and every financial report for the same and It lets you compare stocks based on various properties

## Technology stack

Front-end: Reactjs
Middle-tier: Nodejs, Java, Flask
Back-end: Mongo

Back-end: PostgreSQL

## Installation

### Reactjs and Nodejs packages

Open **client folder for react packages** installation on terminal and **server folder for nodejs packages** the run the follwing bash command on the respective terminal to install all the required package for react and nodejs.

```bash
npm install
```

### Python modules and Plotly setup

- flask
- flask_cors
- flask_pymongo
- os
- requests
- plotly.graph_objs
- chart_studio
- pandas
- pandas_datareader.data

1. Create an account on chart studio or login if you have already registered [Chart Studio - Login](https://chart-studio.plot.ly/Auth/login/?action=signin#/)
2. Now generate an api key from chart studio and copy it.
3. Add your credentials in python files as your username and api key.

## Data Loading

### Loading csv files to mongo.

1.  Create a folder db-init inside server. The folder structure should look like following.

        .
        |___ stockbazaar
            |___ client
            |___ server
                |___db-init

2.  Add

## Java Setup for Monte Carlo

1. Run the StockbazaarjavaApplication.java file inside stockbazaar folder under stockbazaarjava

## Good to go

1. Run the following command on terminal inside server folder to start the server.
   ```bash
   npm start
   ```
2. Now execute the **deploy.py** file to start the flask api server for ohlc, stock chart, earnings vs revenue, profit loss, assets, liabilities and cash flow visualization.
3. Next run the following command on terminal inside client folder to start the client.
   ```bash
   npm start
   ```

## License

[Headstrait Softwares](https://headstrait.com)
