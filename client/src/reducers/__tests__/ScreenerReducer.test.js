import screenerReducer from "../Screener";
import {
  GET_ALL_INDUSTRIES_BY_SECTOR,
  GET_TICKERS_BY_FILTER
} from "../../actions/Types";

// Test for GET_ALL_INDUSTRIES_BY_SECTOR
describe("Testing Screener Reducer for GET_ALL_INDUSTRIES_BY_SECTOR", () => {
  it("test with initial state and expect to return a state object with industriesBySector as array", () => {
    const action = {
      type: GET_ALL_INDUSTRIES_BY_SECTOR,
      payload: [{}, {}, {}]
    };
    const returnedState = screenerReducer(undefined, action);
    expect(returnedState).toEqual({
      industriesBySector: action.payload,
      filteredTickers: []
    });
  });
  it("test without initial state and expect to return a state object with industriesBySector as array", () => {
    const initialState = {
      industriesBySector: [{}, {}, {}],
      filteredTickers: []
    };
    const action = {
      type: GET_ALL_INDUSTRIES_BY_SECTOR,
      payload: [{}, {}, {}]
    };
    const returnedState = screenerReducer(initialState, action);
    expect(returnedState).toEqual({
      industriesBySector: action.payload,
      filteredTickers: []
    });
  });
});

// Test for GET_TICKERS_BY_FILTER
describe("Testing Screener Reducer for GET_TICKERS_BY_FILTER", () => {
  it("test with initial state and expect to return a state object with industriesBySector as array", () => {
    const action = {
      type: GET_TICKERS_BY_FILTER,
      payload: [{}, {}, {}]
    };
    const returnedState = screenerReducer(undefined, action);
    expect(returnedState).toEqual({
      industriesBySector: [],
      filteredTickers: action.payload
    });
  });
  it("test without initial state and expect to return a state object with industriesBySector as array", () => {
    const initialState = {
      industriesBySector: [],
      filteredTickers: [{}, {}, {}]
    };
    const action = {
      type: GET_TICKERS_BY_FILTER,
      payload: [{}, {}, {}]
    };
    const returnedState = screenerReducer(initialState, action);
    expect(returnedState).toEqual({
      industriesBySector: [],
      filteredTickers: action.payload
    });
  });
});

// Test for SOME_TYPE
describe("Testing Screener Reducer for SOME_TYPE", () => {
  it("test with initial state and expect to return a state object with industriesBySector as array", () => {
    const action = {
      type: "SOME_TYPE"
    };
    const returnedState = screenerReducer(undefined, action);
    expect(returnedState).toEqual({
      industriesBySector: [],
      filteredTickers: []
    });
  });
  it("test without initial state and expect to return a state object with industriesBySector as array", () => {
    const initialState = {
      industriesBySector: [{}, {}, {}],
      filteredTickers: [{}, {}, {}]
    };
    const action = {
      type: "SOME_TYPE",
      payload: [{}, {}, {}]
    };
    const returnedState = screenerReducer(initialState, action);
    expect(returnedState).toEqual(initialState);
  });
});
