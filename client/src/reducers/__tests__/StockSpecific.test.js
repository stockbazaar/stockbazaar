import stockSpecificReducer from "../stockSpecificReducer";
import {
  GET_STOCKCHART,
  GET_STOCK_DETAILS,
  GET_FINANCIAL_REPORTS,
  GET_MONTECARLO_GRAPH,
  GET_EARNINGSVSREVENUE,
  GET_PROFITLOSS,
  GET_ASSETS,
  GET_LIABILITIES,
  GET_CASHFLOW
} from "../../actions/Types";

describe("Testing stockSpecific Reducer for GET_STOCKCHART", () => {
  it("test with initial state and expect to return a state object with stockGraph as array", () => {
    const action = {
      type: GET_STOCKCHART,
      payload: []
    };
    const returnedState = stockSpecificReducer(undefined, action);
    expect(returnedState).toEqual({
      stockGraph: action.payload,
      allreports: [],
      assets: [],
      cashflow: [],
      earningsvsrevenue: [],
      financialdetails: {},
      financialdetails2: {},
      liabilities: [],
      montecarlograph: "",
      profitloss: [],
      stockGraph: [],
      stockdetails: [],
      stockdetails2: []
    });
  });
  it("test without initial state and expect to return a state object with industriesBySector as array", () => {
    const initialState = {
      stockGraph: []
    };
    const action = {
      type: GET_STOCKCHART,
      payload: []
    };
    const returnedState = stockSpecificReducer(initialState, action);
    expect(returnedState).toEqual({
      stockGraph: action.payload
    });
  });
});

describe("Testing stockSpecific Reducer for GET_STOCKDETAILS", () => {
  it("test with initial state and expect to return a state object with stockdetails as array", () => {
    const action = {
      type: GET_STOCK_DETAILS,
      payload: []
    };
    const returnedState = stockSpecificReducer(undefined, action);
    expect(returnedState).toEqual({
      stockdetails: action.payload,
      allreports: [],
      assets: [],
      cashflow: [],
      earningsvsrevenue: [],
      financialdetails: {},
      financialdetails2: {},
      liabilities: [],
      montecarlograph: "",
      profitloss: [],
      stockGraph: [],
      stockGraph: [],
      stockdetails2: []
    });
  });
  it("test without initial state and expect to return a state object with stockdetails as array", () => {
    const initialState = {
      stockdetails: []
    };
    const action = {
      type: GET_STOCK_DETAILS,
      payload: []
    };
    const returnedState = stockSpecificReducer(initialState, action);
    expect(returnedState).toEqual({
      stockdetails: action.payload
    });
  });
});

describe("Testing stockSpecific Reducer for GET_FINANCIAL_REPORTS", () => {
  it("test with initial state and expect to return a state object with financialdetails as array", () => {
    const action = {
      type: GET_FINANCIAL_REPORTS,
      payload: {}
    };
    const returnedState = stockSpecificReducer(undefined, action);
    expect(returnedState).toEqual({
      stockdetails: [],
      allreports: [],
      assets: [],
      cashflow: [],
      earningsvsrevenue: [],
      financialdetails: action.payload,
      financialdetails2: {},
      liabilities: [],
      montecarlograph: "",
      profitloss: [],
      stockGraph: [],
      stockGraph: [],
      stockdetails2: []
    });
  });
  it("test without initial state and expect to return a state object with stockdetails as array", () => {
    const initialState = {
      financialdetails: {}
    };
    const action = {
      type: GET_FINANCIAL_REPORTS,
      payload: []
    };
    const returnedState = stockSpecificReducer(initialState, action);
    expect(returnedState).toEqual({
      financialdetails: action.payload
    });
  });
});

describe("Testing stockSpecific Reducer for GET_MONTECARLO_GRAPH", () => {
  it("test with initial state and expect to return a state object with montecarlograph as array", () => {
    const action = {
      type: GET_MONTECARLO_GRAPH,
      payload: ""
    };
    const returnedState = stockSpecificReducer(undefined, action);
    expect(returnedState).toEqual({
      stockdetails: [],
      allreports: [],
      assets: [],
      cashflow: [],
      earningsvsrevenue: [],
      financialdetails: {},
      financialdetails2: {},
      liabilities: [],
      montecarlograph: action.payload,
      profitloss: [],
      stockGraph: [],
      stockGraph: [],
      stockdetails2: []
    });
  });
  it("test without initial state and expect to return a state object with montecarlograph as array", () => {
    const initialState = {
      montecarlograph: ""
    };
    const action = {
      type: GET_MONTECARLO_GRAPH,
      payload: ""
    };
    const returnedState = stockSpecificReducer(initialState, action);
    expect(returnedState).toEqual({
      montecarlograph: action.payload
    });
  });
});

describe("Testing stockSpecific Reducer for GET_EARNINGSVSREVENUE", () => {
  it("test with initial state and expect to return a state object with earningsvsrevenue as array", () => {
    const action = {
      type: GET_EARNINGSVSREVENUE,
      payload: []
    };
    const returnedState = stockSpecificReducer(undefined, action);
    expect(returnedState).toEqual({
      stockdetails: [],
      allreports: [],
      assets: [],
      cashflow: [],
      earningsvsrevenue: action.payload,
      financialdetails: {},
      financialdetails2: {},
      liabilities: [],
      montecarlograph: "",
      profitloss: [],
      stockGraph: [],
      stockGraph: [],
      stockdetails2: []
    });
  });
  it("test without initial state and expect to return a state object with earningsvsrevenue as array", () => {
    const initialState = {
      earningsvsrevenue: []
    };
    const action = {
      type: GET_EARNINGSVSREVENUE,
      payload: ""
    };
    const returnedState = stockSpecificReducer(initialState, action);
    expect(returnedState).toEqual({
      earningsvsrevenue: action.payload
    });
  });
});

describe("Testing stockSpecific Reducer for GET_PROFITLOSS", () => {
  it("test with initial state and expect to return a state object with profitloss as array", () => {
    const action = {
      type: GET_PROFITLOSS,
      payload: []
    };
    const returnedState = stockSpecificReducer(undefined, action);
    expect(returnedState).toEqual({
      stockdetails: [],
      allreports: [],
      assets: [],
      cashflow: [],
      earningsvsrevenue: [],
      financialdetails: {},
      financialdetails2: {},
      liabilities: [],
      montecarlograph: "",
      profitloss: action.payload,
      stockGraph: [],
      stockGraph: [],
      stockdetails2: []
    });
  });
  it("test without initial state and expect to return a state object with profitloss as array", () => {
    const initialState = {
      profitloss: []
    };
    const action = {
      type: GET_PROFITLOSS,
      payload: []
    };
    const returnedState = stockSpecificReducer(initialState, action);
    expect(returnedState).toEqual({
      profitloss: action.payload
    });
  });
});

describe("Testing stockSpecific Reducer for GET_ASSETS", () => {
  it("test with initial state and expect to return a state object with assets as array", () => {
    const action = {
      type: GET_ASSETS,
      payload: []
    };
    const returnedState = stockSpecificReducer(undefined, action);
    expect(returnedState).toEqual({
      stockdetails: [],
      allreports: [],
      assets: action.payload,
      cashflow: [],
      earningsvsrevenue: [],
      financialdetails: {},
      financialdetails2: {},
      liabilities: [],
      montecarlograph: "",
      profitloss: [],
      stockGraph: [],
      stockGraph: [],
      stockdetails2: []
    });
  });
  it("test without initial state and expect to return a state object with assets as array", () => {
    const initialState = {
      assets: []
    };
    const action = {
      type: GET_ASSETS,
      payload: []
    };
    const returnedState = stockSpecificReducer(initialState, action);
    expect(returnedState).toEqual({
      assets: action.payload
    });
  });
});

describe("Testing stockSpecific Reducer for GET_LIABILITIES", () => {
  it("test with initial state and expect to return a state object with liabilities as array", () => {
    const action = {
      type: GET_LIABILITIES,
      payload: []
    };
    const returnedState = stockSpecificReducer(undefined, action);
    expect(returnedState).toEqual({
      stockdetails: [],
      allreports: [],
      assets: [],
      cashflow: [],
      earningsvsrevenue: [],
      financialdetails: {},
      financialdetails2: {},
      liabilities: action.payload,
      montecarlograph: "",
      profitloss: [],
      stockGraph: [],
      stockGraph: [],
      stockdetails2: []
    });
  });
  it("test without initial state and expect to return a state object with liabilities as array", () => {
    const initialState = {
      liabilities: []
    };
    const action = {
      type: GET_LIABILITIES,
      payload: []
    };
    const returnedState = stockSpecificReducer(initialState, action);
    expect(returnedState).toEqual({
      liabilities: action.payload
    });
  });
});

describe("Testing stockSpecific Reducer for GET_CASHFLOW", () => {
  it("test with initial state and expect to return a state object with cashflow as array", () => {
    const action = {
      type: GET_CASHFLOW,
      payload: []
    };
    const returnedState = stockSpecificReducer(undefined, action);
    expect(returnedState).toEqual({
      stockdetails: [],
      allreports: [],
      assets: [],
      cashflow: action.payload,
      earningsvsrevenue: [],
      financialdetails: {},
      financialdetails2: {},
      liabilities: [],
      montecarlograph: "",
      profitloss: [],
      stockGraph: [],
      stockGraph: [],
      stockdetails2: []
    });
  });
  it("test without initial state and expect to return a state object with cashflow as array", () => {
    const initialState = {
      cashflow: []
    };
    const action = {
      type: GET_CASHFLOW,
      payload: []
    };
    const returnedState = stockSpecificReducer(initialState, action);
    expect(returnedState).toEqual({
      cashflow: action.payload
    });
  });
});
