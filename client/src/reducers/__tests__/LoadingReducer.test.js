import Loading from "../LoadingReducer";
import { LOADING_START, LOADING_STOP } from "../../actions/Types";

describe("Testing Users Reducers", () => {
  //
  it("should return a state object with array equal to the payload in the action when the action type is LOADING_START (when the returned state is initial state)", () => {
    const action = {
      type: LOADING_START,
      payload: [{}, {}, {}]
    };
    const returnedState = Loading(undefined, action);
    expect(returnedState).toEqual({ isLoading: true });
  });

  it("should return a state object with array equal to the payload in the action when the action type is LOADING_STOP (when the returned state is initial state)", () => {
    const initialState = {
      //   users: [1, 2, 3, 4, 5]
    };
    const action = {
      type: LOADING_STOP,
      payload: [{}, {}, {}]
    };
    const returnedState = Loading(initialState, action);
    expect(returnedState).toEqual({ isLoading: false });
  });

  it("should return a state object with array equal to the payload in the action when the action type is DEFAULT (when the returned state is initial state)", () => {
    const initialState = {
      //   users: [1, 2, 3, 4, 5]
    };
    const action = {
      payload: [{}, {}, {}]
    };
    const returnedState = Loading(initialState, action);
    expect(returnedState).toEqual({});
  });
});
