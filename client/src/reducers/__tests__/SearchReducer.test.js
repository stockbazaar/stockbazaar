import search from "../SearchReducer";
import { ALL_SEARCH } from "../../actions/Types";

describe("Testing Global Search Reducers", () => {
  it("should return a state object with search array equal to the payload in the action when the action type is ALL_SEARCH (when the returned state is initial state)", () => {
    const action = {
      type: ALL_SEARCH,
      payload: [{}, {}, {}]
    };
    const returnedState = search(undefined, action);
    expect(returnedState).toEqual({ error: "", search: action.payload });
  });

  it("should return a state object with search array equal to the payload in the action when the action type is ALL_SEARCH (when the returned state is not an initial state)", () => {
    const initialState = {
      search: [1, 2, 3, 4, 5]
    };
    const action = {
      type: ALL_SEARCH,
      payload: [{}, {}, {}]
    };
    const returnedState = search(initialState, action);
    expect(returnedState).toEqual({ search: action.payload });
  });

  it("should return a state object with search array equal to the payload in the action when the action type is default (when the returned state is not an initial state)", () => {
    const initialState = {
      search: [{}, {}, {}]
    };
    const action = {
      payload: [{}, {}, {}]
    };
    const returnedState = search(initialState, action);
    expect(returnedState).toEqual({ search: action.payload });
  });
});
