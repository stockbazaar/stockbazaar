import {
  GET_INDICES,
  GET_OVERVIEW,
  GET_PERFORMANCE,
  GET_GAINERS_MARGIN,
  GET_LOSERS_VALUATION,
  GET_LOSERS_MARGIN,
  GET_SECTORS,
  GET_INDUSTRIES,
  GET_SECTORS_PERFORMANCE,
  GET_INDUSTRY_PERFORMANCE,
  GET_GAINERS_VALUATION
} from "../../actions/Types";

describe("Testing Stocks Landing Page Reducers", () => {
  it("should return a state object with indices array equal to the payload in the action when the action type is GET_INDICES (when the returned state is initial state)", () => {
    const action = {
      type: GET_INDICES,
      payload: [{}, {}, {}]
    };
    const returnedState = indices(undefined, action);
    expect(returnedState).toEqual({ error: "", indices: action.payload });
  });

  it("should return a state object with indices array equal to the payload in the action when the action type is GET_INDICES (when the returned state is not an initial state)", () => {
    const initialState = {
      indices: [{}, {}, {}]
    };
    const action = {
      type: GET_INDICES,
      payload: [{}, {}, {}]
    };
    const returnedState = indices(initialState, action);
    expect(returnedState).toEqual({ indices: action.payload });
  });
  it("should return a state object with goverview array equal to the payload in the action when the action type is GET_ALL_INDICES (when the returned state is initial state)", () => {
    const action = {
      type: GET_OVERVIEW,
      payload: [{}, {}, {}]
    };
    const returnedState = goverview(undefined, action);
    expect(returnedState).toEqual({
      error: "",
      goverview: action.payload.goverview
    });
  });

  it("should return a state object with loverview array equal to the payload in the action when the action type is GET_OVERVIEW (when the returned state is not an initial state)", () => {
    const initialState = {
      loverview: [{}, {}, {}]
    };
    const action = {
      type: GET_OVERVIEW,
      payload: [{}, {}, {}]
    };
    const returnedState = loverview(initialState, action);
    expect(returnedState).toEqual({ loverview: action.payload.loverview });
  });
  it("should return a state object with gperformance array equal to the payload in the action when the action type is GET_ALL_INDICES (when the returned state is initial state)", () => {
    const action = {
      type: GET_PERFORMANCE,
      payload: [{}, {}, {}]
    };
    const returnedState = gperformance(undefined, action);
    expect(returnedState).toEqual({
      error: "",
      gperformance: action.payload.gperformance
    });
  });

  it("should return a state object with lperformance array equal to the payload in the action when the action type is GET_OVERVIEW (when the returned state is not an initial state)", () => {
    const initialState = {
      lperformance: [{}, {}, {}]
    };
    const action = {
      type: GET_PERFORMANCE,
      payload: [{}, {}, {}]
    };
    const returnedState = lperformance(initialState, action);
    expect(returnedState).toEqual({
      lperformance: action.payload.lperformance
    });
  });
  it("should return a state object with gvalue array equal to the payload in the action when the action type is GET_GAINERS_VALUATION (when the returned state is not an initial state)", () => {
    const initialState = {
      gvalue: [{}, {}, {}]
    };
    const action = {
      type: GET_GAINERS_VALUATION,
      payload: [{}, {}, {}]
    };
    const returnedState = gvalue(initialState, action);
    expect(returnedState).toEqual({
      gvalue: action.payload
    });
  });

  it("should return a state object with lvalue array equal to the payload in the action when the action type is GET_LOSERS_VALUATION (when the returned state is not an initial state)", () => {
    const initialState = {
      lvalue: [{}, {}, {}]
    };
    const action = {
      type: GET_LOSERS_VALUATION,
      payload: [{}, {}, {}]
    };
    const returnedState = lvalue(initialState, action);
    expect(returnedState).toEqual({
      lvalue: action.payload
    });
  });
  it("should return a state object with gmargin array equal to the payload in the action when the action type is GET_GAINERS_MARGIN (when the returned state is not an initial state)", () => {
    const initialState = {
      gmargin: [{}, {}, {}]
    };
    const action = {
      type: GET_GAINERS_MARGIN,
      payload: [{}, {}, {}]
    };
    const returnedState = gmargin(initialState, action);
    expect(returnedState).toEqual({
      gmargin: action.payload
    });
  });
  it("should return a state object with lmargin array equal to the payload in the action when the action type is GET_LOSERS_MARGIN (when the returned state is not an initial state)", () => {
    const initialState = {
      lmargin: [{}, {}, {}]
    };
    const action = {
      type: GET_LOSERS_MARGIN,
      payload: [{}, {}, {}]
    };
    const returnedState = lmargin(initialState, action);
    expect(returnedState).toEqual({
      lmargin: action.payload
    });
  });
  it("should return a state object with sectors array equal to the payload in the action when the action type is GET_SECTORS (when the returned state is not an initial state)", () => {
    const initialState = {
      sectors: [{}, {}, {}]
    };
    const action = {
      type: GET_SECTORS,
      payload: [{}, {}, {}]
    };
    const returnedState = sectors(initialState, action);
    expect(returnedState).toEqual({
      sectors: action.payload
    });
  });
  it("should return a state object with industries array equal to the payload in the action when the action type is GET_INDUSTRIES (when the returned state is not an initial state)", () => {
    const initialState = {
      industries: [{}, {}, {}]
    };
    const action = {
      type: GET_INDUSTRIES,
      payload: [{}, {}, {}]
    };
    const returnedState = industries(initialState, action);
    expect(returnedState).toEqual({
      industries: action.payload
    });
  });

  it("should return a state object with iperformance array equal to the payload in the action when the action type is GET_INDUSTRY_PERFORMANCE (when the returned state is not an initial state)", () => {
    const initialState = {
      iperformance: [{}, {}, {}]
    };
    const action = {
      type: GET_INDUSTRY_PERFORMANCE,
      payload: [{}, {}, {}]
    };
    const returnedState = iperformance(initialState, action);
    expect(returnedState).toEqual({
      iperformance: action.payload
    });
  });
  it("should return a state object with sperformance array equal to the payload in the action when the action type is GET_SECTORS_PERFORMANCE (when the returned state is not an initial state)", () => {
    const initialState = {
      sperformance: [{}, {}, {}]
    };
    const action = {
      type: GET_SECTORS_PERFORMANCE,
      payload: [{}, {}, {}]
    };
    const returnedState = sperformance(initialState, action);
    expect(returnedState).toEqual({
      sperformance: action.payload
    });
  });
});
