import user from "../userReducer";
import { SUCCESS, ERROR, CLEAR } from "../../actions/Types";

describe("Testing Users Reducers", () => {
  it("should return a state object with user array equal to the payload in the action when the action type is SUCCESS (when the returned state is initial state)", () => {
    const action = {
      type: SUCCESS,
      payload: "success"
    };
    const returnedState = user(undefined, action);
    expect(returnedState).toEqual({ error: "", success: action.payload });
  });

  it("should return a state object with user array equal to the payload in the action when the action type is GET_USER (when the returned state is not an initial state)", () => {
    const initialState = {
      error: "error msg",
      success: "success msg"
    };
    const action = {
      type: SUCCESS,
      payload: "new success msg"
    };
    const returnedState = user(initialState, action);
    expect(returnedState).toEqual({
      error: initialState.error,
      success: action.payload
    });
  });

  it("should return the initial state object when the action type is not mentioned or doesn't concern the reducer (when the returned state is initial state)", () => {
    let action = {
      payload: " "
    };
    let returnedState = user(undefined, action);
    expect(returnedState).toEqual({ error: "", success: "" });
    action = {
      type: "SOME_TYPE",
      payload: ""
    };
    returnedState = user(undefined, action);
    expect(returnedState).toEqual({ error: "", success: "" });
  });

  it("should return a state object with user array equal to the payload in the action when the action type is ERROR (when the returned state is initial state)", () => {
    const action = {
      type: ERROR
    };
    const returnedState = user(undefined, action);
    expect(returnedState).toEqual({ error: undefined, success: "" });
  });
  it("should return a state object with user array equal to the payload in the action when the action type is   ERROR (when the returned state is not an initial state)", () => {
    const initialState = {
      error: "error msg"
    };
    const action = {
      type: ERROR
    };
    const returnedState = user(initialState, action);
    expect(returnedState).toEqual({ error: initialState.error });
  });
  it("should return a state object with user array equal to the payload in the action when the action type is CLEAR (when the returned state is not an initial state)", () => {
    const initialState = {
      error: "error msg",
      success: "success msg"
    };
    const action = {
      type: CLEAR
    };
    const returnedState = user(initialState, action);
    expect(returnedState).toEqual({ error: "", success: "" });
  });
});
