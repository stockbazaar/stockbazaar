// Importing Required Types - Eli
import { ALL_SEARCH, COMPARE_SEARCH } from "../actions/Types";

// Declaring a Reducer - Eli
const initialstate = {
  search: [],
  compare: []
};

// Expecting the Reducer to be Filled according to the Action Type - Eli
export default function(state = initialstate, action) {
  switch (action.type) {
    case ALL_SEARCH:
      return { ...state, search: action.payload };
    case COMPARE_SEARCH:
      return { ...state, compare: action.payload };
    default:
      return state;
  }
}
