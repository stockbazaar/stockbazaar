import {
  GET_INDICES,
  GET_INDEX_OHLC,
  GET_OVERVIEW,
  GET_PERFORMANCE,
  GET_GAINERS_MARGIN,
  GET_LOSERS_MARGIN,
  GET_SECTORS,
  GET_INDUSTRIES,
  GET_SECTORS_PERFORMANCE,
  GET_INDUSTRY_PERFORMANCE,
  GET_GAINERS_VALUATION,
  GET_LOSERS_VALUATION
} from "../actions/Types";

const initialstate = {
  indices: [],
  indexohlc: [],
  goverview: [],
  gperformance: [],
  gvalue: [],
  lvalue: [],
  gmargin: [],
  loverview: [],
  lperformance: [],
  lmargin: [],
  sectors: [],
  industries: [],
  sperformance: [],
  iperformance: []
};

export default function(state = initialstate, action) {
  switch (action.type) {
    case GET_INDICES:
      return { ...state, indices: action.payload };
    case GET_INDEX_OHLC:
      return { ...state, indexohlc: action.payload };
    case GET_OVERVIEW:
      return {
        ...state,
        goverview: action.payload.gainers,
        loverview: action.payload.losers
      };
    case GET_PERFORMANCE:
      return {
        ...state,
        gperformance: action.payload.gainers,
        lperformance: action.payload.losers
      };
    case GET_GAINERS_VALUATION:
      return { ...state, gvalue: action.payload };
    case GET_LOSERS_VALUATION:
      return { ...state, lvalue: action.payload };
    case GET_SECTORS:
      return { ...state, sectors: action.payload };
    case GET_INDUSTRIES:
      return { ...state, industries: action.payload };
    case GET_GAINERS_MARGIN:
      return { ...state, gmargin: action.payload };
    case GET_LOSERS_MARGIN:
      return { ...state, lmargin: action.payload };
    case GET_SECTORS_PERFORMANCE:
      return { ...state, sperformance: action.payload };
    case GET_INDUSTRY_PERFORMANCE:
      console.log("Reached the reducer");
      return { ...state, iperformance: action.payload };
    default:
      return state;
  }
}
// }
