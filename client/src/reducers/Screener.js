import {
  GET_ALL_INDUSTRIES_BY_SECTOR,
  GET_TICKERS_BY_FILTER
} from "../actions/Types";

const initialState = {
  industriesBySector: [],
  filteredTickers: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_ALL_INDUSTRIES_BY_SECTOR:
      return {
        ...state,
        industriesBySector: action.payload
      };
    case GET_TICKERS_BY_FILTER:
      return {
        ...state,
        filteredTickers: action.payload
      };
    default:
      return state;
  }
}
