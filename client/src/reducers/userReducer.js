import { SUCCESS, ERROR, CLEAR } from "../actions/Types";

//stores error and success messages in reponse of api/server respectively
const initialstate = {
  error: "",
  success: ""
};

export default function(state = initialstate, action) {
  switch (action.type) {
    case SUCCESS:
      return { ...state, success: action.payload };
    case ERROR:
      return { ...state, error: action.payload };
    case CLEAR:
      return initialstate;
    default:
      return state;
  }
}
