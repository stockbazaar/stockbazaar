import {
  GET_STOCKCHART,
  GET_STOCK_DETAILS,
  GET_STOCK_DETAILS_2,
  GET_FINANCIAL_REPORTS,
  GET_FINANCIAL_REPORTS_2,
  GET_MONTECARLO_GRAPH,
  GET_ALL_REPORTS,
  GET_EARNINGSVSREVENUE,
  GET_PROFITLOSS,
  GET_ASSETS,
  GET_LIABILITIES,
  GET_CASHFLOW
} from "../actions/Types";

const initialstate = {
  stockGraph: [],
  stockdetails: [],
  stockdetails2: [],
  financialdetails: {},
  financialdetails2: {},
  earningsvsrevenue: [],
  profitloss: [],
  assets: [],
  liabilities: [],
  cashflow: [],
  //stores graph url as a string
  montecarlograph: "",
  allreports: []
};

export default function(state = initialstate, action) {
  switch (action.type) {
    case GET_STOCKCHART:
      return { ...state, stockGraph: action.payload };
    case GET_STOCK_DETAILS:
      return { ...state, stockdetails: action.payload };
    case GET_STOCK_DETAILS_2:
      return { ...state, stockdetails2: action.payload };
    case GET_FINANCIAL_REPORTS:
      return { ...state, financialdetails: action.payload };
    case GET_FINANCIAL_REPORTS_2:
      return { ...state, financialdetails2: action.payload };
    case GET_EARNINGSVSREVENUE:
      return { ...state, earningsvsrevenue: action.payload };
    case GET_PROFITLOSS:
      return { ...state, profitloss: action.payload };
    case GET_ASSETS:
      return { ...state, assets: action.payload };
    case GET_LIABILITIES:
      return { ...state, liabilities: action.payload };
    case GET_CASHFLOW:
      return { ...state, cashflow: action.payload };
    case GET_MONTECARLO_GRAPH:
      return { ...state, montecarlograph: action.payload };
    case GET_ALL_REPORTS:
      return { ...state, allreports: action.payload };
    default:
      return state;
  }
}
