// Importing Required Types - Eli
import { LOADING_START, LOADING_STOP } from "../actions/Types";

// Declaring a Pre-defined State - Eli
const initialState = {
  isLoading: false
};

// Expecting various Output Result according to Various Functions as well as Checking for Default - Eli
export default function(state = initialState, action) {
  switch (action.type) {
    case LOADING_START:
      return {
        ...state,
        isLoading: true
      };
    case LOADING_STOP:
      return {
        ...state,
        isLoading: false
      };
    default:
      return state;
  }
}
