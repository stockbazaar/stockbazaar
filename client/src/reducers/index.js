import { combineReducers } from "redux";
import users from "./userReducer";
import LoadingReducer from "./LoadingReducer";
import stockdetails from "./stockSpecificReducer";
import stockdetails2 from "./stockSpecificReducer";
import financialdetails from "./stockSpecificReducer";
import financialdetails2 from "./stockSpecificReducer";
import stocksLanding from "./stocksLanding";
import search from "./SearchReducer";
import screenerReducer from "./Screener";
import compare from "./SearchReducer";

export default combineReducers({
  userReducer: users,
  LoadingReducer,
  searchReducer: search,
  stocksLandingReducer: stocksLanding,
  stockSpecificReducer: stockdetails,
  stockSpecificReducer: stockdetails2,
  stockSpecificReducer: financialdetails,
  stockSpecificReducer: financialdetails2,
  screenerReducer: screenerReducer,
  compareReducer: compare
});
