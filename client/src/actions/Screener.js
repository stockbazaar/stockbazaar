import { GET_ALL_INDUSTRIES_BY_SECTOR, GET_TICKERS_BY_FILTER } from "./Types";

import axios from "axios";

const url = "https://stockbazaar-node.herokuapp.com/api/screener";

export const getAllIndustriesBySector = () => dispatch => {
  return axios
    .get(url + "/getIndustriesBySector")
    .then(res => {
      dispatch({
        type: GET_ALL_INDUSTRIES_BY_SECTOR,
        payload: res.data.data
      });
    })
    .catch(err => {
      console.log(err);
    });
};
export const getTickersByFilter = filterData => dispatch => {
  return axios
    .post(url + "/getTickersByFilter", filterData)
    .then(res => {
      dispatch({
        type: GET_TICKERS_BY_FILTER,
        payload: res.data.data
      });
    })
    .catch(err => {
      console.log(err);
    });
};
