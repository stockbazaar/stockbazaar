import {
  GET_STOCKCHART,
  GET_STOCK_DETAILS,
  GET_STOCK_DETAILS_2,
  GET_FINANCIAL_REPORTS,
  GET_FINANCIAL_REPORTS_2,
  GET_MONTECARLO_GRAPH,
  GET_ALL_REPORTS,
  GET_EARNINGSVSREVENUE,
  GET_PROFITLOSS,
  GET_ASSETS,
  GET_LIABILITIES,
  GET_CASHFLOW
} from "./Types";

import axios from "axios";
const url = "https://stockbazaar-node.herokuapp.com/api/screener";
const ploturl = "https://stockbazaar-python.herokuapp.com/";

export const getStockChart = ticker_id => dispatch => {
  return axios
    .get(ploturl + "stockchart/" + ticker_id)
    .then(res => {
      // console.log(ticker_id);
      dispatch({
        type: GET_STOCKCHART,
        payload: res.data
      });
      // console.log(res.data);
    })
    .catch(err => {
      console.log(err);
    });
};

export const getStockDetails = ticker_id => dispatch => {
  return axios
    .get(url + "/getstockdetails/" + ticker_id)
    .then(res => {
      // console.log(ticker_id);
      dispatch({
        type: GET_STOCK_DETAILS,
        payload: res.data.data
      });
      // console.log(res.data.data);
    })
    .catch(err => {
      console.log(err);
    });
};

export const getStockDetails2 = ticker_id => dispatch => {
  return axios
    .get(url + "/getstockdetails/" + ticker_id)
    .then(res => {
      // console.log(ticker_id);
      dispatch({
        type: GET_STOCK_DETAILS_2,
        payload: res.data.data
      });
      // console.log(res.data.data);
    })
    .catch(err => {
      console.log(err);
    });
};

export const getFinancialReports = ticker_id => dispatch => {
  return axios
    .get(url + "/getfinancialreports/" + ticker_id)
    .then(res => {
      // console.log(ticker_id);
      dispatch({
        type: GET_FINANCIAL_REPORTS,
        payload: res.data.data
      });
      // console.log(res.data.data);
    })
    .catch(err => {
      console.log(err);
    });
};

export const getFinancialReports2 = ticker_id => dispatch => {
  return axios
    .get(url + "/getfinancialreports/" + ticker_id)
    .then(res => {
      // console.log(ticker_id);
      dispatch({
        type: GET_FINANCIAL_REPORTS_2,
        payload: res.data.data
      });
      // console.log(res.data.data);
    })
    .catch(err => {
      console.log(err);
    });
};

//[kavita]  api call to get monte carlo graph url source for a specified ticker id
export const getMonteCarloGraph = ticker_id => dispatch => {
  return axios
    .get(ploturl + "montecarlo/" + ticker_id)
    .then(res => {
      dispatch({
        type: GET_MONTECARLO_GRAPH,
        payload: res.data
      });
    })
    .catch(err => {
      console.log(err);
    });
};

//[kavita 20-11-2019] api call to get all  financial reports of all  years for  a specific ticker id
//used to provide download data to user
export const getAllReports = ticker_id => dispatch => {
  return axios
    .get(url + "/download/allreports/" + ticker_id)
    .then(res => {
      dispatch({
        type: GET_ALL_REPORTS,
        payload: res.data.data
      });
    })
    .catch(err => {
      console.log(err);
    });
};

export const getEarningsVsRevenue = ticker_id => dispatch => {
  return axios
    .get(ploturl + "earningsvsrevenue/" + ticker_id)
    .then(res => {
      // console.log(ticker_id);
      dispatch({
        type: GET_EARNINGSVSREVENUE,
        payload: res.data
      });
      // console.log(res.data);
    })
    .catch(err => {
      console.log(err);
    });
};

export const getProfitLoss = ticker_id => dispatch => {
  return axios
    .get(ploturl + "profitloss/" + ticker_id)
    .then(res => {
      // console.log(ticker_id);
      dispatch({
        type: GET_PROFITLOSS,
        payload: res.data
      });
      // console.log(res.data);
    })
    .catch(err => {
      console.log(err);
    });
};

export const getAssets = ticker_id => dispatch => {
  return axios
    .get(ploturl + "assets/" + ticker_id)
    .then(res => {
      console.log(ticker_id);
      dispatch({
        type: GET_ASSETS,
        payload: res.data
      });
      console.log(res.data);
    })
    .catch(err => {
      console.log(err);
    });
};

export const getLiabilities = ticker_id => dispatch => {
  return axios
    .get(ploturl + "liabilities/" + ticker_id)
    .then(res => {
      console.log(ticker_id);
      dispatch({
        type: GET_LIABILITIES,
        payload: res.data
      });
      console.log(res.data);
    })
    .catch(err => {
      console.log(err);
    });
};

export const getCashFlow = ticker_id => dispatch => {
  return axios
    .get(ploturl + "cashflow/" + ticker_id)
    .then(res => {
      console.log(ticker_id);
      dispatch({
        type: GET_CASHFLOW,
        payload: res.data
      });
      console.log(res.data);
    })
    .catch(err => {
      console.log(err);
    });
};
