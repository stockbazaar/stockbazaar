// Importing required types - Eli
import { ALL_SEARCH, COMPARE_SEARCH } from "./Types";

import axios from "axios";

// Connecting to the middletier service via route - Eli
const url = "https://stockbazaar-node.herokuapp.com/api/screener";

// Expecting the Result of the Mentioned Type by SearchString - Eli
export const allSearch = searchString => dispatch => {
  return axios
    .post(url + "/all", searchString)
    .then(res => {
      dispatch({
        type: ALL_SEARCH,
        payload: res.data.data
      });
    })
    .catch(err => {});
};

// Expecting the Result of the Mentioned Type by CompareSearchString - Eli
export const allSectorSearch = (searchString, sector) => dispatch => {
  return axios
    .post(url + "/sectorsearch", searchString, sector)
    .then(res => {
      dispatch({
        type: COMPARE_SEARCH,
        payload: res.data.data
      });
    })
    .catch(err => {});
};
