import * as action from "../StocksLanding";
import {
  GET_INDICES,
  GET_OVERVIEW,
  GET_PERFORMANCE,
  GET_GAINERS_MARGIN,
  GET_LOSERS_VALUATION,
  GET_LOSERS_MARGIN,
  GET_SECTORS,
  GET_INDUSTRIES,
  GET_SECTORS_PERFORMANCE,
  GET_INDUSTRY_PERFORMANCE,
  GET_GAINERS_VALUATION
} from "../Types";
import moxios from "moxios";
import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
const url = "https://stockbazaar-node.herokuapp.com/api/screener";

const middlewares = [thunk];
const mockstore = configureMockStore(middlewares);

describe("Testing user Action", () => {
  beforeEach(() => {
    moxios.install();
  });
  afterEach(() => {
    moxios.uninstall();
  });
  it("should create an acton with the GET_INDICES and the payload should be same as the api response when the response is 200", () => {
    const responseOfApi = [{ type: "LOADING_START" }, { type: "LOADING_STOP" }];
    moxios.stubRequest(url + "/indices", {
      status: 200,
      response: { data: responseOfApi }
    });
    const store = mockstore({});
    const expectedActions = [
      {
        type: GET_INDICES,
        payload: responseOfApi
      }
    ];
    return store.dispatch(action.getAllIndices()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
  it("should create an acton with the GET_OVERVIEW and the payload should be same as the api response when the response is 200", () => {
    const responseOfApi = [{}, {}, {}];
    moxios.stubRequest(url + "/overview", {
      status: 200,
      response: { data: responseOfApi }
    });
    const store = mockstore({});
    const expectedActions = [
      {
        type: GET_OVERVIEW,
        payload: responseOfApi
      }
    ];
    return store.dispatch(action.getOverview()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it("should create an acton with the GET_PERFORMANCE and the payload should be same as the api response when the response is 200", () => {
    const responseOfApi = [{}, {}, {}];
    moxios.stubRequest(url + "/performance", {
      status: 200,
      response: { data: responseOfApi }
    });
    const store = mockstore({});
    const expectedActions = [
      {
        type: GET_PERFORMANCE,
        payload: responseOfApi
      }
    ];
    return store.dispatch(action.getPerformance()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
  it("should create an acton with the GET_GAINERS_VALUATION and the payload should be same as the api response when the response is 200", () => {
    const responseOfApi = [{}, {}, {}];
    moxios.stubRequest(url + "/gainersvaluation", {
      status: 200,
      response: { data: responseOfApi }
    });
    const store = mockstore({});
    const expectedActions = [
      {
        type: GET_GAINERS_VALUATION,
        payload: responseOfApi
      }
    ];
    return store.dispatch(action.getGainersValuation()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
  it("should create an acton with the GET_LOSERS_VALUATION and the payload should be same as the api response when the response is 200", () => {
    const responseOfApi = [{}, {}, {}];
    moxios.stubRequest(url + "/losersvaluation", {
      status: 200,
      response: { data: responseOfApi }
    });
    const store = mockstore({});
    const expectedActions = [
      {
        type: GET_LOSERS_VALUATION,
        payload: responseOfApi
      }
    ];
    return store.dispatch(action.getLosersValuation()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
  it("should create an acton with the GET_GAINERS_MARGIN and the payload should be same as the api response when the response is 200", () => {
    const responseOfApi = [{}, {}, {}];
    moxios.stubRequest(url + "/gainersmargin", {
      status: 200,
      response: { data: responseOfApi }
    });
    const store = mockstore({});
    const expectedActions = [
      {
        type: GET_GAINERS_MARGIN,
        payload: responseOfApi
      }
    ];
    return store.dispatch(action.getGainersMargin()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
  it("should create an acton with the GET_LOSERS_MARGIN and the payload should be same as the api response when the response is 200", () => {
    const responseOfApi = [{}, {}, {}];
    moxios.stubRequest(url + "/losersmargin", {
      status: 200,
      response: { data: responseOfApi }
    });
    const store = mockstore({});
    const expectedActions = [
      {
        type: GET_LOSERS_MARGIN,
        payload: responseOfApi
      }
    ];
    return store.dispatch(action.getLosersMargin()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
  it("should create an acton with the GET_SECTORS and the payload should be same as the api response when the response is 200", () => {
    const responseOfApi = [{}, {}, {}];
    moxios.stubRequest(url + "/sectors", {
      status: 200,
      response: { data: responseOfApi }
    });
    const store = mockstore({});
    const expectedActions = [
      {
        type: GET_SECTORS,
        payload: responseOfApi
      }
    ];
    return store.dispatch(action.getSectors()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
  it("should create an acton with the GET_INDUSTRIES and the payload should be same as the api response when the response is 200", () => {
    const responseOfApi = [{}, {}, {}];
    moxios.stubRequest(url + "/industries", {
      status: 200,
      response: { data: responseOfApi }
    });
    const store = mockstore({});
    const expectedActions = [
      {
        type: GET_INDUSTRIES,
        payload: responseOfApi
      }
    ];
    return store.dispatch(action.getIndustries()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
  it("should create an acton with the GET_SECTORS_PERFORMANCE and the payload should be same as the api response when the response is 200", () => {
    const responseOfApi = [{}, {}, {}];
    moxios.stubRequest(url + "/sectorperformance", {
      status: 200,
      response: { data: responseOfApi }
    });
    const store = mockstore({});
    const expectedActions = [
      {
        type: GET_SECTORS_PERFORMANCE,
        payload: responseOfApi
      }
    ];
    return store.dispatch(action.getSectorsPerformance()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
  it("should create an acton with the GET_INDUSTRY_PERFORMANCE and the payload should be same as the api response when the response is 200", () => {
    const responseOfApi = [{}, {}, {}];
    moxios.stubRequest(url + "/industryperformance", {
      status: 200,
      response: { data: responseOfApi }
    });
    const store = mockstore({});
    const expectedActions = [
      {
        type: GET_INDUSTRY_PERFORMANCE,
        payload: responseOfApi
      }
    ];
    return store.dispatch(action.getIndustryPerformance()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
