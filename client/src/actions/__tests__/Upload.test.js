import * as action from "../Upload";
import { SUCCESS, ERROR } from "../Types";
import moxios from "moxios";
import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
const url = "https://stockbazaar-node.herokuapp.com/api/screener";

const middlewares = [thunk];
const mockstore = configureMockStore(middlewares);

describe("Testing user Action", () => {
  beforeEach(() => {
    moxios.install();
  });
  afterEach(() => {
    moxios.uninstall();
  });
  it("should create an acton withthe UPLOAD and the payload should be same as the api response when the response is 200", () => {
    const responseOfApi = [{}, {}, {}];
    moxios.stubRequest(url + "/upload", {
      status: 200,
      response: { data: responseOfApi }
    });
    const store = mockstore({});
    const expectedActions = [
      {
        type: SUCCESS,
        payload: responseOfApi
      }
    ];
    return store.dispatch(action.upload()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
  it("should create an action with the UPLOAD and the payload should be same as the api response when the response is 404", () => {
    const responseOfApi = [{}, {}, {}];
    moxios.stubRequest(url + "/upload", {
      status: 500,
      response: { data: responseOfApi }
    });
    const store = mockstore({});
    const expectedActions = [
      {
        type: ERROR,
        payload: responseOfApi
      }
    ];
    return store.dispatch(action.upload()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
  it("should create an acton withthe download and the payload should be same as the api response when the response is 200", () => {
    const responseOfApi = [{}, {}, {}];
    moxios.stubRequest(url + "/download/dummydata", {
      status: 200,
      response: { data: responseOfApi }
    });
    const store = mockstore({});
    const expectedActions = [
      {
        type: SUCCESS,
        payload: responseOfApi
      }
    ];
    return store.dispatch(action.downloadDummydata()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
  it("should create an acton with the Download  and the payload should be same as the api response when the response is 200", () => {
    const responseOfApi = [{}, {}, {}];
    moxios.stubRequest(url + "/download/dummydata", {
      status: 200,
      response: { data: responseOfApi }
    });
    const store = mockstore({});
    const expectedActions = [
      {
        type: SUCCESS,
        payload: responseOfApi
      }
    ];
    return store.dispatch(action.downloadDummydata()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
