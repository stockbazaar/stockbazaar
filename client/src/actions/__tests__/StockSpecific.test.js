import * as actions from "../stockSpecificAction";
import {
  GET_STOCKCHART,
  GET_EARNINGSVSREVENUE,
  GET_STOCK_DETAILS,
  GET_FINANCIAL_REPORTS,
  GET_PROFITLOSS
} from "../Types";
import moxios from "moxios";
import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

const url = "https://stockbazaar-node.herokuapp.com/api/screener";
const url2 = "https://stockbazaar-python.herokuapp.com";

describe("testing action for getStockChart", () => {
  beforeEach(() => {
    moxios.install();
  });
  afterEach(() => {
    moxios.uninstall();
  });
  it("checks for the positive response from the action getStockChart", () => {
    const responseOfApi = [];
    let ticker_id = 9;
    moxios.stubRequest(url2 + "/stockchart/" + ticker_id, {
      status: 200,
      response: { data: responseOfApi }
    });
    const store = mockStore({});
    const expectedActions = [
      {
        type: GET_STOCKCHART,
        payload: responseOfApi
      }
    ];
    return store.dispatch(actions.getStockChart()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
  it("checks for the negative response from the action getStockChart", () => {
    const responseOfApi = [];
    moxios.stubRequest(url2 + "/", {
      status: 404,
      response: { data: responseOfApi }
    });
    const store = mockStore({});
    const expectedActions = [];
    return store.dispatch(actions.getStockChart()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});

describe("testing action for getStockDetails", () => {
  beforeEach(() => {
    moxios.install();
  });
  afterEach(() => {
    moxios.uninstall();
  });
  it("checks for the positive response from the action getStockDetails", () => {
    const responseOfApi = [];
    let ticker_id = 9;
    moxios.stubRequest(url + "/getstockdetails/" + ticker_id, {
      status: 200,
      response: { data: responseOfApi }
    });
    const store = mockStore({});
    const expectedActions = [
      {
        type: GET_STOCK_DETAILS,
        payload: responseOfApi
      }
    ];
    return store.dispatch(actions.getStockDetails()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
  it("checks for the negative response from the action getStockDetails", () => {
    const responseOfApi = [];
    moxios.stubRequest(url + "/", {
      status: 404,
      response: { data: responseOfApi }
    });
    const store = mockStore({});
    const expectedActions = [];
    return store.dispatch(actions.getStockDetails()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
