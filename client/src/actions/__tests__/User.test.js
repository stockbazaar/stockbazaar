import * as action from "../Users";
import { SUCCESS, ERROR, CLEAR } from "../Types";
import moxios from "moxios";
import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
const url = "https://stockbazaar-node.herokuapp.com/api/screener";

const middlewares = [thunk];
const mockstore = configureMockStore(middlewares);

describe("Testing user Action", () => {
  beforeEach(() => {
    moxios.install();
  });
  afterEach(() => {
    moxios.uninstall();
  });

  it("should create an acton with the create_user and the payload should be same as the api response when the response is 200", () => {
    const responseOfApi = [{}];
    const obj = {
      email: "test@test.com",
      name: "test",
      password: "test123"
    };
    moxios.stubRequest(url + "/new", obj, {
      status: 200,
      response: { data: responseOfApi }
    });
    const store = mockstore({});
    const expectedActions = [
      {
        type: SUCCESS
      }
    ];
    return store.dispatch(action.createUser()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
  it("should create an acton with the login and the payload should be same as the api response when the response is 200", () => {
    const responseOfApi = [{}];
    moxios.stubRequest(url + "/login", {
      status: 404,
      response: { data: responseOfApi }
    });
    const store = mockstore({});
    const expectedActions = [
      {
        type: ERROR
      }
    ];
    return store.dispatch(action.userLogin()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  //login

  it("should create an acton with the login and the payload should be same as the api response when the response is 20*", () => {
    const responseOfApi = [{}];
    const obj = {
      email: "test@test.com",
      password: "test123"
    };
    moxios.stubRequest(url + "/login", {
      status: 200,
      response: { data: responseOfApi }
    });
    const store = mockstore({});
    const expectedActions = [
      {
        type: SUCCESS,
        payload: responseOfApi
      }
    ];
    return store.dispatch(action.userLogin(obj)).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
  it("should create an acton with the login and the payload should be same as the api response when the response is 20*", () => {
    const responseOfApi = [{}, {}, {}];
    moxios.stubRequest(url + "/login", {
      status: 404,
      response: { data: responseOfApi }
    });
    const store = mockstore({});
    const expectedActions = [
      {
        type: ERROR
      }
    ];
    return store.dispatch(action.userLogin()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  // otpVerify
  it("should create an acton with the login and the payload should be same as the api response when the response is 20*", () => {
    const responseOfApi = [{}];
    const obj = {
      email: "test@test.com"
    };
    moxios.stubRequest(url + "/sendotp", obj, {
      status: 200,
      response: { data: responseOfApi }
    });
    const store = mockstore({});
    const expectedActions = [
      {
        type: SUCCESS
      }
    ];
    return store.dispatch(action.sendOTP()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
  it("should create an acton with the login and the payload should be same as the api response when the response is 20*", () => {
    const responseOfApi = [{}];
    moxios.stubRequest(url + "/sendotp", {
      status: 404,
      response: { data: responseOfApi }
    });
    const store = mockstore({});
    const expectedActions = [
      {
        type: ERROR
      }
    ];
    return store.dispatch(action.sendOTP()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  //email_verfication

  it("should craete an acton withthe login and the payload should be same as the api response when the response is 20*", () => {
    const responseOfApi = [{}];
    const obj = {
      email: "test@test.com",
      name: "test",
      password: 13456
    };
    moxios.stubRequest(url + "/otpverification", obj, {
      status: 200,
      response: { data: responseOfApi }
    });
    const store = mockstore({});
    const expectedActions = [
      {
        type: SUCCESS
      }
    ];
    return store.dispatch(action.verifyOTPRegister()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
  it("should craete an acton withtye login and the payload should be same as the api response when the response is 20*", () => {
    const responseOfApi = [{}];
    moxios.stubRequest(url + "/otpverification", {
      status: 404,
      response: { data: responseOfApi }
    });
    const store = mockstore({});
    const expectedActions = [
      {
        type: ERROR
      }
    ];
    return store.dispatch(action.verifyOTPReset()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
  it("should clear the reducer", () => {
    const store = mockstore({});
    const expectedActions = [
      {
        type: CLEAR
      }
    ];
    return store.dispatch(action.clearReducer()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
