import { GET_ALL_INDUSTRIES_BY_SECTOR, GET_TICKERS_BY_FILTER } from "../Types";
import * as action from "../Screener";
import moxios from "moxios";
import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

const url = "https://stockbazaar-node.herokuapp.com/api/screener";

describe("testing action for getAllIndustriesBySector", () => {
  beforeEach(() => {
    moxios.install();
  });
  afterEach(() => {
    moxios.uninstall();
  });
  it("checks for the positive response from the action getAllIndustriesBySector", () => {
    const responseOfApi = [{}, {}, {}];
    moxios.stubRequest(url + "/getIndustriesBySector", {
      status: 200,
      response: { data: responseOfApi }
    });
    const store = mockStore({});
    const expectedActions = [
      {
        type: GET_ALL_INDUSTRIES_BY_SECTOR,
        payload: responseOfApi
      }
    ];
    return store.dispatch(action.getAllIndustriesBySector()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
  it("checks for the negative response from the action getAllIndustriesBySector", () => {
    const responseOfApi = [];
    moxios.stubRequest(url + "/getIndustriesBySector", {
      status: 404,
      response: { data: responseOfApi }
    });
    const store = mockStore({});
    const expectedActions = [];
    return store.dispatch(action.getAllIndustriesBySector()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});

describe("testing action for getTickersByFilter", () => {
  beforeEach(() => {
    moxios.install();
  });
  afterEach(() => {
    moxios.uninstall();
  });
  it("checks for the positive response from the action getTickersByFilter", () => {
    const responseOfApi = [{}, {}, {}];
    const apiData = {
      sectors: ["Technology"],
      industries: ["Computer Hardware"]
    };
    moxios.stubRequest(url + "/getTickersByFilter", apiData, {
      status: 200,
      response: { data: responseOfApi }
    });
    const store = mockStore({});
    const expectedActions = [
      {
        type: GET_TICKERS_BY_FILTER,
        payload: responseOfApi
      }
    ];
    return store.dispatch(action.getTickersByFilter()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
  it("checks for the negative response from the action getTickersByFilter", () => {
    const responseOfApi = [];
    const apiData = {};
    moxios.stubRequest(url + "/getTickersByFilter", apiData, {
      status: 404,
      response: { data: responseOfApi }
    });
    const store = mockStore({});
    const expectedActions = [];
    return store.dispatch(action.getTickersByFilter()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
