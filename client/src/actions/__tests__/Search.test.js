import * as action from "../SearchAction";
import { ALL_SEARCH } from "../Types";
import moxios from "moxios";
import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
const url = "https://stockbazaar-node.herokuapp.com/api/screener";

const middlewares = [thunk];
const mockstore = configureMockStore(middlewares);

describe("Testing user Action", () => {
  beforeEach(() => {
    moxios.install();
  });
  afterEach(() => {
    moxios.uninstall();
  });
  // Checking for Positive Case of the Search Action - Eli
  it("should create an action with the ALL_SEARCH and the payload should be same as the api response when the response is 200", () => {
    const responseOfApi = [{}, {}, {}];
    let searchString = "GOOG";
    moxios.stubRequest(url + "/all", {
      status: 200,
      response: { data: responseOfApi }
    });
    const store = mockstore({});
    const expectedActions = [
      {
        type: ALL_SEARCH,
        payload: responseOfApi
      }
    ];
    // Comparing result of the Mockstore and the result of API - Eli
    return store.dispatch(action.allSearch(searchString)).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  // Checking for Negative Case of the Search Action - Eli
  it("should create an action with the ALL_SEARCH and the payload should be same as the api response when the response is 404", () => {
    const responseOfApi = [{}, {}, {}];
    moxios.stubRequest(url + "/al", {
      status: 404,
      response: { data: responseOfApi }
    });
    const store = mockstore({});
    const expectedActions = [
      {
        type: ALL_SEARCH,
        payload: responseOfApi
      }
    ];
    // Comparing result of the Mockstore and the result of API - Eli
    return store.dispatch(action.allSearch(searchString)).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it("should create an action with the COMPARE_SEARCH and the payload should be same as the api response when the response is 200", () => {
    const responseOfApi = [{}, {}, {}];
    let value = { searchString: "AAPL", sector: "Technology" };
    moxios.stubRequest(url + "/sectorsearch", {
      status: 200,
      response: { data: responseOfApi }
    });
    const store = mockstore({});
    const expectedActions = [
      {
        type: COMPARE_SEARCH,
        payload: responseOfApi
      }
    ];
    // Comparing result of the Mockstore and the result of API - Eli
    return store.dispatch(action.allSectorSearch(value)).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
