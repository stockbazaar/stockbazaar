import { SUCCESS, ERROR, CLEAR } from "./Types";

import axios from "axios";
const url = "https://stockbazaar-node.herokuapp.com/api/screener";

export const createUser = user => dispatch => {
  return axios
    .post(url + "/new", user)
    .then(res => {
      dispatch({
        type: SUCCESS,
        //stores the success message from server
        payload: res.data.message
      });

      console.log(res.data);
      console.log("User created successfully");
    })
    .catch(err => {
      //stores the error message from server
      dispatch({ type: ERROR, payload: err.response.data.message });
      // console.log(err.response.data.message);
    });
};

//calls an api which provides user login and sets the  token in the api response in the local storage of client/user
export const userLogin = (user, history) => dispatch => {
  return axios
    .post(url + "/login", user)
    .then(res => {
      dispatch({
        type: SUCCESS,
        //stores the success message from server

        payload: res.data.message
      });
      localStorage.setItem("token", res.data.token);
      localStorage.setItem("isAdmin", res.data.isAdmin);
      //on succesful login, redirects the user to the home page after a small timeout so that the success message could be displayed on the same page
      setTimeout(() => {
        history.push("/");
      }, 1500);
    })
    .catch(err => {
      //stores the error message from server
      dispatch({ type: ERROR, payload: err.response.data.message });
    });
};
//calls the api to send OTP to email provided
export const sendOTP = email => dispatch => {
  return axios
    .post(url + "/sendotp", email)
    .then(res => {
      dispatch({
        type: SUCCESS,
        //stores the success message from server
        payload: res.data.message
      });
    })
    .catch(err => {
      //stores the error message from server
      dispatch({ type: ERROR, payload: err.response.data.message });
    });
};
//verifies the OTP and creates a new user
export const verifyOTPRegister = (user, history) => dispatch => {
  return axios
    .post(url + "/otpverification", user)
    .then(async res => {
      dispatch({
        type: SUCCESS,
        //stores the success message from server

        payload: res.data.message
      });
      //calls the action to create new user
      await dispatch(createUser(user));
      // calls the action to provide user login along with the history to redirect him/her to home page
      await dispatch(userLogin(user, history));
    })
    .catch(err => {
      //stores the error message from server
      dispatch({ type: ERROR, payload: err.response.data.message });
    });
};

//calls an api to verify the otp and then make an action call to reset the password
export const verifyOTPReset = (user, history) => dispatch => {
  return axios
    .post(url + "/otpverification", user)
    .then(async res => {
      dispatch({
        type: SUCCESS,
        //stores the success message from server
        payload: res.data.message
      });
      await dispatch(resetPassword(user, history));
    })
    .catch(err => {
      //stores the error message from server
      dispatch({ type: ERROR, payload: err.response.data.message });
    });
};

export const resetPassword = (user, history) => dispatch => {
  console.log("action", user);
  return axios
    .post(url + "/resetpassword", user)
    .then(res => {
      dispatch({
        type: SUCCESS,
        //stores the success message from server
        payload: res.data.message
      });
      //on succesful reset of password, redirects the user to the login page after a small timeout so that the success message could be displayed on the same page
      setTimeout(() => {
        history.push("/login");
      }, 1500);
    })
    .catch(err => {
      //stores the error message from server
      dispatch({ type: ERROR, payload: err.response.data.message });
    });
};
//makes call to an api which sends OTP to registered email addresses only...this is used when the user is already registered and wishes to reset his/her password
export const sendOTPtoRegisteredEmail = email => dispatch => {
  return axios
    .post(url + "/sendOTPtoRegisteredEmail", email)
    .then(res => {
      dispatch({
        type: SUCCESS,
        //stores the success message from server
        payload: res.data.message
      });
      console.log("OTP sent ");
    })
    .catch(err => {
      //stores the error message from server
      dispatch({ type: ERROR, payload: err.response.data.message });
    });
};
//calls an api to delete a user...this is for testing purpose only..i.e to delete the test user after testing for create api
export const deleteUser = id => dispatch => {
  return axios
    .post(url + "/delete" + id)
    .then(res => {
      dispatch({
        type: SUCCESS,
        //stores the success message from server
        payload: res.data.message
      });
    })
    .catch(err => {
      //stores the error message from server
      dispatch({ type: ERROR, payload: err.response.data.message });
    });
};
//clears the  reducer to avoid usage of previous values stored in it
export const clearReducer = () => dispatch => {
  dispatch({
    type: CLEAR
  });
};
