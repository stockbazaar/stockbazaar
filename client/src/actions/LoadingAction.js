// Importing the Required Types - Eli
import { LOADING_START, LOADING_STOP } from "./Types";

// Returning the Type According to the Function Defined - Eli
export const startLoading = () => {
  return {
    type: LOADING_START
  };
};

// Returning the Type According to the Function Defined - Eli
export const stopLoading = () => {
  return {
    type: LOADING_STOP
  };
};
