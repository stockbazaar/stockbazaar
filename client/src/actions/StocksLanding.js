import {
  GET_INDEX_OHLC,
  GET_INDICES,
  GET_OVERVIEW,
  GET_PERFORMANCE,
  GET_GAINERS_MARGIN,
  GET_LOSERS_VALUATION,
  GET_LOSERS_MARGIN,
  GET_SECTORS,
  GET_INDUSTRIES,
  GET_SECTORS_PERFORMANCE,
  GET_INDUSTRY_PERFORMANCE,
  GET_GAINERS_VALUATION,
  GET_INDICES_KAFKA
} from "./Types";

import axios from "axios";
import { stopLoading, startLoading } from "./LoadingAction";
const url = "https://stockbazaar-node.herokuapp.com/api/screener";

export const getAllIndices = () => dispatch => {
  dispatch(startLoading());
  return axios
    .get(url + "/indices", {})
    .then(res => {
      dispatch(stopLoading());
      dispatch({
        type: GET_INDICES,
        payload: res.data.data
      });
      return true;
    })
    .catch(err => {
      console.log(err);
    });
};
export const getOverview = () => dispatch => {
  return axios
    .get(url + "/overview", {})
    .then(res => {
      dispatch({
        type: GET_OVERVIEW,
        payload: res.data.data
      });
      return true;
    })
    .catch(err => {
      console.log(err);
    });
};
export const getPerformance = () => dispatch => {
  return axios
    .get(url + "/performance", {})
    .then(res => {
      dispatch({
        type: GET_PERFORMANCE,
        payload: res.data.data
      });
      return true;
    })
    .catch(err => {
      console.log(err);
    });
};
export const getGainersValuation = ticker => dispatch => {
  return axios
    .post(url + "/gainersvaluation", ticker)
    .then(res => {
      dispatch({
        type: GET_GAINERS_VALUATION,
        payload: res.data.data
      });
      return true;
    })
    .catch(err => {
      console.log(err);
    });
};
export const getGainersMargin = ticker => dispatch => {
  return axios
    .post(url + "/gainersmargin", ticker)
    .then(res => {
      dispatch({
        type: GET_GAINERS_MARGIN,
        payload: res.data.data
      });
      return true;
    })
    .catch(err => {
      console.log(err);
    });
};
export const getLosersValuation = ticker => dispatch => {
  return axios
    .post(url + "/losersvaluation", ticker)
    .then(res => {
      dispatch({
        type: GET_LOSERS_VALUATION,
        payload: res.data.data
      });
      return true;
    })
    .catch(err => {
      console.log(err);
    });
};
export const getLosersMargin = ticker => dispatch => {
  return axios
    .post(url + "/losersmargin", ticker)
    .then(res => {
      dispatch({
        type: GET_LOSERS_MARGIN,
        payload: res.data.data
      });
      return true;
    })
    .catch(err => {
      console.log(err);
    });
};
export const getSectors = () => dispatch => {
  return axios
    .get(url + "/sectors", {})
    .then(res => {
      dispatch({
        type: GET_SECTORS,
        payload: res.data.data
      });
      return true;
    })
    .catch(err => {
      console.log(err);
    });
};
export const getSectorsPerformance = () => dispatch => {
  return axios
    .get(url + "/sectorperformance", {})
    .then(res => {
      dispatch({
        type: GET_SECTORS_PERFORMANCE,
        payload: res.data.data
      });
      return true;
    })
    .catch(err => {
      console.log(err);
    });
};
export const getIndustries = () => dispatch => {
  return axios
    .get(url + "/industry", {})
    .then(res => {
      console.log("recieved industries");
      dispatch({
        type: GET_INDUSTRIES,
        payload: res.data.data
      });
      return true;
    })
    .catch(err => {
      console.log(err);
    });
};
export const getIndustryPerformance = () => dispatch => {
  return axios
    .get(url + "/industryperformance", {})
    .then(res => {
      dispatch({
        type: GET_INDUSTRY_PERFORMANCE,
        payload: res.data.data
      });
      return true;
    })
    .catch(err => {
      console.log(err);
    });
};

export const getIndexOHLC = id => dispatch => {
  console.log("inside getIndexOHLC");
  return axios
    .get("https://python-stockbazaar.herokuapp.com/ohlc/" + id)
    .then(res => {
      console.log(id);
      dispatch({
        type: GET_INDEX_OHLC,
        payload: res.data
      });
      console.log(res.data);
      return true;
    })
    .catch(err => {
      console.log(err);
    });
};

export const getIndicesKafka = () => dispatch => {
  return axios
    .get("https://kafka-producer-stockbazaar.herokuapp.com/aa")
    .then(res => {
      dispatch({
        type: GET_INDICES_KAFKA,
        payload: res.data
      });
      console.log(res);
      return true;
    })
    .catch(err => {
      console.log(err);
    });
};
