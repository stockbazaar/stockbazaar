import { SUCCESS, ERROR } from "./Types";
import axios from "axios";
const url = "https://stockbazaar-node.herokuapp.com/api/screener";

//[kavita] action to upload file
export const upload = data => dispatch => {
  return axios
    .post(url + "/upload", data, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => {
      dispatch({
        type: SUCCESS,
        //stores success message from server
        payload: res.data.message
      });
    })
    .catch(err => {
      dispatch({ type: ERROR, payload: err.response.data.message });
    });
};
//provides downloaded file with dummy data
export const downloadDummydata = () => dispatch => {
  console.log("download");
  return axios
    .get(url + "/download/dummydata", {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => {
      dispatch({
        type: SUCCESS,
        //stores success message from server
        payload: res.data
      });
    })
    .catch(err => {
      dispatch({ type: ERROR, payload: err.response.data.message });
    });
};
