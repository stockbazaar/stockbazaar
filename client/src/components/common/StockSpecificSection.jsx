import React, { Component } from "react";
import "../../assets/style/StockSpecificSection.css";

// Props to pass at the bottom of the code.

export default class StockSpecificSection extends Component {
  componentWillReceiveProps(nextProps) {
    // console.log("will recieve props", this.props);

    if (this.props.params) {
      if (nextProps.ticker_id != this.props.match.params.ticker_id) {
        this.props.getStockDetails(nextProps.ticker_id);
      }
    }
    // console.log("Props are", this.props);
  }

  render() {
    // console.log(this.props);
    return (
      <table className="statements-table">
        {this.props.headers.map(indicatorName => (
          <tr>
            {indicatorName === "date" ? (
              <td className="financialTableDataTitle">Indicator Name</td>
            ) : indicatorName === "Total Assets" ||
              indicatorName === "Total Liabilities" ||
              indicatorName === "Equity Before Minorities" ||
              indicatorName === "Gross Profit" ||
              indicatorName === "Current Assets" ||
              indicatorName === "Total Noncurrent Assets" ||
              indicatorName === "Total Equity" ||
              indicatorName === "Net Change in Cash" ? (
              <td className="financialsTotalTD">{indicatorName}</td>
            ) : (
              <td>{indicatorName}</td>
            )}
            <>
              {this.props.reportdata.map(value => (
                <>
                  {value[indicatorName] === "CHG%" ? (
                    <td className="financialTableDataTitle">
                      {value[indicatorName]}
                    </td>
                  ) : indicatorName === "date" ? (
                    <td className="financialTableDataTitle">
                      {" "}
                      {new Date(value[indicatorName]).toLocaleDateString(
                        "en-IN",
                        {
                          month: "short",
                          year: "2-digit"
                        }
                      )}
                    </td>
                  ) : indicatorName === "Total Assets" ||
                    indicatorName === "Total Liabilities" ||
                    indicatorName === "Equity Before Minorities" ||
                    indicatorName === "Gross Profit" ||
                    indicatorName === "Current Assets" ||
                    indicatorName === "Total Noncurrent Assets" ||
                    indicatorName === "Total Equity" ||
                    indicatorName === "Net Change in Cash" ? (
                    <td className="financialsTotalTD">
                      {value[indicatorName]
                        ? Math.round(value[indicatorName] * 100) / 100
                        : "-"}
                    </td>
                  ) : (
                    <td>
                      {value[indicatorName]
                        ? Math.round(value[indicatorName] * 100) / 100
                        : "-"}
                    </td>
                  )}
                </>
              ))}
            </>
          </tr>
        ))}
      </table>
    );
  }
}
