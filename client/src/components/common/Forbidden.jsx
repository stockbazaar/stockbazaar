import React, { Component } from "react";

export default class Forbidden extends Component {
  render() {
    return (
      <div>
        <h3 style={{ marginTop: "100px", marginLeft: "20px" }}>
          You are not authorized to access this page
        </h3>
      </div>
    );
  }
}
