import "../../assets/style/Tabs.css";
import React, { Component } from "react";

export default class Tabs extends Component {
  render() {
    return (
      <div>
        <table id={this.props.id} className={"stockTabs"}>
          <tr>
            {this.props.sectionData.map(data => (
              <th
                id="th"
                className={data.className}
                onClick={() => data.tabFunction(data.tabName)}
              >
                {console.log("classname", data.className)}
                {data.tabName}
              </th>
            ))}
          </tr>
        </table>
      </div>
    );
  }
}
