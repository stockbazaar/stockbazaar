import React, { Component } from "react";
import "../../assets/style/Navbar.css";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import stockBazaarLogo from "../../assets/images/stock-bazaar-primary.png";
import { allSearch } from "../../actions/SearchAction";
import { debounce } from "debounce";
// let decoded_token;

export class Navbar extends Component {
  // Declaring the SearchInput value
  state = {
    active: true,
    searchInput: "",
    isChecked: window.innerWidth >= 526 ? true : false,
    width: window.innerWidth,
    redirect: false,
    pageLink: this.props.pageLink ? this.props.pageLink : "",
    isSearchChecked: false
  };

  toggleChange = pageLink => {
    this.setState({ pageLink });
    window.innerWidth <= 526
      ? this.setState({ isChecked: !this.state.isChecked })
      : console.log("do nothing");
  };
  toggleSearch = () => {
    this.setState({ isSearchChecked: !this.state.isSearchChecked });
    // window.innerWidth <= 526
    //   ? this.setState({ isChecked: !this.state.isChecked })
    //   : console.log("do nothing");
  };
  closeSearch = () => {
    this.setState({ isSearchChecked: !this.state.isSearchChecked });
  };

  // onClick updating the state value and calling another function
  handleSearchInputChange = e => {
    this.setState({ [e.target.name]: e.target.value });
    this.inputSearch();
  };

  // Using the Debounce function to wait for a specific time for action call
  inputSearch = debounce(() => {
    let search_term = {
      searchInput: this.state.searchInput
    };
    // Providing the call to the Search Action
    this.props.allSearch(search_term);
  }, 750);

  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions);
  }

  updateDimensions = () => {
    this.setState({
      width: window.innerWidth,
      isChecked: window.innerWidth >= 526 ? true : false
    });
  };

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/login" />;
    }
  };

  logout = () => {
    localStorage.removeItem("token");
    localStorage.removeItem("isAdmin");
    this.setState({ redirect: true });
  };

  render() {
    return (
      <div className="nav-parent">
        {this.renderRedirect()}
        <nav className="menu">
          <ul>
            <input
              type="checkbox"
              id="search-btn"
              className="search-btn"
              checked={this.state.isSearchChecked}
              onClick={() => this.toggleSearch()}
            />
            <label
              className="search-icon-li"
              for="search-btn"
              style={
                {
                  // display: this.state.width < 768 ? "block" : "none"
                }
              }
            >
              <svg class="search-icon" viewBox="0 0 20 20">
                <path
                  fill="none"
                  d={
                    "M19.129,18.164l-4.518-4.52c1.152-1.373,1.852-3.143,1.852-" +
                    "5.077c0-4.361-3.535-7.896-7.896-7.896c-4.361,0-7.896,3.535-" +
                    "7.896,7.896s3.535,7.896,7.896,7.896c1.934,0,3.705-0.698,5.0" +
                    "78-1.853l4.52,4.519c0.266,0.268,0.699,0.268,0.965,0C19.396," +
                    "18.863,19.396,18.431,19.129,18.164z M8.567,15.028c-3.568,0-" +
                    "6.461-2.893-6.461-6.461s2.893-6.461,6.461-6.461c3.568,0,6.4" +
                    "6,2.893,6.46,6.461S12.135,15.028,8.567,15.028z"
                  }
                ></path>
              </svg>
            </label>

            {/* hidden checkbox to show and hide the menu in small screen device */}
            <input
              type="checkbox"
              id="menu-btn"
              className="menu-btn"
              checked={this.state.isChecked}
              onClick={() => this.toggleChange(this.state.pageLink)}
            />

            {/* label for checkbox which makes the hamburger menu */}
            <label
              className="menu-icon"
              for="menu-btn"
              // style={{
              //   display: this.state.width < 526 ? "block" : "none"
              // }}
            >
              <span className="bars"></span>
            </label>

            {/* brand name which is at left of navbar */}
            <li className="nav-brand-li">
              <Link
                to="/"
                id="nav-brand"
                onClick={() => {
                  this.setState({ pageLink: "" });
                }}
              >
                <img
                  src={stockBazaarLogo}
                  alt="stockBazaar logo"
                  className="brand-image"
                />
              </Link>
            </li>

            {/* stocks link */}
            <li
              className={
                this.state.pageLink === "stocks" ? "item active-link" : "item"
              }
              // style={{
              //   display: this.state.isChecked ? "block" : "none"
              // }}
              // onClick={() => this.toggleChange("stocks")}
              id="stocks-li"
            >
              <Link className="link" to="/" id="stocks">
                Stocks
              </Link>
            </li>

            {/* compare link */}
            <li
              className={
                this.state.pageLink === "compare" ? "item active-link" : "item"
              }
              // style={{
              //   display: this.state.isChecked ? "block" : "none"
              // }}
              // onClick={() => this.toggleChange("compare")}
              id="compare-li"
            >
              <Link className="link" to="/compare" id="compare">
                Compare
              </Link>
            </li>

            {/* screener link */}
            <li
              className={
                this.state.pageLink === "screener" ? "item active-link" : "item"
              }
              // style={{
              //   display: this.state.isChecked ? "block" : "none"
              // }}
              // onClick={() => this.toggleChange("screener")}
              id="screener-li"
            >
              <Link className="link" to="/screener" id="screener">
                Screener
              </Link>
            </li>

            {/* search box list item */}
            <li className="search-box-li">
              {/* search box to search for tickers,companies, sectors and industries */}
              <input
                id="searchInput"
                name="searchInput"
                className="search-box"
                value={this.state.searchInput}
                onChange={this.handleSearchInputChange}
                autoComplete="off"
                placeholder="Search for Companies"
                autofocus
              />
              <div className="close-icon" onClick={this.closeSearch}>
                <span></span>
              </div>
              {/* it changes to value of display on a condition */}
              <div
                className="search-result"
                style={{
                  display: this.state.searchInput ? "block" : "none"
                }}
              >
                {/* checks whether search has entries or not if not then prints no
              tickers found */}
                {/* checks for ticker array length in search
										if present map all the tickers and indexes available  */}
                {this.props.search.length !== 0 ? (
                  this.props.search.map(mapped_search => (
                    // Changing the display back to initialState onClick
                    <div
                      id="jest-div"
                      onClick={() => {
                        this.setState({
                          searchInput: ""
                        });
                      }}
                    >
                      {/* Providing Link to redirect on a Different Page  */}
                      <Link
                        className="search-list-item"
                        id="search-list-item"
                        to={{
                          pathname: "/stockspecific/" + mapped_search.ticker_id
                        }}
                      >
                        {/* The Ticker Names , Company, Sectors and Industies shown according to SearchInput */}
                        <div>
                          <img
                            src={
                              mapped_search.ticker_logo
                                ? "data:image/jpeg;base64," +
                                  mapped_search.ticker_logo
                                : stockBazaarLogo
                            }
                            alt="Ticker Logo"
                            className="brand-image-2"
                          />
                        </div>
                        <div
                          className="search-suggestion"
                          id={"ticker-search" + mapped_search.ticker_id}
                        >
                          <div
                            id="jest-search-list-item"
                            style={{ marginBottom: "5px" }}
                          >
                            {mapped_search.ticker_name}
                          </div>
                          <div style={{ marginBottom: "5px" }}>
                            {mapped_search.company_name}
                          </div>
                          <div display="block">
                            {mapped_search.industry == null
                              ? "NA"
                              : mapped_search.industry}{" "}
                            , {mapped_search.sector}
                          </div>
                        </div>
                      </Link>
                    </div>
                  ))
                ) : (
                  // Handling done if user searches something irrelevant
                  <div>
                    <p>NOTHING MATCHED YOUR CRITERIA</p>
                  </div>
                )}
              </div>
            </li>

            {!localStorage.getItem("token") ? (
              <>
                <li className="item user-button">
                  <Link className="link" to="/login">
                    <div id="login" className="btn-background">
                      Login
                    </div>
                  </Link>
                </li>
                <li className="item user-button">
                  <Link className="link" to="/register">
                    <div id="login" className="btn-border">
                      Register
                    </div>
                  </Link>
                </li>
              </>
            ) : (
              <>
                {localStorage.getItem("isAdmin") === "true" && (
                  <li className="item user-button">
                    <Link className="user-options-item" to="/upload">
                      <div id="upload" className="btn-background">
                        Upload
                      </div>
                    </Link>
                  </li>
                )}
                <li className="item user-button">
                  <Link className="user-options-item" onClick={this.logout}>
                    <div id="logout" className="btn-border">
                      Logout
                    </div>
                  </Link>
                </li>
              </>
            )}
          </ul>
        </nav>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  search: state.searchReducer.search
});

export default connect(mapStateToProps, { allSearch })(Navbar);
