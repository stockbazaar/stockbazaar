import React, { Component } from "react";
import TickerTables from "./TickersTables";
import "../../assets/style/gainers.css";

import Tabs from "./Tabs";
export default class ReusableSection extends Component {
  render() {
    return (
      <div>
        {console.log(this.props.sectionData)}
        <section className="gainers" id={this.props.section_id}>
          <h3 id="sectionHeading">{this.props.title}</h3>
          {/* {console.log("reusable sectionfunction", this.props.sectionData)} */}
          <Tabs
            tab_id={this.props.tab_id}
            sectionData={this.props.sectionData}
            sectionFunction={data => this.props.sectionFunction(data)}
          />

          <div className="reusableTicker">
            {/* importing the resusable component TickerTables */}
            <TickerTables
              id={this.props.table_id}
              columnNames={this.props.tableHeadings}
              rowsData={this.props.tableData}
              isIndex={this.props.isIndex}
              linesType={this.props.linesType}
              onClickBehaviour={this.props.onClickBehaviour}
            />
          </div>
        </section>
      </div>
    );
  }
}
