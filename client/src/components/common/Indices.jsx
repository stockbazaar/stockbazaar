import React, { Component } from "react";
import "../../assets/style/Indices.css";
import { connect } from "react-redux";
import { getIndicesKafka } from "../../actions/StocksLanding";
var stompClient = null;
const Stomp = require("stompjs");

var SockJS = require("sockjs-client");

export class Indices extends Component {
  state = {
    data: []
  };

  componentDidMount() {
    SockJS = new SockJS("https://stockbazaar-kafkaconsumer.herokuapp.com/ws");

    stompClient = Stomp.over(SockJS);
    stompClient.debug = null;
    stompClient.connect({}, this.onConnected, this.onError);
  }

  onConnected = () => {
    this.setState({
      data: []
    });

    // console.log("connected");

    // Subscribing to the public topic
    stompClient.subscribe("/topic/public", this.onMessageReceived);
  };

  onError = error => {
    this.setState({
      error:
        "Could not connect you to the Indices Server. Please refresh this page and try again!"
    });
  };

  onMessageReceived = msg => {
    var body = JSON.parse(msg.body);
    this.setState({
      data: body
    });
  };

  render() {
    return (
      <div id="kafkamainContainer">
        {this.state.data.map(i => (
          <div id="kafka">
            <div id="jest-indices">{i.tickerName}</div>
            <div>
              {Number(i.closing).toFixed(2) + " "}
              <span
                id={
                  String(i.changePercentage).charAt(0) == "-"
                    ? "change-ve"
                    : "changepositive"
                }
                style={{ fontSize: "14px" }}
              >
                ({Number(i.changePercentage).toFixed(2)}%)
              </span>
              {String(i.changePercentage).charAt(0) == "-" ? (
                <span
                  class="fa fa-caret-down"
                  style={{ color: "#ff4d4d", marginLeft: "3px" }}
                ></span>
              ) : (
                <span
                  class="fa fa-caret-up"
                  style={{ color: "#27ae60", marginLeft: "3px" }}
                ></span>
              )}
            </div>
          </div>
        ))}
      </div>
    );
  }
}

const mapStateToProps = state => ({});

export default connect(mapStateToProps, {
  getIndicesKafka
})(Indices);
