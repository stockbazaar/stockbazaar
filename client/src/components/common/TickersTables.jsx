import React, { Component } from "react";
import "../../assets/style/TickersTables.css";
import { connect } from "react-redux";
import { getIndexOHLC } from "../../actions/StocksLanding";
import { compose } from "redux";
import { withRouter } from "react-router-dom";

// Props to pass at the bottom of the code.

export class TickersTables extends Component {
  state = {
    rowsData: this.props.rowsData ? this.props.rowsData : [],
    isReverse: false,
    sortedColumn: "changePercentage",
    colorTab: "RUT"
  };

  componentWillReceiveProps(nextProps, prevstate) {
    this.setState({ rowsData: nextProps.rowsData });
  }

  // Sorts data function to sort the columns in asc or desc order
  sortData = columnName => {
    let sortedData;
    const { rowsData } = this.state;

    // sorting data in ascending order
    if (!this.state.isReverse) {
      sortedData = rowsData.sort((a, b) =>
        // checks if column is tickername then sorts by string else does by
        columnName === "tickerName"
          ? a[`${columnName}`].toLowerCase() < b[`${columnName}`].toLowerCase()
            ? -1
            : 1
          : a.tickerValue[`${columnName}`] < b.tickerValue[`${columnName}`]
          ? -1
          : 1
      );
    }
    // sorting data in descending order
    else {
      sortedData = rowsData
        .sort((a, b) =>
          columnName === "tickerName"
            ? a[`${columnName}`].toLowerCase() <
              b[`${columnName}`].toLowerCase()
              ? -1
              : 1
            : a.tickerValue[`${columnName}`] < b.tickerValue[`${columnName}`]
            ? -1
            : 1
        )
        .reverse();
    }
    this.setState({
      rowsData: sortedData,
      isReverse: !this.state.isReverse,
      sortedColumn: columnName
    });
  };

  // handles the click behaviour on a company or index
  handleOnClickBehaviour = funcParam => {
    this.setState({ colorTab: funcParam });
    console.log(this.state.colorTab, funcParam);
    if (this.props.isIndex) {
      this.props.getIndexOHLC(funcParam);
    } else {
      this.props.history.push(funcParam);
    }
  };
  render() {
    return (
      <div
        id={this.props.id + "-div"}
        className={
          this.props.isIndex ? "table-wrapper" : "table-wrapper table-border"
        }
      >
        <table id={this.props.id} className="table">
          <tr className={this.props.isIndex ? "" : "table-background"}>
            {/* Mapping the column names to table heading */}
            {/* [Ankit: 07-11-2019 - 10:15am] */}
            {/* [Modified: Ankit: 07-11-2019, 10:15am] */}
            {Object.entries(this.props.columnNames).map(columnName => (
              <>
                <th
                  className={
                    (this.props.isIndex ? "" : "table-background") +
                    " " +
                    this.props.linesType
                  }
                  id={columnName[0].toLowerCase().replace(/[^a-zA-Z0-9]/g, "-")}
                  key={columnName[0].toString()}
                  // Calls to sort function when clicked
                  onClick={() => this.sortData(columnName[0])}
                >
                  <div>
                    {columnName[1]}
                    <i
                      className="triangle-up"
                      style={{
                        borderBottomColor:
                          this.state.isReverse &&
                          this.state.sortedColumn === columnName[0]
                            ? "var(--primary-color)"
                            : "#808080"
                      }}
                    ></i>
                    <i
                      className="triangle-down"
                      style={{
                        borderTopColor:
                          !this.state.isReverse &&
                          this.state.sortedColumn === columnName[0]
                            ? "var(--primary-color)"
                            : "#808080"
                      }}
                    ></i>
                  </div>
                </th>
              </>
            ))}
          </tr>

          {/* Mapping rows to table row */}
          {/* [Ankit: 07-11-2019, 10:20am] */}
          {/* [Modified: Ankit, 13-11-2019, 04:31pm] */}
          {this.state.rowsData.map((singleRow, rowIndex) => (
            <tr>
              <td
                id={singleRow.tickerName}
                key={singleRow.tickerName}
                onClick={
                  this.props.isIndex
                    ? this.props.onClickBehaviour
                      ? () => this.handleOnClickBehaviour(singleRow.tickerName)
                      : null
                    : this.props.onClickBehaviour
                    ? () =>
                        this.handleOnClickBehaviour(
                          "/stockspecific/" + singleRow.tickerId
                        )
                    : null
                }
                style={{
                  cursor: "pointer",
                  backgroundColor:
                    this.state.colorTab === singleRow.tickerName &&
                    "var(--primary-color)",
                  color:
                    this.state.colorTab === singleRow.tickerName && "#ffffff"
                }}
                className={this.props.linesType}
              >
                <p>{singleRow.tickerName}</p>
                {singleRow.hasOwnProperty("tickerCompanyName") ? (
                  <span className="ticker-company">
                    {singleRow.tickerCompanyName}
                  </span>
                ) : (
                  <></>
                )}
              </td>
              {/* Mapping each row item to table row */}
              {/* [Ankit: 07-11-2019, 10:28] */}
              {/* [Modified: Ankit: 07-11-2019, 10:28] */}
              {Object.keys(singleRow.tickerValue).map((keyName, index) => (
                <td
                  id={keyName + "-" + (rowIndex + 1)}
                  key={keyName + "-" + (rowIndex + 1)}
                  className={this.props.linesType}
                  style={{
                    backgroundColor:
                      this.state.colorTab === singleRow.tickerName &&
                      "var(--primary-color)",
                    color:
                      this.state.colorTab !== singleRow.tickerName &&
                      (keyName === "changePercentage" || keyName === "change")
                        ? singleRow.tickerValue[keyName] > 0
                          ? "var(--green)"
                          : "var(--red)"
                        : "#ffffff"
                  }}
                >
                  {singleRow.tickerValue[keyName]}
                </td>
              ))}
            </tr>
          ))}
        </table>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  indexohlc: state.stocksLandingReducer.indexohlc
});

export default compose(
  withRouter,
  connect(mapStateToProps, { getIndexOHLC })
)(TickersTables);

// ------ Props to pass to use this component ------ //
// (1. id,    2. columnNames,   3. rowsData,    4. linesType,   5. isIndex)
// (6. onClickBehaviour)
//
// 1. id -> takes the id of the table.
//
// 2. columnNames -> takes an object of key as tickerValue keys and
//                   value as column name having column names
//    example: columnNames={
//                      tickerName: "Ticker",
//                      "Share Price": "Last",
//                      change: "CHG"
//                   }
//
// 3. rowsData -> takes an array of object containing each row data.
//             -> follow the same format shown in example.
//             -> if tickerCompanyName is not available you can skip it.
//    example: rowsData=[
//                {
//                  tickerId:1,
//                  tickerName: "ticker name",
//                  tickerCompanyName: "ticker company name",
//                  tickerValue: {
//                      key1: 46.156,
//                      key2: 5654.68,
//                      key3: 564
//                    }
//                },
//                {
//                  tickerId:2,
//                  tickerName: "ticker name",
//                  tickerCompanyName: "ticker company name",
//                  tickerValue: {
//                      key1: 46.156,
//                      key2: 5654.68,
//                      key3: 564
//                    }
//                }
//              ]

// 4. linesType -> takes either of two values
//              -> (1) "light" for light seperation lines
//              -> (2) "dark" for dark seperation lines

// 5. isIndex -> takes boolean value
// 6. onClickBehaviour -> takes boolean value

// ==> isIndex and onClickBehaviour works together
//      -> isIndex: true and onClickBehaviour true
//          +-> This will make the table heading background transparent and
//              table border none and calls to getStockChart function onClick
//          +-> Can be used for showing indices

//      -> isIndex: false and onClickBehaviour true
//          +-> This will add the table heading background and table border
//              and redirects to stockspecific page when clicked.
//          +-> Can be used for tickers with links

//      -> isIndex: true and onClickBehaviour false
//          +-> This will make the table heading background transparent and
//              table border none and does nothing when clicked
//          +-> Can be used for finance table.
