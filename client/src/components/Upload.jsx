import React, { Component } from "react";
// import axios from "axios";
import jwt from "jwt-decode";
import { upload, downloadDummydata } from "../actions/Upload";
import { clearReducer } from "../actions/Users";
import { connect } from "react-redux";

import "../assets/style/Admin.css";

export class UploadFile extends Component {
  state = {
    selectedFile: "",
    files: [],
    message: ""
  };
  componentDidMount() {
    //checks whether a token is present and the user is an admin
    // if not, redirects the user to forbidden page

    if (!localStorage.getItem("token")) {
      this.props.history.push("/forbidden");
    } else {
      const token = localStorage.getItem("token");
      const user = jwt(token);
      console.log(user);
      if (user.isAdmin === "false") this.props.history.push("/forbidden");
    }

    //clears error and success messages in the reducer after the component is mounted
    // so that the user doesn't see any previous messages
    this.props.clearReducer();
  }

  onChangeHandler = event => {
    this.setState({ selectedFile: event.target.files[0] });
    console.log("selected file", this.state.selectedFile);
  };

  onUpload = event => {
    this.props.clearReducer();
    const data = new FormData();
    this.setState({ selectedFile: event.target.files[0].name });
    console.log(event.target.files[0]);
    data.append("file", event.target.files[0]);
    // event.target.files[0].type != "csv"
    // this.setState({ message: "Invalid file type" })
    console.log(data);
    this.props.upload(data);
  };

  render() {
    return (
      <div>
        <div className="dropbox">
          <p
            style={{
              marginBottom: "20px"
            }}
          >
            The files should be in .csv format with delimiter as comma (,)
            {/* <div
              id="download-link"
              onClick={() => {
                this.props.downloadDummydata();
              }}
            >
              Download format{" "}
            </div> */}
          </p>
          <form>
            <input
              id="upload-file"
              type="file"
              accept=".csv"
              enctype="multipart/form-data"
              name="file"
              onChange={this.onUpload}
            />
            <p> Click or Drag and Drop to upload files</p>
            {/* <button onClick={this.onUpload}>Upload</button> */}
          </form>
          <div style={{ color: "white" }}>{this.state.selectedFile}</div>
          <br />
          {this.props.error ? (
            <div className="errorMessage">{this.props.error}</div>
          ) : (
            <div className="success-msg">{this.props.success}</div>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  error: state.userReducer.error,
  success: state.userReducer.success
});

export default connect(mapStateToProps, {
  upload,
  clearReducer,
  downloadDummydata
})(UploadFile);
