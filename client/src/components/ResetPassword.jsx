import React, { Component } from "react";
import {
  sendOTPtoRegisteredEmail,
  verifyOTPReset,
  clearReducer
} from "../actions/Users.js";
import { connect } from "react-redux";
import "../assets/style/ResetPassword.css";

export class ResetPassword extends Component {
  state = {
    email: "",
    password: "",
    confirmPassword: "",
    //indicates whether to show otp input field or not
    show_otp_field: false,
    otp: "",
    //indicates whether to show the custom error message or not
    showError: false,
    //custom error message for client-side validation
    errorMessage: ""
  };

  componentDidMount() {
    //checks whether a token is present and redirects the user to home page if (s)he's already logged in

    if (localStorage.getItem("token")) {
      this.props.history.push("/");
    }
    //clears error and success messages in the reducer after the component is mounted
    // so that the user doesn't see any previous messages
    this.props.clearReducer();
  }

  onChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  onSendOTPButtonClick = e => {
    //prevents the default behaviour of form i.e. showing the input fields values in url
    e.preventDefault();
    //checks whether the all the fields are filled  and validated
    // if not, then sets the state of showError to true to display custom error messages
    if (
      !this.state.email ||
      !this.state.password ||
      !this.state.confirmPassword
    ) {
      this.setState({
        showError: true,
        errorMessage: "Enter all the fields"
      });
    } else {
      this.setState({
        showError: false,
        errorMessage: ""
      });
      if (this.state.password !== this.state.confirmPassword) {
        this.setState({
          showError: true,
          errorMessage: "Passwords does not match"
        });
      } else {
        if (this.state.email.length < 5) {
          this.setState({
            showError: true,
            errorMessage: "Please enter valid  Email!"
          });
        } else {
          if (this.state.password.length < 5) {
            this.setState({
              showError: true,
              errorMessage:
                "Password length should be between 6 to 20 charachters!"
            });
          } else {
            this.setState({
              showError: false,
              errorMessage: ""
            });
            //after the input fields are vaidated  , an otp is  to user email provided

            let email = {
              email: this.state.email
            };
            this.props.sendOTPtoRegisteredEmail(email);

            //sets the show_otp_filed to true if there's no custom error message from client-side validation and the otp is  sent successfully
            this.setState({
              show_otp_field: this.state.error ? false : true
            });
          }
        }
      }
    }
  };

  onResetPasswordButtonClick = e => {
    //prevents the default behaviour of form i.e. showing the input fields values in url

    e.preventDefault();

    //checks whether  otp is filled  and validated and
    // if not, then sets the state of showError to true to display custom error messages
    if (!this.state.otp) {
      this.setState({
        showError: true,
        errorMessage: "Please Enter OTP"
      });
    } else {
      //hides the error message block and removes any previous mesaages

      this.setState({
        showError: false,
        errorMessage: ""
      });
      let user = {
        email: this.state.email,
        password: this.state.password,
        otp: this.state.otp
      };
      //makes a call to server with all the input fields along with history to redirect the user  to login page
      //the action call initially verifies the otp entered and then resets the password
      this.props.verifyOTPReset(user, this.props.history);
    }
  };

  render() {
    return (
      <div>
        <div className="reset-password-container">
          <form id="msform">
            <fieldset>
              <h1>Reset Password</h1>
              {/* shows the success message of server, if any  */}

              <div className="success-msg">{this.props.success}</div>

              <label className="outlined-inputs">
                <input
                  id="reset-email"
                  type="email"
                  name="email"
                  placeholder=" "
                  onChange={this.onChange}
                  value={this.state.email}
                  autoComplete="off"
                />
                <span>Email</span>
              </label>
              <label className="outlined-inputs">
                <input
                  id="reset-password"
                  type="password"
                  name="password"
                  placeholder=" "
                  onChange={this.onChange}
                  value={this.state.password}
                  autoComplete="off"
                />
                <span>Password</span>
              </label>
              <label className="outlined-inputs">
                <input
                  id="reset-confirm-password"
                  type="password"
                  name="confirmPassword"
                  placeholder=" "
                  onChange={this.onChange}
                  value={this.state.confirmPassword}
                  autoComplete="off"
                />
                <span>Confirm Password</span>
              </label>
              {/* the error message is shown only if th show_otp_filed is false to avoid duplicate display of error mesaage*/}

              {this.state.show_otp_field === false ? (
                <div className="errorMessage">
                  {/* shows the error message of server of any or shows the custom error message */}
                  {this.props.error ? (
                    <>{this.props.error}</>
                  ) : (
                    <span
                      className="errorMessage"
                      style={{
                        display: this.state.showError ? "block" : "none"
                      }}
                    >
                      {this.state.errorMessage}
                    </span>
                  )}
                </div>
              ) : null}
              {/* shows the otp input field with register button */}

              {this.state.show_otp_field === true ? (
                <>
                  <button
                    id="send-otp-button"
                    className="formbutton"
                    onChange={this.onChange}
                    onClick={this.onSendOTPButtonClick}
                  >
                    Send OTP
                  </button>
                  <label className="outlined-inputs">
                    <input
                      id="reset-otp"
                      type="password"
                      name="otp"
                      placeholder=" "
                      onChange={this.onChange}
                      value={this.state.otp}
                      autoComplete="off"
                    />
                    <span>OTP</span>
                  </label>
                  {/* shows the error message of server of any or shows the custom error message */}

                  <div className="errorMessage">
                    {/* dispatch error from node */}
                    {this.props.error ? (
                      <>{this.props.error}</>
                    ) : (
                      <> {this.state.errorMessage}</>
                    )}
                  </div>
                  <button
                    className="formbutton"
                    id="reset-button"
                    onChange={this.onChange}
                    onClick={this.onResetPasswordButtonClick}
                  >
                    Verify OTP and Reset Password
                  </button>
                </>
              ) : (
                // shows the send Otp button if the show otp field is false

                <button
                  id="send-otp-button"
                  className="formbutton"
                  onChange={this.onChange}
                  onClick={this.onSendOTPButtonClick}
                >
                  Send OTP
                </button>
              )}
            </fieldset>
          </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  error: state.userReducer.error,
  success: state.userReducer.success
});

export default connect(mapStateToProps, {
  sendOTPtoRegisteredEmail,
  verifyOTPReset,
  clearReducer
})(ResetPassword);
