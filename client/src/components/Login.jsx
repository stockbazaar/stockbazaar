import React, { Component } from "react";

import { userLogin, clearReducer } from "../actions/Users.js";
import { connect } from "react-redux";

import { Link } from "react-router-dom";
import "../assets/style/login.css";

export class Login extends Component {
  state = {
    email: "",
    password: "",
    //indicates whether to show the custom error message or not
    showError: false,
    //custom error message for client-side validation
    errorMessage: ""
  };

  componentDidMount() {
    //checks whether a token is present and redirects the user to home page if (s)he's already logged in
    if (localStorage.getItem("token")) {
      this.props.history.push("/");
    }
    //clears error and success messages in the reducer after the component is mounted
    // so that the user doesn't see any previous messages
    this.props.clearReducer();
  }

  onChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  onLogin = e => {
    //prevents the default behaviour of form i.e. showing the input fields values in url
    e.preventDefault();

    //checks whether the all the fields are filled  and validated and
    // sets the state of showError to true to display error messages
    if (!this.state.email || !this.state.password) {
      this.setState({
        showError: true,
        errorMessage: "Please fill all the fields"
      });
    } else {
      if (this.state.email.length < 5) {
        this.setState({
          showError: true,
          errorMessage: "Please enter valid email"
        });
      } else {
        if (this.state.password.length < 5) {
          this.setState({
            showError: true,
            errorMessage: "Please enter valid password"
          });
        } else {
          //if the input fields are validated then set the state of showError to false to hide the errorMessage
          this.setState({
            showError: false,
            errorMessage: ""
          });
          let user = {
            email: this.state.email,
            password: this.state.password
          };
          //makes a call to server with all the input fields along with history to redirect the user directly to home page
          this.props.userLogin(user, this.props.history);
          this.setState({
            email: "",
            password: ""
          });
        }
      }
    }
  };

  render() {
    return (
      <div>
        <div className="login-form-container">
          {/* toggle menu for login and register page */}

          <div style={{ height: "10%", margin: "0px" }}>
            <span className="login-tab" id="login-tab">
              Login
            </span>
            <Link to="/register">
              <span className="register-tab" id="register-tab">
                Register
              </span>
            </Link>
          </div>
          {/* shows the success message of server  */}
          <div className="success-msg">{this.props.success}</div>
          <form id="msform">
            <fieldset>
              <label className="outlined-inputs">
                <input
                  id="login-email"
                  type="email"
                  name="email"
                  placeholder=" "
                  onChange={this.onChange}
                  value={this.state.email}
                  autoComplete="off"
                />
                <span>Email</span>
              </label>
              <label className="outlined-inputs">
                <input
                  id="login-password"
                  type="password"
                  name="password"
                  placeholder=" "
                  onChange={this.onChange}
                  value={this.state.password}
                  autoComplete="off"
                />
                <span>Password</span>
              </label>
              <div className="errorMessage">
                {/* shows the error message of server of any or shows the custom error message */}
                {this.props.error ? (
                  <>{this.props.error}</>
                ) : (
                  <span
                    className="errorMessage"
                    style={{
                      display: this.state.showError ? "block" : "none"
                    }}
                  >
                    {this.state.errorMessage}
                  </span>
                )}
              </div>
              <button
                id="login-button"
                className="formbutton"
                onChange={this.onChange}
                onClick={this.onLogin}
              >
                Login
              </button>
              {/* <div>
                <label class="btn" for="modal-1">
                  Login
                </label>
              </div>
              <input class="modal-state" id="modal-1" type="checkbox" />
              <div class="modal">
                <label class="modal__bg" for="modal-1"></label>
                <div class="modal__inner">
                  <label class="modal__close" for="modal-1"></label>
                  <h2>{this.props.success}</h2>
                </div>
              </div> */}
              {/* link to reset password page */}
              <p>
                <Link className="link" to="/reset">
                  <span
                    id="forget-password-link"
                    style={{
                      color: " #F18019",
                      fontWeight: "normal"
                    }}
                  >
                    Forgot Password?
                  </span>
                </Link>
              </p>
            </fieldset>
          </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  error: state.userReducer.error,
  success: state.userReducer.success
});

export default connect(mapStateToProps, { userLogin, clearReducer })(Login);
