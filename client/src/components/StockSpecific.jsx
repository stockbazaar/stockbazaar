import React, { Component } from "react";
import { connect } from "react-redux";
import "../assets/style/StockSpecific.css";
import {
  getStockChart,
  getStockDetails,
  getFinancialReports,
  getEarningsVsRevenue,
  getMonteCarloGraph,
  getAllReports,
  getProfitLoss,
  getAssets,
  getLiabilities,
  getCashFlow
} from "../actions/stockSpecificAction";
import StockSpecificSection from "./common/StockSpecificSection";
import stockBazaarLogo from "../assets/images/stock-bazaar-primary.png";
import { Accordion, AccordionItem } from "react-sanfona";

export class StockSpecific extends Component {
  state = {
    isChecked: window.innerWidth > 425 ? true : false,
    width: window.innerWidth,
    ticker_id: this.props.match.params.ticker_id
  };

  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions);
    this.props.getStockDetails(this.state.ticker_id);
    this.props.getStockChart(this.state.ticker_id);
    this.props.getMonteCarloGraph(this.state.ticker_id);
    this.props.getFinancialReports(this.state.ticker_id);
    this.props.getEarningsVsRevenue(this.state.ticker_id);
    this.props.getProfitLoss(this.state.ticker_id);
    this.props.getAssets(this.state.ticker_id);
    this.props.getLiabilities(this.state.ticker_id);
    this.props.getCashFlow(this.state.ticker_id);
    console.log(this.props);
  }

  updateDimensions = () => {
    this.setState({
      width: window.innerWidth,
      isChecked: window.innerWidth > 425 ? true : false
    });
  };
  //[kavita 20-11-2019]
  // provides download of all the financial reports in csv format
  //needs the reports as initialObject along with the name of the file provided to user for the downloaded file
  exportCSVFile = (initialObject, fileTitle) => {
    // converts initialObject to csv i.e comma seperated values as it is in JSON format
    var JSONData = JSON.stringify(initialObject);
    var arrData = typeof JSONData != "object" ? JSON.parse(JSONData) : JSONData;
    var CSV = "",
      row = "",
      headers = [],
      f1 = 0;
    // initially stores the headers i.e. all unique keys in object
    for (var i = 0; i < arrData.length; i++) {
      for (var index in arrData[i]) {
        index = index.replace(",", "&");
        if (!row.includes(index)) {
          row += index + ",";
          headers.push(index);
        }
      }
      row = row.slice(0, -1);
    }
    CSV += row + "\r\n";
    // stores the value for corresponding keys and adds zero when none is obtained
    for (var l in arrData) {
      var data = "";
      for (var k in headers) {
        for (var name in arrData[l]) {
          // if the header key matches the key of object, the value is appended
          if (name == headers[k]) {
            f1 = arrData[l][name];
            break;
          } else {
            // if the header key doesn't find any matches in the key of object, then zero  is appended
            f1 = 0;
          }
        }
        data += f1 + ",";
      }
      data.slice(0, data.length - 1);
      CSV += data + "\r\n";
    }
    // the title of the downloaded file is assigned
    var exportedFilenmae = fileTitle + ".csv" || "export.csv";
    // makes a blob object of the  converted CSV with file type
    var blob = new Blob([CSV], { type: "text/csv;charset=utf-8;" });
    if (navigator.msSaveBlob) {
      navigator.msSaveBlob(blob, exportedFilenmae);
    } else {
      //creates a hidden link to download the blob object and triggers download in the user's machine
      var link = document.createElement("a");
      if (link.download !== undefined) {
        var url = URL.createObjectURL(blob);
        link.setAttribute("href", url);
        link.setAttribute("download", exportedFilenmae);
        link.style.visibility = "hidden";
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    }
  };
  // an action call is made to get all the financial reports for all years of the given ticker id , so that the data is fetched only if the user wishes to download it
  // this function is called with the ticker name as the name of downloaded file
  onDownload = async fileTitle => {
    await this.props.getAllReports(this.state.ticker_id);
    // call to convert the object to csv and trigger a download for the same
    this.exportCSVFile(this.props.allreports, fileTitle);
  };
  render() {
    let reports = this.props
      ? this.props.financialdetails.datesCF
        ? this.props.financialdetails.datesCF.length > 0
          ? this.props.financialdetails.datesCF
          : []
        : []
      : [];

    return (
      <div>
        <div className="stockspecific-container">
          {this.props.stockdetails.map(stockdetails => (
            <div className="profile">
              <div className="firstprofilecolumn">
                <img
                  className="firstprofilecolumn-logo"
                  src={
                    stockdetails.ticker_logo
                      ? "data:image/jpeg;base64," + stockdetails.ticker_logo
                      : stockBazaarLogo
                  }
                />
                <p className="firstprofilecolumn-price">
                  <b>{stockdetails.last_price} USD</b>
                </p>
                <p className="firstprofilecolumn-lastprice">
                  LAST PRICE (
                  {new Date(stockdetails.last_date).toLocaleDateString(
                    "en-IN",
                    {
                      month: "short",
                      day: "2-digit",
                      year: "numeric"
                    }
                  )}
                  )
                </p>
              </div>
              <div className="secondprofilecolumn">
                {stockdetails.company_name ? (
                  <p
                    className="secondprofilecolumn-name"
                    id="positivecompany_name"
                  >
                    {stockdetails.company_name}
                    {/* [kavita 20-11-2019] download icon which calls a function to provide download  */}
                    <i
                      style={{
                        fontSize: "25px",
                        cursor: "pointer",
                        marginLeft: "10px"
                      }}
                      class="fa fa-download"
                      aria-hidden="true"
                      onClick={() => {
                        this.onDownload(stockdetails.ticker_name);
                      }}
                    ></i>
                  </p>
                ) : (
                  <p
                    className="secondprofilecolumn-name"
                    id="positivecompanyname"
                  >
                    NA
                  </p>
                )}
                <p className="secondprofilecolumn-ticker">
                  {stockdetails.ticker_name}
                </p>
                <p className="secondprofilecolumn-change">
                  {stockdetails.change} ({stockdetails.changePercentage}%)
                </p>
              </div>
            </div>
          ))}
          {this.state.isChecked ? (
            <div className="secondary-nav" id="secondarynav">
              <a href="#ratios">Ratios</a>
              <a href="#stock-chart">Price</a>
              <a href="#pandl">Profit & Loss</a>
              <a href="#balancesheet">Balance Sheet</a>
              <a href="#cashflow">Cash Flow</a>
            </div>
          ) : (
            <div className="secondary-nav">
              <a href="#ratios">Ratios</a>
              <a href="#stock-chart">Price</a>
              <a href="#pandl">Profit & Loss</a>

              <div class="secondary-nav-dropdown" id="secondarynav">
                <button class="secondary-nav-dropdown-dropbtn">More</button>
                <div class=" secondary-nav-dropdown-dropdown-content">
                  <a href="#pandl">Profit & Loss</a>
                  <a href="#balancesheet">Balance Sheet</a>
                  <a href="#cashflow">Cash Flow</a>
                </div>
              </div>
            </div>
          )}
          {!String(this.props.stockdetails.profile).length == 0 && (
            <div className="description" id="description">
              <Accordion>
                {this.props.stockdetails.map(stockdetails => (
                  <AccordionItem
                    title="Company Profile :"
                    className="description-heading"
                  >
                    <div className="description-input">
                      <p>{stockdetails.profile}</p>
                    </div>
                  </AccordionItem>
                ))}
              </Accordion>
            </div>
          )}

          <div className="ratios" id="ratios">
            <p className="ratios-heading">Ratios</p>
            {this.props.financialdetails.hasOwnProperty("ratiosarray")
              ? this.props.financialdetails.ratiosarray.map(
                  financialdetails => (
                    <div className="ratios-flex">
                      <div className="first-ratio">
                        <p>{financialdetails["EPS"]}</p>
                        <p>EPS</p>
                      </div>
                      <div className="second-ratio">
                        <p>{Number(financialdetails["P/E"]).toFixed(2)}</p>
                        <p>P/E</p>
                      </div>
                      <div className="third-ratio">
                        <p>{financialdetails["Debt to Assets Ratio"]}</p>
                        <p>Debt to Assets Ratio</p>
                      </div>
                      <div className="fourth-ratio">
                        <p>{financialdetails["Liabilities to Equity Ratio"]}</p>
                        <p>Liabilities to Equity Ratio</p>
                      </div>
                      <div className="fifth-ratio">
                        <p>{financialdetails["EV / EBITDA"]}</p>
                        <p>EV/EBITDA</p>
                      </div>
                      <div className="sixth-ratio">
                        <p>{financialdetails["Return on Equity"]}</p>
                        <p>Return on Equity</p>
                      </div>
                    </div>
                  )
                )
              : console.log(null)}
          </div>

          <div className="stock-chart" id="stock-chart">
            <div className="stockchart-heading">
              <span>Stock Chart</span>
            </div>
            <div className="stock-graph">
              <iframe
                src={this.props.stockGraph}
                style={{
                  width: "100%",
                  height: "500px",
                  border: "none"
                }}
              ></iframe>
            </div>
          </div>

          <div
            className="earnings-revenue-history"
            id="earnings-revenue-history"
          >
            {/* [kavita ] Monte Carlo graph */}
            {console.log(this.props.montecarlograph)}

            <p className="earnings-revenue-history-heading">
              Earnings & Revenue History
            </p>

            <div className="earningsvsrevenue-graph">
              <iframe
                src={this.props.earningsvsrevenue}
                style={{
                  width: "100%",
                  height: "500px",
                  border: "none"
                }}
              ></iframe>
            </div>
          </div>

          <div className="monte-carlo-graph">
            {/* <h4>Graph</h4> */}
            <div className="monte-carlo-graph-graph">
              <iframe
                src={this.props.montecarlograph}
                style={{
                  width: "100%",
                  height: "500px",
                  border: "none"
                }}
              ></iframe>
            </div>
          </div>

          <div className="statements" id="pandl">
            <p className="statements-heading">Profit & Loss</p>
            <StockSpecificSection
              headers={[
                "date",
                "Revenues",
                "EBITDA",
                "Cash   From Operating Activities",
                "Net Profit",
                "Gross Profit"
              ]}
              reportdata={reports}
            />
          </div>

          <div className="statement-graph">
            {/* <h4>Graph</h4> */}
            <div className="statement-graph-graph">
              <iframe
                src={this.props.profitloss}
                style={{
                  width: "100%",
                  height: "500px",
                  border: "none"
                }}
              ></iframe>
            </div>
          </div>

          <div className="bsstatements" id="balancesheet">
            <p className="bsstatements-heading">Balance Sheet</p>
            <StockSpecificSection
              headers={[
                "date",
                "Cash, Cash Equivalents & Short Term Investments",
                "Receivables",
                "Other Short Term Assets",
                "Current Assets",
                "Net PP&E",
                "Long Term Investments & Receivables",
                "Total Noncurrent Assets",
                "Total Assets",
                "Short term debt",
                "Current Liabilities",
                "Long Term Debt",
                "Total Noncurrent Liabilities",
                "Total Liabilities",
                "Retained Earnings",
                "Equity Before Minorities",
                "Total Equity"
              ]}
              reportdata={reports}
            />
          </div>

          <div className="bs-statement-graph">
            {/* <h4>Graph</h4> */}
            <div className="statement-graph-graph">
              <iframe
                src={this.props.assets}
                style={{
                  width: "100%",
                  height: "500px",
                  border: "none"
                }}
              ></iframe>
              <iframe
                src={this.props.liabilities}
                style={{
                  width: "100%",
                  height: "500px",
                  border: "none"
                }}
              ></iframe>
            </div>
          </div>

          <div className="cfstatements" id="cashflow">
            <p className="cfstatements-heading">Cash Flow</p>
            <StockSpecificSection
              headers={[
                "date",
                "Depreciation & Amortisation",
                "Change in Working Capital",
                "Cash From Operating Activities",
                "Net Change in PP&E & Intangibles",
                "Cash From Investing Activities",
                "Dividends",
                "Cash From Financing Activities",
                "Net Change in Cash"
              ]}
              reportdata={reports}
            />
          </div>

          <div className="cf-statement-graph">
            {/* <h4>Graph</h4> */}
            <div className="statement-graph-graph">
              <iframe
                src={this.props.cashflow}
                style={{
                  width: "100%",
                  height: "500px",
                  border: "none"
                }}
              ></iframe>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  stockGraph: state.stockSpecificReducer.stockGraph,
  stockdetails: state.stockSpecificReducer.stockdetails,
  financialdetails: state.stockSpecificReducer.financialdetails,
  earningsvsrevenue: state.stockSpecificReducer.earningsvsrevenue,
  montecarlograph: state.stockSpecificReducer.montecarlograph,
  allreports: state.stockSpecificReducer.allreports,
  profitloss: state.stockSpecificReducer.profitloss,
  assets: state.stockSpecificReducer.assets,
  liabilities: state.stockSpecificReducer.liabilities,
  cashflow: state.stockSpecificReducer.cashflow
});

export default connect(mapStateToProps, {
  getStockChart,
  getStockDetails,
  getFinancialReports,
  getEarningsVsRevenue,
  getMonteCarloGraph,
  getAllReports,
  getProfitLoss,
  getAssets,
  getLiabilities,
  getCashFlow
})(StockSpecific);
