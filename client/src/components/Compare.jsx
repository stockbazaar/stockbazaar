import React, { Component } from "react";
import "../assets/style/Compare.css";
import { debounce } from "debounce";
import { allSearch, allSectorSearch } from "../actions/SearchAction";
import { connect } from "react-redux";
import stockBazaarLogo from "../assets/images/stock-bazaar-primary.png";
import {
  getStockDetails,
  getStockDetails2,
  getFinancialReports,
  getFinancialReports2
} from "../actions/stockSpecificAction";
import { Accordion, AccordionItem } from "react-sanfona";
import StockSpecificSection from "./common/StockSpecificSection";

export class Compare extends Component {
  state = {
    searchInput: "",
    searchInput2: "",
    sector: ""
  };

  componentDidMount() {
    //checks whether a token is present and redirects the user to home page if (s)he's already logged in
    if (!localStorage.getItem("token")) {
      this.props.history.push("/login");
    }
  }

  componentWillReceiveProps(nextProps) {
    console.log("Next", nextProps);
    if (nextProps.stockdetails.length > 0)
      this.setState({ sector: nextProps.stockdetails[0].sector });
  }

  // onClick updating the state value and calling another function
  handleSearchInputChange = e => {
    this.setState({ [e.target.name]: e.target.value });
    this.inputSearch();
  };

  // Using the Debounce function to wait for a specific time for action call
  inputSearch = debounce(() => {
    let search_term = {
      searchInput: this.state.searchInput
    };
    // Providing the call to the Search Action
    this.props.allSearch(search_term);
  }, 750);

  // onClick updating the state value and calling another function
  handleSearchInputChange2 = e => {
    this.setState({ [e.target.name]: e.target.value });
    this.inputSearch2();
  };

  // Using the Debounce function to wait for a specific time for action call
  inputSearch2 = debounce(() => {
    let search_term = {
      searchInput2: this.state.searchInput2,
      sector: this.state.sector
    };
    // Providing the call to the Search Action
    this.props.allSectorSearch(search_term);
  }, 750);

  render() {
    let reports = this.props
      ? this.props.financialdetails.datesCF
        ? this.props.financialdetails.datesCF.length > 0
          ? this.props.financialdetails.datesCF
          : []
        : []
      : [];
    let reports2 = this.props
      ? this.props.financialdetails2.datesCF
        ? this.props.financialdetails2.datesCF.length > 0
          ? this.props.financialdetails2.datesCF
          : []
        : []
      : [];
    return (
      <div>
        <div className="search-company-div">
          <div className="compare-div">
            {this.props.stockdetails.length === 0 ? (
              <>
                <div id="compare-search-div">
                  <input
                    name="searchInput"
                    id="search-company-input"
                    placeholder="Search for a company to compare"
                    value={this.state.searchInput}
                    onChange={this.handleSearchInputChange}
                    autoComplete="off"
                  ></input>
                </div>
                <div
                  className="search-result-1"
                  style={{
                    display: this.state.searchInput ? "block" : "none"
                  }}
                >
                  {/* checks whether search has entries or not if not then prints no
              tickers found */}
                  {/* checks for ticker array length in search
										if present map all the tickers and indexes available  */}
                  {this.props.search.length !== 0 ? (
                    this.props.search.map(mapped_search => (
                      // Changing the display back to initialState onClick
                      <div
                        id="jest-div-1"
                        onClick={() => {
                          this.setState({
                            searchInput: ""
                          });
                          this.props.getStockDetails(mapped_search.ticker_id);
                          this.props.getFinancialReports(
                            mapped_search.ticker_id
                          );
                        }}
                      >
                        <div className="search-list-item">
                          {/* The Ticker Names , Company, Sectors and Industies shown according to SearchInput */}
                          <div>
                            <img
                              src={
                                mapped_search.ticker_logo
                                  ? "data:image/jpeg;base64," +
                                    mapped_search.ticker_logo
                                  : stockBazaarLogo
                              }
                              alt="Ticker Logo"
                              className="brand-image-2"
                            />
                          </div>
                          <div
                            className="search-suggestion"
                            id={"ticker-search" + mapped_search.ticker_id}
                          >
                            <div
                              id="jest-search-list-item-1"
                              style={{ marginBottom: "5px" }}
                            >
                              {mapped_search.ticker_name}
                            </div>
                            <div style={{ marginBottom: "5px" }}>
                              {mapped_search.company_name}
                            </div>
                            <div display="block">
                              {mapped_search.industry == null
                                ? "NA"
                                : mapped_search.industry}{" "}
                              , {mapped_search.sector}
                            </div>
                          </div>
                        </div>
                      </div>
                    ))
                  ) : (
                    // Handling done if user searches something irrelevant
                    <div>
                      <p>NOTHING MATCHED YOUR CRITERIA</p>
                    </div>
                  )}
                </div>
              </>
            ) : (
              <div>
                {this.props.stockdetails.map(stockdetails => (
                  <div className="profile-1">
                    <div className="firstprofilecolumn">
                      <img
                        className="firstprofilecolumn-logo"
                        src={
                          stockdetails.ticker_logo
                            ? "data:image/jpeg;base64," +
                              stockdetails.ticker_logo
                            : stockBazaarLogo
                        }
                      />
                      <p className="firstprofilecolumn-price">
                        <b>{stockdetails.last_price} USD</b>
                      </p>
                      <p className="firstprofilecolumn-lastprice">
                        LAST PRICE (
                        {new Date(stockdetails.last_date).toLocaleDateString(
                          "en-IN",
                          {
                            month: "short",
                            day: "2-digit",
                            year: "numeric"
                          }
                        )}
                        )
                      </p>
                    </div>
                    <div className="secondprofilecolumn">
                      {stockdetails.company_name ? (
                        <p
                          className="secondprofilecolumn-name"
                          id="positivecompany_name"
                        >
                          {stockdetails.company_name}
                        </p>
                      ) : (
                        <p
                          className="secondprofilecolumn-name"
                          id="positivecompanyname"
                        >
                          NA
                        </p>
                      )}
                      <p className="secondprofilecolumn-ticker">
                        {stockdetails.ticker_name}
                      </p>
                      <p className="secondprofilecolumn-change">
                        {stockdetails.change} ({stockdetails.changePercentage}%)
                      </p>
                    </div>
                  </div>
                ))}
                <br></br>
                <div className="ratios-1" id="ratios-1">
                  <p className="ratios-heading-1">Ratios</p>
                  {this.props.financialdetails.hasOwnProperty("ratiosarray")
                    ? this.props.financialdetails.ratiosarray.map(
                        financialdetails => (
                          <div className="ratios-flex-1">
                            <div className="first-ratio-1">
                              <p>{financialdetails["EPS"]}</p>
                              <p>EPS</p>
                            </div>
                            <div className="second-ratio-1">
                              <p>
                                {Number(financialdetails["P/E"]).toFixed(2)}
                              </p>
                              <p>P/E</p>
                            </div>
                            <div className="third-ratio-1">
                              <p>{financialdetails["Debt to Assets Ratio"]}</p>
                              <p>Debt to Assets Ratio</p>
                            </div>
                            <div className="fourth-ratio-1">
                              <p>
                                {
                                  financialdetails[
                                    "Liabilities to Equity Ratio"
                                  ]
                                }
                              </p>
                              <p>Liabilities to Equity Ratio</p>
                            </div>
                            <div className="fifth-ratio-1">
                              <p>{financialdetails["EV / EBITDA"]}</p>
                              <p>EV/EBITDA</p>
                            </div>
                            <div className="sixth-ratio-1">
                              <p>{financialdetails["Return on Equity"]}</p>
                              <p>Return on Equity</p>
                            </div>
                          </div>
                        )
                      )
                    : console.log(null)}
                </div>
                <br></br>
                {!String(this.props.stockdetails.profile).length == 0 && (
                  <div className="description-1" id="description-1">
                    <Accordion>
                      {this.props.stockdetails.map(stockdetails => (
                        <AccordionItem
                          title="Company Profile :"
                          className="description-heading-1"
                        >
                          <div className="description-input-1">
                            <p>{stockdetails.profile}</p>
                          </div>
                        </AccordionItem>
                      ))}
                    </Accordion>
                  </div>
                )}
                <br></br>
                <div className="statements-1" id="pandl-1">
                  <p className="statements-heading-1">Profit & Loss</p>
                  <StockSpecificSection
                    headers={[
                      "date",
                      "Revenues",
                      "EBITDA",
                      "Cash   From Operating Activities",
                      "Net Profit",
                      "Gross Profit"
                    ]}
                    reportdata={reports}
                  />
                </div>
                <br></br>
                <div className="bsstatements-1" id="balancesheet-1">
                  <p className="bsstatements-heading-1">Balance Sheet</p>
                  <StockSpecificSection
                    headers={[
                      "date",
                      "Cash, Cash Equivalents & Short Term Investments",
                      "Receivables",
                      "Other Short Term Assets",
                      "Current Assets",
                      "Net PP&E",
                      "Long Term Investments & Receivables",
                      "Total Noncurrent Assets",
                      "Total Assets",
                      "Short term debt",
                      "Current Liabilities",
                      "Long Term Debt",
                      "Total Noncurrent Liabilities",
                      "Total Liabilities",
                      "Retained Earnings",
                      "Equity Before Minorities",
                      "Total Equity"
                    ]}
                    reportdata={reports}
                  />
                </div>
                <br></br>
                <div className="cfstatements-1" id="cashflow-1">
                  <p className="cfstatements-heading-1">Cash Flow</p>
                  <StockSpecificSection
                    headers={[
                      "date",
                      "Depreciation & Amortisation",
                      "Change in Working Capital",
                      "Cash From Operating Activities",
                      "Net Change in PP&E & Intangibles",
                      "Cash From Investing Activities",
                      "Dividends",
                      "Cash From Financing Activities",
                      "Net Change in Cash"
                    ]}
                    reportdata={reports}
                  />
                </div>
              </div>
            )}
          </div>

          <div className="compare-div">
            {this.props.stockdetails2.length === 0 ? (
              <>
                <div id="compare-search-div">
                  <input
                    name="searchInput2"
                    id="search-company-input"
                    placeholder="Search for a company to compare"
                    value={this.state.searchInput2}
                    onChange={this.handleSearchInputChange2}
                    autoComplete="off"
                  ></input>
                </div>
                <div
                  className="search-result-1"
                  style={{
                    display: this.state.searchInput2 ? "block" : "none"
                  }}
                >
                  {/* checks whether search has entries or not if not then prints no
              tickers found */}
                  {/* checks for ticker array length in search
										if present map all the tickers and indexes available  */}
                  {this.props.compare.length !== 0 ? (
                    this.props.compare.map(mapped_search => (
                      // Changing the display back to initialState onClick
                      <div
                        id="jest-div-2"
                        onClick={() => {
                          this.setState({
                            searchInput2: ""
                          });
                          this.props.getStockDetails2(mapped_search.ticker_id);
                          this.props.getFinancialReports2(
                            mapped_search.ticker_id
                          );
                        }}
                      >
                        <div className="search-list-item">
                          {/* The Ticker Names , Company, Sectors and Industies shown according to SearchInput */}
                          <div>
                            <img
                              src={
                                mapped_search.ticker_logo
                                  ? "data:image/jpeg;base64," +
                                    mapped_search.ticker_logo
                                  : stockBazaarLogo
                              }
                              alt="Ticker Logo"
                              className="brand-image-2"
                            />
                          </div>
                          <div
                            className="search-suggestion"
                            id={"ticker-search" + mapped_search.ticker_id}
                          >
                            <div
                              id="jest-search-list-item-2"
                              style={{ marginBottom: "5px" }}
                            >
                              {mapped_search.ticker_name}
                            </div>
                            <div style={{ marginBottom: "5px" }}>
                              {mapped_search.company_name}
                            </div>
                            <div display="block">
                              {mapped_search.industry == null
                                ? "NA"
                                : mapped_search.industry}{" "}
                              , {mapped_search.sector}
                            </div>
                          </div>
                        </div>
                      </div>
                    ))
                  ) : (
                    // Handling done if user searches something irrelevant
                    <div>
                      <p>NOTHING MATCHED YOUR CRITERIA</p>
                    </div>
                  )}
                </div>
              </>
            ) : (
              <div>
                {this.props.stockdetails2.map(stockdetails => (
                  <div className="profile-1">
                    <div className="firstprofilecolumn">
                      <img
                        className="firstprofilecolumn-logo"
                        src={
                          stockdetails.ticker_logo
                            ? "data:image/jpeg;base64," +
                              stockdetails.ticker_logo
                            : stockBazaarLogo
                        }
                      />
                      <p className="firstprofilecolumn-price">
                        <b>{stockdetails.last_price} USD</b>
                      </p>
                      <p className="firstprofilecolumn-lastprice">
                        LAST PRICE (
                        {new Date(stockdetails.last_date).toLocaleDateString(
                          "en-IN",
                          {
                            month: "short",
                            day: "2-digit",
                            year: "numeric"
                          }
                        )}
                        )
                      </p>
                    </div>
                    <div className="secondprofilecolumn">
                      {stockdetails.company_name ? (
                        <p
                          className="secondprofilecolumn-name"
                          id="positivecompany_name"
                        >
                          {stockdetails.company_name}
                        </p>
                      ) : (
                        <p
                          className="secondprofilecolumn-name"
                          id="positivecompanyname"
                        >
                          NA
                        </p>
                      )}
                      <p className="secondprofilecolumn-ticker">
                        {stockdetails.ticker_name}
                      </p>
                      <p className="secondprofilecolumn-change">
                        {stockdetails.change} ({stockdetails.changePercentage}%)
                      </p>
                    </div>
                  </div>
                ))}
                <br></br>
                <div className="ratios-1" id="ratios-2">
                  <p className="ratios-heading-1">Ratios</p>
                  {this.props.financialdetails2.hasOwnProperty("ratiosarray")
                    ? this.props.financialdetails2.ratiosarray.map(
                        financialdetails => (
                          <div className="ratios-flex-1">
                            <div className="first-ratio-1">
                              <p id="jest-eps">{financialdetails["EPS"]}</p>
                              <p>EPS</p>
                            </div>
                            <div className="second-ratio-1">
                              <p>
                                {Number(financialdetails["P/E"]).toFixed(2)}
                              </p>
                              <p>P/E</p>
                            </div>
                            <div className="third-ratio-1">
                              <p>{financialdetails["Debt to Assets Ratio"]}</p>
                              <p>Debt to Assets Ratio</p>
                            </div>
                            <div className="fourth-ratio-1">
                              <p>
                                {
                                  financialdetails[
                                    "Liabilities to Equity Ratio"
                                  ]
                                }
                              </p>
                              <p>Liabilities to Equity Ratio</p>
                            </div>
                            <div className="fifth-ratio-1">
                              <p>{financialdetails["EV / EBITDA"]}</p>
                              <p>EV/EBITDA</p>
                            </div>
                            <div className="sixth-ratio-1">
                              <p>{financialdetails["Return on Equity"]}</p>
                              <p>Return on Equity</p>
                            </div>
                          </div>
                        )
                      )
                    : console.log(null)}
                </div>
                <br></br>
                {!String(this.props.stockdetails2.profile).length == 0 && (
                  <div className="description-1" id="description-1">
                    <Accordion>
                      {this.props.stockdetails2.map(stockdetails => (
                        <AccordionItem
                          title="Company Profile :"
                          className="description-heading-1"
                        >
                          <div className="description-input-1">
                            <p>{stockdetails.profile}</p>
                          </div>
                        </AccordionItem>
                      ))}
                    </Accordion>
                  </div>
                )}
                <br></br>
                <div className="statements-1" id="pandl-2">
                  <p className="statements-heading-1">Profit & Loss</p>
                  <StockSpecificSection
                    headers={[
                      "date",
                      "Revenues",
                      "EBITDA",
                      "Cash   From Operating Activities",
                      "Net Profit",
                      "Gross Profit"
                    ]}
                    reportdata={reports2}
                  />
                </div>
                <br></br>
                <div className="bsstatements-1" id="balancesheet-2">
                  <p className="bsstatements-heading-1">Balance Sheet</p>
                  <StockSpecificSection
                    headers={[
                      "date",
                      "Cash, Cash Equivalents & Short Term Investments",
                      "Receivables",
                      "Other Short Term Assets",
                      "Current Assets",
                      "Net PP&E",
                      "Long Term Investments & Receivables",
                      "Total Noncurrent Assets",
                      "Total Assets",
                      "Short term debt",
                      "Current Liabilities",
                      "Long Term Debt",
                      "Total Noncurrent Liabilities",
                      "Total Liabilities",
                      "Retained Earnings",
                      "Equity Before Minorities",
                      "Total Equity"
                    ]}
                    reportdata={reports2}
                  />
                </div>
                <br></br>
                <div className="cfstatements-1" id="cashflow-2">
                  <p className="cfstatements-heading-1">Cash Flow</p>
                  <StockSpecificSection
                    headers={[
                      "date",
                      "Depreciation & Amortisation",
                      "Change in Working Capital",
                      "Cash From Operating Activities",
                      "Net Change in PP&E & Intangibles",
                      "Cash From Investing Activities",
                      "Dividends",
                      "Cash From Financing Activities",
                      "Net Change in Cash"
                    ]}
                    reportdata={reports2}
                  />
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  search: state.searchReducer.search,
  stockdetails: state.stockSpecificReducer.stockdetails,
  stockdetails2: state.stockSpecificReducer.stockdetails2,
  financialdetails: state.stockSpecificReducer.financialdetails,
  financialdetails2: state.stockSpecificReducer.financialdetails2,
  compare: state.compareReducer.compare
});

export default connect(mapStateToProps, {
  allSearch,
  getStockDetails,
  getStockDetails2,
  getFinancialReports,
  getFinancialReports2,
  allSectorSearch
})(Compare);
