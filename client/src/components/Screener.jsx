import React, { Component } from "react";
import "../assets/style/Screener.css";
import { connect } from "react-redux";
import {
  getAllIndustriesBySector,
  getTickersByFilter
} from "../actions/Screener";
import FilterModal from "./FilterModal";
import SelectOptions from "./SelectOptions";
import FilteredTickerCard from "./common/FilteredTickerCard";

export class Screener extends Component {
  state = {
    showModal: window.innerWidth >= 768 ? true : false,
    title: "",
    step: 1,
    searchFilterInput: "",
    industriesBySector: "",
    filteredTickers: [],
    dataPoints: [49, 45, 80, 48, 40]
  };

  // Calls the getIndustriesBySector method to get all the industries by sector
  componentDidMount = () => {
    //checks whether a token is present and redirects the user to home page if (s)he's already logged in
    if (!localStorage.getItem("token")) {
      this.props.history.push("/login");
    }
    this.props.getAllIndustriesBySector();
    this.props.getTickersByFilter({
      sectors: ["Technology"],
      industries: [],
      evebitda: this.state.dataPoints[0],
      growth: this.state.dataPoints[1],
      mktcap: this.state.dataPoints[2],
      profit: this.state.dataPoints[3],
      assets: this.state.dataPoints[4]
    });
  };

  // closes the modal when close/cross icon is clicked
  handleCloseButton = () => {
    this.setState({ showModal: false, step: 1 });
  };

  // opens the modal when filter FAB is clicked
  handleFilterFAB = () => {
    this.setState({ showModal: true });
  };

  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.industriesBySector !== this.state.industriesBySector) {
      this.setState({
        industriesBySector: nextProps.industriesBySector
      });
      return true;
    }
    if (nextProps.filteredTickers !== this.state.filteredTickers) {
      this.setState({
        filteredTickers: nextProps.filteredTickers
      });
      return true;
    }
    return true;
  }
  // handle the next component to be called when a specific filter is selected
  // at step one
  handleSelectOption = title => {
    this.setState({
      title,
      step: this.state.step + 1
    });
  };

  // goes back to previous component when left arrow or back button is called
  handleBackButton = () => {
    this.setState({ step: this.state.step - 1, searchFilterInput: "" });
  };

  handleCheckboxChange = e => {
    if (this.state.title == "Sector") {
      let sectorsData = this.state.industriesBySector;
      sectorsData.forEach(sectors => {
        if (sectors.sector.name === e.target.value) {
          sectors.sector.isChecked = e.target.checked;
        }
      });
      this.setState({ industriesBySector: sectorsData });
    } else if (this.state.title == "Industry") {
      let industriesData = this.state.industriesBySector;
      industriesData.forEach(industryArrData => {
        industryArrData.industries.forEach(industry => {
          if (industry.name === e.target.value) {
            industry.isChecked = e.target.checked;
          }
        });
      });
      this.setState({ industriesBySector: industriesData });
    }
  };

  // handles the search input and updates the state when somthing is type in
  // input field and calls the search function
  handleSearchInputChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleApplyClick = () => {
    let allSectorsData = [];
    let allIndustriesData = [];
    let sectorsData = this.state.industriesBySector;
    for (let i = 0; i < sectorsData.length; i++) {
      if (sectorsData[i].sector.isChecked === true) {
        allSectorsData.push(sectorsData[i].sector.name);
      }
      for (let j = 0; j < sectorsData[i].industries; j++) {
        if (sectorsData[i].industries[j].isChecked === true) {
          allIndustriesData.push(sectorsData[i].industries[j].name);
        }
      }
    }
    this.props.getTickersByFilter({
      sectors: allSectorsData,
      industries: allIndustriesData,
      evebitda: this.state.dataPoints[0],
      growth: this.state.dataPoints[1],
      mktcap: this.state.dataPoints[2],
      profit: this.state.dataPoints[3],
      assets: this.state.dataPoints[4]
    });
  };

  handleOnDragEnd = dataPointsArray => {
    this.setState({ dataPoints: dataPointsArray });
  };

  render() {
    return (
      <div id="screener-page" className="nav-margin">
        <div className="filtered-tickers-container">
          {this.state.filteredTickers.length} companies found
          <div className="filtered-tickers-grid">
            {this.state.filteredTickers.map(ticker => (
              <FilteredTickerCard tickerData={ticker} />
            ))}
          </div>
        </div>
        <div
          className="filter-container"
          style={{ display: this.state.showModal ? "flex" : "none" }}
        >
          <div className="filter-modal">
            {/* if it is 1st step then calls the filter modal */}
            {this.state.step == 1 && (
              <FilterModal
                handleCloseButton={this.handleCloseButton}
                handleSelectOption={title => this.handleSelectOption(title)}
                handleOnDragEnd={dataPointsArray =>
                  this.handleOnDragEnd(dataPointsArray)
                }
                dataPoints={this.state.dataPoints}
              />
            )}
            {/* calls the select options when either of two options is clicked 
                in first step */}
            {this.state.step == 2 && (
              <SelectOptions
                title={this.state.title}
                searchFilterInput={this.state.searchFilterInput}
                handleBackButton={this.handleBackButton}
                handleCloseButton={this.handleCloseButton}
                handleSearchInputChange={this.handleSearchInputChange}
                handleCheckboxChange={this.handleCheckboxChange}
                industriesBySector={this.state.industriesBySector}
              />
            )}
            <div
              id="apply-button"
              class="apply-button"
              onClick={this.handleApplyClick}
            >
              Apply
            </div>
          </div>
        </div>
        <div id="filter-fab" onClick={this.handleFilterFAB}>
          <span></span>
        </div>
      </div>
    );
  }
}

// access the store properties of screenReducer
const mapStateToProps = state => ({
  industriesBySector: state.screenerReducer.industriesBySector,
  filteredTickers: state.screenerReducer.filteredTickers
});

// connects the component to store
export default connect(mapStateToProps, {
  getAllIndustriesBySector,
  getTickersByFilter
})(Screener);
