import React from "react";
import ChartComponent from "./ChartComponent";

//  first step of filter which is called when Filter FAB is clicked.
//  It has two options Sector and Industry clicking on either of them calls the
//  selectoptions by passing respective props.
export default function FilterModal(props) {
  return (
    <>
      {/* filter header contains filter modal title and close button */}
      <div className="filter-header">
        <p id="filter-title">Filter</p>
        <div
          className="close-icon"
          id="filter-close-icon"
          onClick={props.handleCloseButton}
        >
          <span></span>
        </div>
      </div>
      <hr />
      {/* contains two buttons Sector and Industry */}
      <div className="filter-items">
        <div
          id="sector-select"
          className="selectable-button"
          onClick={() => props.handleSelectOption("Sector")}
        >
          <span id="sector-title">Sector</span>
          <span className="arrow right"></span>
        </div>
        <div
          id="industry-select"
          className="selectable-button"
          onClick={() => props.handleSelectOption("Industry")}
        >
          <span id="industry-title">Industry</span>
          <span className="arrow right"></span>
        </div>
      </div>
      <ChartComponent
        handleOnDragEnd={dataPointsArray => {
          props.handleOnDragEnd(dataPointsArray);
        }}
        dataPoints={props.dataPoints}
      />
    </>
  );
}
