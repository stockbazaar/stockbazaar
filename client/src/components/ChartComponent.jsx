import React, { Component } from "react";
import { Chart, Radar } from "react-chartjs-2";
import "chartjs-plugin-dragdata";
Chart.defaults.global.legend.display = false;

export default class ChartComponent extends Component {
  state = {
    data: {
      type: "radar",
      labels: ["EV/EBITDA", "Growth", "MKT CAP", "Net Profit", "Assets"],
      datasets: [
        {
          label: "# of Highlights",
          data: this.props.dataPoints,
          borderWidth: 2,
          borderJoinStyle: "round",
          pointRadius: 2,
          pointHoverRadius: 6,
          pointHoverBackgroundColor: "#ffffff",
          pointHoverBorderWidth: 3
        }
      ]
    }
  };
  handleOnDragEnd = (index, value) => {
    this.props.handleOnDragEnd(this.state.data.datasets[0].data);
  };

  // updates the data in the state on dragging the points of radar
  handleOnDrag = (e, datasetIndex, index, value) => {
    let data = this.state.data;
    data.datasets[0].data[index] = value;
    this.setState({ data });
  };

  // returns gradient color to the data
  setGradientColor = (canvas, hslValue) => {
    const ctx = canvas.getContext("2d");
    const gradient = ctx.createLinearGradient(100, 200, 3500, 100);
    gradient.addColorStop(0, hslValue);
    gradient.addColorStop(0.9, "rgba(10,10,10,0.3)");
    return gradient;
  };

  // this function updates the background color, border color and border color
  // of point while hovering on it, the color changes when points are dragged.
  getChart = canvas => {
    let data = this.state.data;
    let sum = 0;
    for (let i = 0; i < data.datasets[0].data.length; i++) {
      sum += data.datasets[0].data[i];
    }
    // calculates the average and get the color range for hsl value from 0 to 155
    sum = ((sum / 5) * 155) / 100;
    if (sum <= 31) {
      sum = 0;
    } else {
      sum -= 31;
    }
    let hslValue = "hsl(" + sum + ",87%,57%,0.9)";
    if (data.datasets) {
      data.datasets[0].backgroundColor = this.setGradientColor(
        canvas,
        hslValue
      );
      data.datasets[0].borderColor = "hsl(" + sum + ",87%,57%,1)";
      data.datasets[0].pointHoverBorderColor = "hsl(" + sum + ",87%,57%,1)";
    }
    return data;
  };
  render() {
    return (
      <div className="radar-canvas">
        <Radar
          data={this.getChart}
          options={{
            responsive: true,
            dragData: true,
            dragDataRound: 0,
            onDragStart: function(e) {},
            onDrag: (e, datasetIndex, index, value) =>
              this.handleOnDrag(e, datasetIndex, index, value),
            onDragEnd: (e, datasetIndex, index, value) =>
              this.handleOnDragEnd(index, value),
            scale: {
              pointLabels: {
                fontColor: "#ffffff",
                fontFamily: "'Work Sans',sans-serif",
                fontSize: "12"
              },
              angleLines: {
                display: true,
                color: "#4b527b"
              },
              gridLines: {
                color: "#4b527b"
              },
              ticks: {
                display: false,
                max: 100,
                min: 0,
                stepSize: 20
              }
            },
            tooltips: {
              mode: "index"
            },
            hover: {
              mode: "index"
            }
          }}
        />
      </div>
    );
  }
}
