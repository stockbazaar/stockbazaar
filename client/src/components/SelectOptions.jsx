import React from "react";

// Makes a checkbox used in SelectOptions
const Checkbox = ({ checkboxData, handleCheckboxChange }) => {
  return (
    <li className="checkbox-li">
      <input
        id={
          "checkbox-" +
          checkboxData.name.toLowerCase().replace(/[^a-zA-Z0-9]/g, "-")
        }
        value={checkboxData.name}
        type="checkbox"
        className="checkbox"
        checked={checkboxData.isChecked}
        onClick={handleCheckboxChange}
      />
      {checkboxData.name}
    </li>
  );
};

// select option is called when either of two option is clicked it maps the
// sectors or industries list and providing a search for the same depending on
// option selected.
export default function SelectOptions(props) {
  return (
    <>
      {/* filter header contains filter modal title, back button and close 
              button */}
      <div className="filter-header">
        <div>
          <span
            id="filter-back-icon"
            className="arrow left"
            onClick={props.handleBackButton}
          ></span>
          <p id="filter-title">{props.title}</p>
        </div>
        <div
          id="filter-close-icon"
          className="close-icon"
          onClick={props.handleCloseButton}
        >
          <span></span>
        </div>
      </div>
      <hr />
      {/* filter items contains list of sectors/industries with checkbox and
              search to search for the same */}
      <div className="filter-items">
        {props.title === "Industry" && (
          <input
            id="search-filter-input"
            name="searchFilterInput"
            className="search-box"
            value={props.searchFilterInput}
            onChange={props.handleSearchInputChange}
            autoComplete="off"
            placeholder={"Search for " + props.title}
            autofocus
          />
        )}
        <div className="filter-items-list">
          {props.title == "Sector" &&
            props.industriesBySector.map(sectors => (
              <Checkbox
                checkboxData={sectors.sector}
                handleCheckboxChange={props.handleCheckboxChange}
              />
            ))}
          {props.title == "Industry" &&
            props.industriesBySector.map(
              industriesData =>
                industriesData.sector.isChecked &&
                industriesData.industries.map(industryData => (
                  <Checkbox
                    checkboxData={industryData}
                    handleCheckboxChange={props.handleCheckboxChange}
                  />
                ))
            )}
        </div>
      </div>
    </>
  );
}
