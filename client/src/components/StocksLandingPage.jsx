import React, { Component } from "react";
import { connect } from "react-redux";
import "../assets/style/stockslanding.css";
import ReusableSection from "./common/ReusableSection";
import classnames from "classnames";
import TickerTables from "./common/TickersTables";
import Loader from "react-loader-spinner";
import {
  // importing all the actions required
  getAllIndices,
  getIndexOHLC,
  getOverview,
  getPerformance,
  getGainersMargin,
  getGainersValuation,
  getLosersMargin,
  getLosersValuation,
  getSectors,
  getIndustries,
  getSectorsPerformance,
  getIndustryPerformance
} from "../actions/StocksLanding";
class StocksLandingPage extends Component {
  componentDidMount() {
    // ohlc graph , the market indices, and overview of gainers, losers, sectors and industries has to be mounted
    this.props.getIndexOHLC("RUT");
    this.props.getAllIndices();
    this.props.getOverview();
    this.props.getSectors();
    this.props.getIndustries();
  }
  // ontoggle is used to switch between the tabs provided on the screen.
  // These tabs scroll u through the page
  ontoggle = tab => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  };

  state = {
    activeTab: "1",
    loserState: "",
    colorTab: "RUT",
    gainersState: "",
    gainersTab: "Overview",
    losersTab: "Overview",
    sectorsTab: "Overview",
    industryTab: "Overview",

    // since their are multiple tabs and each tab when clicked displays a table, its' table headings need to be changed on respective click
    // since the overview is first displayed, the headings are stated for them
    gainersHeading: {
      tickerName: "Ticker",
      last: "Last Price($)",
      marketCap: "Market Cap",
      change: "CHG",
      changePercentage: "CHG%"
    },
    losersHeadings: {
      tickerName: "Ticker",
      last: "Last Price($)",
      marketCap: "Market Cap",
      change: "CHG",
      changePercentage: "CHG%"
    },
    sectorsHeadings: {
      tickerName: "Sector",
      industry_count: "Industry Count",
      marketCap: "Market Cap",
      change: "CHG",
      changePercentage: "CHG%"
    },
    industryHeadings: {
      tickerName: "Industry",
      marketCap: "Market Cap",
      change: "CHG",
      changePercentage: "CHG%",
      sectorName: "Sector"
    }
    // };
  };
  // handleOnClickBehaviour is used to change the colour of the active tab
  handleOnClickBehaviour = funcParam => {
    this.setState({ colorTab: funcParam });
    console.log(this.state.colorTab, funcParam);
    this.props.getIndexOHLC(funcParam);
  };

  // when clicked on a tab, its respective table should be mapped from the reducer.
  // so when the performance tab is clicked, performance table will be mapped and so on
  componentWillReceiveProps(nextProps, prevState) {
    if (this.state.gainersTab === "Overview") {
      this.setState({ gainersState: nextProps.goverview });
    } else if (this.state.gainersTab === "Performance") {
      this.setState({ gainersState: nextProps.gperformance });
    } else if (this.state.gainersTab === "Valuation") {
      this.setState({ gainersState: nextProps.gvalue });
    } else if (this.state.gainersTab === "Margin") {
      this.setState({ gainersState: nextProps.gmargin });
    }
    if (this.state.losersTab === "Overview") {
      this.setState({ loserState: nextProps.loverview });
    } else if (this.state.losersTab === "Performance") {
      this.setState({ loserState: nextProps.lperformance });
    } else if (this.state.losersTab === "Valuation") {
      this.setState({ loserState: nextProps.lvalue });
    } else if (this.state.losersTab === "Margin") {
      this.setState({ loserState: nextProps.lmargin });
    }
    if (this.state.sectorsTab === "Overview") {
      this.setState({ sectorsState: nextProps.sectors });
    } else if (this.state.sectorsTab === "Performance") {
      this.setState({ sectorsState: nextProps.sperformance });
    }
    if (this.state.industryTab === "Overview") {
      this.setState({ industryState: nextProps.industries });
    } else if (this.state.industryTab === "Performance") {
      this.setState({ industryState: nextProps.iperformance });
    }
  }

  // in gainers' section when a tab is clicked, specific api call should be made to display the results
  // so if the overview tab is clicked, getOverview action is called which then makes the api call
  // also we need to pass the table headings/ column names for that table
  onClickGainersTab = tabLinkData => {
    this.setState({ gainersTab: tabLinkData });
    if (tabLinkData === "Overview") {
      this.props.getOverview();
      this.setState({
        gainersHeading: {
          tickerName: "Ticker",
          last: "Last Price($)",
          marketCap: "Market Cap",
          change: "CHG",
          changePercentage: "CHG%"
        }
      });
    } else if (tabLinkData === "Performance") {
      this.props.getPerformance();
      this.setState({
        gainersHeading: {
          tickerName: "Ticker",
          changePercentage: "CHG%",
          quaterChangePercentage: "3 Months(%)",
          halfYearlyPerfChangePercentage: "6 Months(%)",
          yearlyPerfChangePercentage: "1 Y(%)"
        }
      });
    } else if (tabLinkData === "Valuation") {
      this.props.getGainersValuation();
      this.setState({
        gainersHeading: {
          tickerName: "Ticker",
          sharePrice: "Share Price($)",
          changePercentage: "Market Cap (M)",
          totalAssets: "Total Assets",
          pToE: "P/E",
          eps: "EPS",
          ev: "EV",
          evByEbitda: "EV / EBITDA"
        }
      });
    } else if (tabLinkData === "Margin") {
      this.props.getGainersMargin();
      this.setState({
        gainersHeading: {
          tickerName: "Ticker",
          grossMargin: "Gross Margin",
          operatingMargin: "Operating Margin",
          changePercentage: "Net Margin"
        }
      });
    }
  };
  onClickLosersTab = tabLinkData => {
    this.setState({ losersTab: tabLinkData });
    if (tabLinkData === "Overview") {
      this.props.getOverview();
      this.setState({
        losersHeadings: {
          tickerName: "Ticker",
          last: "Last Price($)",
          marketCap: "Market Cap",
          change: "CHG",
          changePercentage: "CHG%"
        }
      });
    } else if (tabLinkData === "Performance") {
      this.props.getPerformance();
      this.setState({
        losersHeadings: {
          tickerName: "Ticker",
          changePercentage: "CHG%",
          quaterChangePercentage: "3 Months(%)",
          halfYearlyPerfChangePercentage: "6 Months(%)",
          yearlyPerfChangePercentage: "1 Y(%)"
        }
      });
    } else if (tabLinkData === "Valuation") {
      this.props.getLosersValuation();
      this.setState({
        losersHeadings: {
          tickerName: "Ticker",
          sharePrice: "Share Price($)",
          changePercentage: "Market Cap (M)",
          totalAssets: "Total Assets",
          pToE: "P/E",
          eps: "EPS",
          ev: "EV",
          evByEbitda: "EV / EBITDA"
        }
      });
    } else if (tabLinkData === "Margin") {
      this.props.getLosersMargin();
      this.setState({
        losersHeadings: {
          tickerName: "Ticker",
          grossMargin: "Gross Margin",
          operatingMargin: "Operating Margin",
          changePercentage: "Net Margin"
        }
      });
    }
  };

  onClickSectorsTab = tabLinkData => {
    this.setState({ sectorsTab: tabLinkData });
    if (tabLinkData === "Overview") {
      this.props.getSectors();
      this.setState({
        sectorsHeadings: {
          tickerName: "Sector",
          industry_count: "Industry Count",
          marketCap: "Market Cap",
          change: "CHG",
          changePercentage: "CHG%"
        }
      });
    } else if (tabLinkData === "Performance") {
      this.props.getSectorsPerformance();

      this.setState({
        sectorsHeadings: {
          tickerName: "Sector",
          changePercentage: "CHG%",
          total_quarter_percent: "3 Months(%)",
          total_halfYear_percent: "6 Months(%)",
          total_Year_percent: "1 Y(%)"
        }
      });
    }
  };
  onClickIndustryTab = tabLinkData => {
    this.setState({ industryTab: tabLinkData });
    if (tabLinkData === "Overview") {
      this.props.getIndustries();
      this.setState({
        industryHeadings: {
          tickerName: "Industry",
          marketCap: "Market Cap",
          change: "CHG",
          changePercentage: "CHG%",
          sectorName: "Sector"
        }
      });
    } else if (tabLinkData === "Performance") {
      this.props.getIndustryPerformance();
      this.setState({
        industryHeadings: {
          tickerName: "Industry",
          changePercentage: "CHG%",
          total_quarter_percent: "3 Months(%)",
          total_halfYear_percent: "6 Months(%)",
          total_Year_percent: "1 Y(%)"
        }
      });
    }
  };

  render() {
    return (
      <div>
        {/* {this.props.indices.length > 0 ? ( */}
        <>
          <div className="secNavItems">
            {/* this div is for the secondary navbar. The sec navbar has 5 links to their respective sections. */}
            <a
              href="#marketIndices"
              className={classnames({ active: this.state.activeTab === "1" })}
              onClick={() => {
                this.ontoggle("1");
              }}
            >
              Market
              {/* Market takes you to the first section of the page that displays all the market indices */}
            </a>
            <a
              href="#gainers"
              className={classnames({ active: this.state.activeTab === "2" })}
              onClick={() => {
                this.ontoggle("2");
              }}
            >
              Gainers
            </a>
            <a
              href="#losers"
              className={classnames({ active: this.state.activeTab === "3" })}
              onClick={() => {
                this.ontoggle("3");
              }}
            >
              Losers
            </a>
            <a
              href="#sectors"
              className={classnames({ active: this.state.activeTab === "4" })}
              onClick={() => {
                this.ontoggle("4");
              }}
            >
              Sector
            </a>
            <a
              href="#industry"
              className={classnames({ active: this.state.activeTab === "5" })}
              onClick={() => {
                this.ontoggle("5");
              }}
            >
              Industry
            </a>
          </div>
          {/* section for displaying the market indices */}
          <section className="marketIndices" id="marketIndices">
            <h3 className="marketHeading">Market Indices</h3>
            <div className="marketContent">
              <div id="tickerTable">
                {/* importing the resusable component TickerTables */}
                {this.props.indices.length > 0 ? (
                  <TickerTables
                    //  ticker table is a reusable component in which  we are passing the id as indices-table and
                    //  in rowsData we are passing the reducer indices that consists of 6 indices namelt RUT, GSPC, OEX, NDX, RUA, DJI
                    // in the columnNames we are passing the tickerName, ots last price, change calculated betwn last 2 days, & changePercentage
                    //  since these are indices we are passing isIndex as true
                    // we are plotting an ohlc graph and we need to change the graph as we click on an index.
                    // so, we are setting onClickBehaviour as true.
                    // linesType is set to light as we have the indices table in light version
                    id="indices-table"
                    rowsData={this.props.indices}
                    columnNames={{
                      tickerName: "Index",
                      last: "Last Price($)",
                      change: "CHG",
                      changePercentage: "CHG%"
                    }}
                    isIndex={true}
                    table_id="indices-table"
                    onClickBehaviour={true}
                    linesType="light"
                  />
                ) : (
                  <Loader
                    type={Loader}
                    style={{
                      marginTop: "350px",
                      marginLeft: "50%",
                      // margin: "auto",
                      padding: "10px"
                    }}
                    color="#00ccdd"
                  />
                )}
              </div>
              {/* this part displays the stock chart or the ohlc graph */}
              <div className="stockChart">
                <iframe
                  src={this.props.indexohlc}
                  id="ohlcGraph"
                  title="OHLC Graph"
                ></iframe>
              </div>
            </div>
            {/* this div is for displaying the indices with their names, last price, change and changePercentage */}
            <div class="indices">
              {this.props.indices.map(index => (
                <a
                  style={{
                    cursor: "pointer",
                    backgroundColor:
                      this.state.colorTab === index.tickerName &&
                      "var(--primary-color)",
                    color: this.state.colorTab === index.tickerName && "#ffffff"
                  }}
                  onClick={() => {
                    this.handleOnClickBehaviour(index.tickerName);
                  }}
                >
                  <p
                    style={{
                      fontSize: 10 + "px",
                      fontWeight: 10
                    }}
                    onClick={() => {
                      this.props.getIndexOHLC(index.tickerName);
                    }}
                  >
                    {index.tickerName}
                  </p>
                  <br />

                  <p
                    style={{
                      color:
                        //  if the value of change is negative, the value will be displayed in red else in green for positive value
                        Math.sign(index.tickerValue.change) === -1
                          ? "#F1304D"
                          : "#229A78",
                      marginTop: "-12px"
                    }}
                  >
                    <b> {index.tickerValue.change}</b>
                    <p
                      style={{
                        color:
                          //  if the value of change percentage is negative, the value will be displayed in red else in green for positive value

                          Math.sign(index.tickerValue.changePercentage) === -1
                            ? "#F1304D"
                            : "#229A78"
                      }}
                    >
                      <b> {index.tickerValue.changePercentage}</b>
                    </p>
                  </p>
                </a>
              ))}
            </div>
          </section>
          {/* this section displays the top gainers */}
          <section>
            {this.props.goverview.length > 0 ? (
              <ReusableSection
                // this is a reusable section for gainers.
                // we pass the section id, table id, title of the section and tab id as overview/performance/valuation/margin
                section_id="gainers"
                table_id="gainers-table"
                title="Gainers"
                tab_id="overview"
                sectionData={[
                  {
                    tabName: "Overview",
                    tabFunction: this.onClickGainersTab,
                    // after clicking on the tab, onClickGainersTab function will be called which will do the following actions mentioned
                    className:
                      this.state.gainersTab === "Overview" ? "tabColor" : null
                  },
                  {
                    tabName: "Performance",
                    tabFunction: this.onClickGainersTab,
                    // after clicking on the tab, onClickGainersTab function will be called which will do the following actions mentioned
                    className:
                      this.state.gainersTab === "Performance"
                        ? "tabColor"
                        : null
                  },
                  // },
                  {
                    tabName: "Valuation",
                    tabFunction: this.onClickGainersTab,
                    // after clicking on the tab, onClickGainersTab function will be called which will do the following actions mentioned
                    className:
                      this.state.gainersTab === "Valuation" ? "tabColor" : null
                  },
                  {
                    tabName: "Margin",
                    tabFunction: this.onClickGainersTab,
                    // after clicking on the tab, onClickGainersTab function will be called which will do the following actions mentioned
                    className:
                      this.state.gainersTab === "Margin" ? "tabColor" : null
                  }
                ]}
                tableHeadings={this.state.gainersHeading}
                tableData={this.state.gainersState}
                linesType="dark"
                isIndex={false}
                onClickBehaviour={true}
              />
            ) : (
              <Loader
                type={Loader}
                style={{
                  marginTop: "350px",
                  marginLeft: "50%",
                  padding: "10px"
                }}
                color="#00ccdd"
              />
            )}
          </section>
          {/* this section displays the top losers */}
          <section>
            {this.props.loverview.length > 0 ? (
              <ReusableSection
                section_id="losers"
                table_id="losers-table"
                title="Losers"
                tab_id="overview"
                sectionData={[
                  {
                    tabName: "Overview",
                    tabFunction: this.onClickLosersTab,
                    className:
                      this.state.losersTab === "Overview" ? "tabColor" : null
                  },
                  {
                    tabName: "Performance",
                    tabFunction: this.onClickLosersTab,
                    className:
                      this.state.losersTab === "Performance" ? "tabColor" : null
                  },
                  {
                    tabName: "Valuation",
                    tabFunction: this.onClickLosersTab,
                    className:
                      this.state.losersTab === "Valuation" ? "tabColor" : null
                  },
                  {
                    tabName: "Margin",
                    tabFunction: this.onClickLosersTab,
                    className:
                      this.state.losersTab === "Margin" ? "tabColor" : null
                  }
                ]}
                // sectionFunction={this.props.getGainersOverview}
                tableHeadings={this.state.losersHeadings}
                tableData={this.state.loserState}
                linesType="dark"
                isIndex={false}
                onClickBehaviour={true}
              />
            ) : (
              <Loader
                type={Loader}
                style={{
                  marginTop: "350px",
                  marginLeft: "50%",
                  // margin: "auto 350px auto 0px",
                  padding: "10px"
                }}
                color="#00ccdd"
              />
            )}
          </section>
          {/* this section displays all the sectors */}
          <section>
            {this.props.sectors.length > 0 ? (
              <ReusableSection
                section_id="sectors"
                table_id="sectors-table"
                title="Sectors"
                tab_id="overview"
                sectionData={[
                  {
                    tabName: "Overview",
                    tabFunction: this.onClickSectorsTab,
                    className:
                      this.state.sectorsTab === "Overview" ? "tabColor" : null
                  },
                  {
                    tabName: "Performance",
                    tabFunction: this.onClickSectorsTab,
                    className:
                      this.state.sectorsTab === "Performance"
                        ? "tabColor"
                        : null
                  }
                ]}
                // sectionFunction={this.props.getGainersOverview}
                tableHeadings={this.state.sectorsHeadings}
                tableData={this.state.sectorsState}
                linesType="dark"
                isIndex={false}
                onClickBehaviour={false}
              />
            ) : (
              <Loader
                type={Loader}
                style={{
                  marginTop: "350px",
                  marginLeft: "50%",
                  padding: "10px"
                }}
                color="#00ccdd"
              />
            )}
          </section>
          {/* this section displays all the industries */}
          <section>
            {this.props.industries.length > 0 ? (
              <ReusableSection
                section_id="industry"
                table_id="industry-table"
                title="Industries"
                tab_id="overview"
                sectionData={[
                  {
                    tabName: "Overview",
                    tabFunction: this.onClickIndustryTab,
                    className:
                      this.state.industryTab === "Overview" ? "tabColor" : null
                  },
                  {
                    tabName: "Performance",
                    tabFunction: this.onClickIndustryTab,
                    className:
                      this.state.industryTab === "Performance"
                        ? "tabColor"
                        : null
                  }
                ]}
                tableHeadings={this.state.industryHeadings}
                tableData={this.state.industryState}
                linesType="dark"
                isIndex={false}
                onClickBehaviour={false}
              />
            ) : (
              <Loader
                type={Loader}
                style={{
                  marginTop: "350px",
                  marginLeft: "50%",
                  // margin: "auto",
                  padding: "10px"
                }}
                color="#00ccdd"
              />
            )}
          </section>
        </>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  goverview: state.stocksLandingReducer.goverview,
  loverview: state.stocksLandingReducer.loverview,
  gperformance: state.stocksLandingReducer.gperformance,
  lperformance: state.stocksLandingReducer.lperformance,
  indexohlc: state.stocksLandingReducer.indexohlc,
  indices: state.stocksLandingReducer.indices,
  gmargin: state.stocksLandingReducer.gmargin,
  gvalue: state.stocksLandingReducer.gvalue,
  lvalue: state.stocksLandingReducer.lvalue,
  lmargin: state.stocksLandingReducer.lmargin,
  sectors: state.stocksLandingReducer.sectors,
  industries: state.stocksLandingReducer.industries,
  sperformance: state.stocksLandingReducer.sperformance,
  iperformance: state.stocksLandingReducer.iperformance
});

export default connect(mapStateToProps, {
  getAllIndices,
  getIndexOHLC,
  getOverview,
  getPerformance,
  getGainersValuation,
  getLosersValuation,
  getLosersValuation,
  getGainersMargin,
  getLosersMargin,
  getSectors,
  getIndustries,
  getSectorsPerformance,
  getIndustryPerformance
})(StocksLandingPage);
