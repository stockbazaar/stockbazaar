import React from "react";
import { shallow, mount } from "enzyme";
import { Screener } from "../Screener";

const industriesBySector = [
  {
    sector: {
      name: "Consumer Cyclical",
      isChecked: false
    },
    industries: [
      {
        name: "Entertainment",
        isChecked: false
      },
      {
        name: "Homebuilding & Construction",
        isChecked: false
      }
    ]
  }
];

const filteredTickers = [
  {
    _id: "5dc940fc03db8b61309e2eeb",
    sector: "Technology",
    industry: "Computer Hardware",
    ticker_data: {
      ticker_name: "AAPL",
      ticker_id: 9,
      secondLast: {
        "Share Price": 199.74,
        "Market Capitalisation": 919018.7205,
        date: "2019-06-27T00:00:00.000Z"
      },
      last: {
        "Share Price": 201.55,
        "Market Capitalisation": 927346.6663,
        date: "2019-07-01T00:00:00.000Z"
      },
      change: 1.81,
      changePercentage: 0.9
    }
  }
];

// Created: Ankit, 16-11-2019 06:03pm
// Modified: Ankit, 16-11-2019 06:03pm
const wrapper = mount(
  <Screener
    industriesBySector={industriesBySector}
    filteredTickers={filteredTickers}
  />
);

describe("testing screener component", () => {
  it("It should match snapshot", () => {
    expect(wrapper).toMatchSnapshot();
  });

  it("checks for the presence of buttons and text", () => {});
});
