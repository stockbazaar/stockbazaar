import React from "react";
import { shallow } from "enzyme";
import { StockSpecific } from "../StockSpecific";
import stockBazaarLogo from "../../assets/images/stock-bazaar-primary.png";

const match = { params: { ticker_id: 1 } };
const getStockDetails = jest.fn();
const getStockChart = jest.fn();
const getMonteCarloGraph = jest.fn();
const getFinancialReports = jest.fn();
const getEarningsVsRevenue = jest.fn();
const getProfitLoss = jest.fn();

const stockdetails = [
  {
    last_date: "2019-07-01",
    last_price: 201,
    change: 1.81,
    changePercentage: 0.9,
    ticker_id: 9,
    ticker_name: "AAPL",
    company_name: "Apple Inc.",
    profile: "Apple Pay",
    ticker_logo: "PHN2Zy"
  }
];

const financialdetails = {
  datesCF: [
    {
      "Abnormal Gains/Losses": 0,
      "Accounts Payable": 30443,
      "Avg Basic Shares Outstanding": 4674071,
      "Avg Diluted Shares Outstanding": 4700646,
      "Book to Market": 0.1214,
      COGS: 36194
    }
  ]
};

const wrapper = shallow(
  <StockSpecific
    match={match}
    stockdetails={stockdetails}
    financialdetails={financialdetails}
    getStockDetails={getStockDetails}
    getStockChart={getStockChart}
    getMonteCarloGraph={getMonteCarloGraph}
    getFinancialReports={getFinancialReports}
    getEarningsVsRevenue={getEarningsVsRevenue}
    getProfitLoss={getProfitLoss}
  />
);

describe("test for the text and snapshot testing", () => {
  it("render the component", () => {
    expect(wrapper).toMatchSnapshot();
  });

  it("should have text Ratios, Price, Profit & Loss, Balance Sheet, Cash Flow", () => {
    // checks for the links text
    expect(wrapper.find("#ratios").text()).toBe("Ratios");
    expect(wrapper.find("#stock-chart").text()).toBe("Stock Chart+ Comparison");
    expect(wrapper.find("#pandl").text()).toBe(
      "Profit & Loss<StockSpecificSection />"
    );
    expect(wrapper.find("#balancesheet").text()).toBe(
      "Balance Sheet<StockSpecificSection />"
    );
    expect(wrapper.find("#cashflow").text()).toBe(
      "Cash Flow<StockSpecificSection />"
    );
  });
});

describe("test for functions -> updateDimensions, getStockDetails, getStockChart, getMonteCarloGraph, getFinancialReports, getEarningsVsRevenue, getProfitLoss", () => {
  // updateDimensions function to be called
  it("checks for the updateDimensions function to be called on window size change", () => {
    const spyUpdateDimensions = jest.spyOn(
      wrapper.instance(),
      "updateDimensions"
    );

    // Triggers the window resize event.
    global.addEventListener("resize", spyUpdateDimensions);
    global.dispatchEvent(new Event("resize"));
    // expects updateDimensions to be called on clicking men link
    // wrapper.find("#menu-btn").simulate("click");
    expect(spyUpdateDimensions).toBeCalled();
  });

  it("checks for the value in the setState object for updateDimensions function", () => {
    // Testing a Particular Function for State Value Change
    wrapper.setState({
      width: 425,
      isChecked: true
    });
    expect(wrapper.instance().state.width).toBe(425);
    expect(wrapper.instance().state.isChecked).toBe(true);
  });

  it("checks for the getStockDetails function to be called", () => {
    expect(getStockDetails).toHaveBeenCalledWith(1);
  });

  it("checks for the getStockChart function to be called", () => {
    expect(getMonteCarloGraph).toHaveBeenCalledWith(1);
  });

  it("If isChecked is false, then dropdown will appear thorugh more button", () => {
    wrapper.setState({
      isChecked: false
    });
    expect(wrapper.find("button").text()).toBe("More");
  });
});

describe("checks for the props data which is mapped in component", () => {
  const stockdetails1 = [
    {
      last_date: "2019-07-01",
      last_price: 201,
      change: 1.81,
      changePercentage: 0.9,
      ticker_id: 9,
      ticker_name: "AAPL",
      company_name: "",
      profile: "",
      ticker_logo: ""
    }
  ];
  const financialdetails = {
    datesCF: [
      {
        "Abnormal Gains/Losses": 0,
        "Accounts Payable": 30443,
        "Avg Basic Shares Outstanding": 4674071,
        "Avg Diluted Shares Outstanding": 4700646,
        "Book to Market": 0.1214,
        COGS: 36194
      }
    ]
  };
  const wrapper1 = shallow(
    <StockSpecific
      stockdetails={stockdetails1}
      financialdetails={financialdetails}
      match={match}
      getStockDetails={getStockDetails}
      getStockChart={getStockChart}
      getMonteCarloGraph={getMonteCarloGraph}
      getFinancialReports={getFinancialReports}
      getEarningsVsRevenue={getEarningsVsRevenue}
      getProfitLoss={getProfitLoss}
    />
  );
  it("If company name is not present in stockdetails, then text should be NA", () => {
    expect(wrapper1.find("#positivecompanyname").text()).toBe("NA");
  });

  it("If ticker_logo is not present in stockdetails, then default image will be displayed", () => {
    expect(wrapper1.find("img").prop("src")).toEqual(stockBazaarLogo);
  });
});
