import React from "react";
import { shallow } from "enzyme";
import { Register } from "../Register";

const sendOTP = jest.fn();
const verifyOTPRegister = jest.fn();
const onChange = jest.fn();
const clearReducer = jest.fn();

const wrapper = shallow(
  <Register
    sendOTP={sendOTP}
    verifyOTPRegister={verifyOTPRegister}
    onChange={onChange}
    clearReducer={clearReducer}
  />
);

describe("Register component Testing", () => {
  it("should render component", () => {
    expect(wrapper).toMatchSnapshot();
  });

  it("should check if component is mounted and the actions in the component are called", () => {
    const componentDidMount = jest.spyOn(
      Register.prototype,
      "componentDidMount"
    );

    expect(componentDidMount).toHaveBeenCalled();
    expect(clearReducer).toHaveBeenCalled();
  });
  it("should check labels text value for login field onchange and state change", () => {
    expect(wrapper.find("#register-name").length).toBe(1);
    expect(wrapper.find("#register-name").prop("placeholder")).toBe(" ");
    expect(wrapper.find("#register-name").prop("name")).toBe("name");
    expect(wrapper.find("#register-name").prop("type")).toBe("text");
    expect(wrapper.find("#register-name").prop("autoComplete")).toBe("off");
    const event = {
      target: {
        name: "name",
        value: "test"
      }
    };
    wrapper.instance().onChange(event);
    expect(wrapper.state().name).toBe(event.target.value);
    const onChange = jest.spyOn(wrapper.instance(), "onChange");
    wrapper.setState({ name: "test" });
    expect(wrapper.state().name).toBe("test");
  });
  it("should check labels text value for login field onchange and state change", () => {
    expect(wrapper.find("#register-email").length).toBe(1);
    expect(wrapper.find("#register-email").prop("placeholder")).toBe(" ");
    expect(wrapper.find("#register-email").prop("name")).toBe("email");
    expect(wrapper.find("#register-email").prop("type")).toBe("email");
    expect(wrapper.find("#register-email").prop("autoComplete")).toBe("off");
    const event = {
      target: {
        name: "email",
        value: "test"
      }
    };
    wrapper.instance().onChange(event);
    expect(wrapper.state().email).toBe(event.target.value);
    const onChange = jest.spyOn(wrapper.instance(), "onChange");
    wrapper.setState({ email: "test" });
    expect(wrapper.state().email).toBe("test");
  });
  it("should check labels text value  for password field onchange and state change", () => {
    expect(wrapper.find("#register-password").length).toBe(1);
    expect(wrapper.find("#register-password").prop("placeholder")).toBe(" ");
    expect(wrapper.find("#register-password").prop("name")).toBe("password");
    expect(wrapper.find("#register-password").prop("type")).toBe("password");
    expect(wrapper.find("#register-password").prop("autoComplete")).toBe("off");
    const event = {
      target: {
        name: "password",
        value: "test123"
      }
    };
    wrapper.instance().onChange(event);
    expect(wrapper.state().password).toBe(event.target.value);
    const onChange = jest.spyOn(wrapper.instance(), "onChange");
    wrapper.setState({ password: "test123" });
    expect(wrapper.state().password).toBe("test123");
  });
  it("should check labels text value for login field onchange and state change", () => {
    wrapper.setState({ show_otp_field: true });
    expect(wrapper.find("#register-otp").length).toBe(1);
    expect(wrapper.find("#register-otp").prop("placeholder")).toBe(" ");
    expect(wrapper.find("#register-otp").prop("name")).toBe("otp");
    expect(wrapper.find("#register-otp").prop("type")).toBe("password");
    expect(wrapper.find("#register-otp").prop("autoComplete")).toBe("off");
    const event = {
      target: {
        name: "otp",
        value: "1234"
      }
    };
    wrapper.instance().onChange(event);
    expect(wrapper.state().otp).toBe(event.target.value);
    const onChange = jest.spyOn(wrapper.instance(), "onChange");
    wrapper.setState({ otp: "1234" });
    expect(wrapper.state().otp).toBe("1234");
  });
  it("should check Login tab  & onClick event", () => {
    expect(wrapper.find("#register-tab").text()).toBe("Register");
    expect(wrapper.find("#login-tab").text()).toBe("Login");
    wrapper.find("#login-tab").simulate("click");
  });

  it("should check send otp button text & onClick event", () => {
    expect(wrapper.find("#register-send-otp-button").text()).toBe("Send OTP");
    wrapper
      .find("#register-send-otp-button")
      .simulate("click", { preventDefault: () => {} });
    let onSendOTPButtonClick = jest.spyOn(
      wrapper.instance(),
      "onSendOTPButtonClick"
    );

    expect(onSendOTPButtonClick).toHaveBeenCalled();
  });

  it("should check register button text & onClick event", () => {
    wrapper.setState({ show_otp_field: true });
    expect(wrapper.find("#register-button").text()).toBe(
      "Verify OTP and Register"
    );
    wrapper
      .find("#register-button")
      .simulate("click", { preventDefault: () => {} });
    let onRegisterButtonClick = jest.spyOn(
      wrapper.instance(),
      "onRegisterButtonClick"
    );

    expect(onRegisterButtonClick).toHaveBeenCalled();
  });
});
