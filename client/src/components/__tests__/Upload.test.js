import { shallow } from "enzyme";
import  {Upload}  from "../Upload";
import React from "react";

const upload = jest.fn();
const onUpload = jest.fn();
const onChangeHandler = jest.fn();
const clearReducer = jest.fn();
const wrapper = shallow(
  <Upload
    upload={upload}
    onUpload={onUpload}
    onChangeHandler={onChangeHandler}
    clearReducer={clearReducer}
  />
);

describe("test case for upload component", () => {
  it("It should match snapshot", () => {
    expect(wrapper).toMatchSnapshot();
  });

  it("Checking if component is mounted and the actions in the component are called", () => {
    const spy = jest.spyOn(Upload.prototype, "componentDidMount");
    wrapper.instance().componentDidMount();
    expect(componentDidMount).toHaveBeenCalled();
    expect(clearReducer).toHaveBeenCalled();
  });
  it("should check labels text value for login field onchange and state change", () => {
    expect(wrapper.find("#upload-file").length).toBe(1);
    expect(wrapper.find("#upload-file").prop("name")).toBe("file");
    expect(wrapper.find("#upload-file").prop("type")).toBe("file");
    expect(wrapper.find("#upload-file").prop("enctype")).toBe(
      "multipart/form-data"
    );
    expect(wrapper.find("#upload-file").prop("accept")).toBe(".csv");

    wrapper.instance().onChange();
    const onChange = jest.spyOn(wrapper.instance(), "onChange");
    let onUpload = jest.spyOn(wrapper.instance(), "onUpload");
    expect(onUpload).toHaveBeenCalled();
  });
});
