import React from "react";
import { shallow, mount } from "enzyme";
import FilterModal from "../FilterModal";

const handleCloseButton = jest.fn();
const handleSelectOption = jest.fn();
const wrapper = shallow(
  <FilterModal
    handleCloseButton={handleCloseButton}
    handleSelectOption={handleSelectOption}
  />
);

describe("testing filter modal", () => {
  it("It should match snapshot", () => {
    expect(wrapper).toMatchSnapshot();
  });
  it("checks for the functions to be called", () => {
    wrapper.find("#filter-close-icon").simulate("click");
    expect(handleCloseButton).toBeCalled();

    wrapper.find("#sector-select").simulate("click");
    expect(handleSelectOption).toBeCalledWith("Sector");
    expect(handleSelectOption).not.toBeCalledWith("Industry");

    wrapper.find("#industry-select").simulate("click");
    expect(handleSelectOption).toBeCalledWith("Industry");
    // expect(handleSelectOption).not.toBeCalledWith("Sector");
  });
  it("checks for the appropriate text to be present", () => {
    expect(wrapper.find("#filter-title").text()).toBe("Filter");
    expect(wrapper.find("#sector-title").text()).toBe("Sector");
    expect(wrapper.find("#industry-title").text()).toBe("Industry");
  });
});
