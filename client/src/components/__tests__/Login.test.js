import React from "react";
import { shallow, mount } from "enzyme";
import { Login } from "../Login";
// import { Router } from "react-router";
// import { BrowserRouter, Route, MemoryRouter } from "react-router-dom";
// import { render } from "react-dom";
// import { act } from "react-dom/test-utils";

// import { createMemoryHistory } from "history";

const userLogin = jest.fn();
const clearReducer = jest.fn();
const preventDefault = jest.fn();

const wrapper = shallow(
  <Login
    userLogin={userLogin}
    clearReducer={clearReducer}
    preventDefault={preventDefault}
  />
);

describe("login component Testing", () => {
  it("should render component", () => {
    expect(wrapper).toMatchSnapshot();
  });

  it("should check if component is mounted and the actions in the component are called", () => {
    // const componentDidMount = jest.spyOn(
    //   wrapper.instance(),
    //   "componentDidMount"
    // );

    // expect(componentDidMount).toHaveBeenCalled();
    expect(clearReducer).toHaveBeenCalled();
  });
  it("should check labels text value for login field onchange and state change", () => {
    expect(wrapper.find("#login-email").length).toBe(1);
    expect(wrapper.find("#login-email").prop("placeholder")).toBe(" ");
    expect(wrapper.find("#login-email").prop("name")).toBe("email");
    expect(wrapper.find("#login-email").prop("type")).toBe("email");
    expect(wrapper.find("#login-email").prop("autoComplete")).toBe("off");
    const event = {
      target: {
        name: "email",
        value: "test"
      }
    };
    wrapper.instance().onChange(event);
    expect(wrapper.state().email).toBe(event.target.value);
    const onChange = jest.spyOn(wrapper.instance(), "onChange");
    wrapper.setState({ email: "test" });
    expect(wrapper.state().email).toBe("test");
  });
  it("should check labels text value  for password field onchange and state change", () => {
    expect(wrapper.find("#login-password").length).toBe(1);
    expect(wrapper.find("#login-password").prop("placeholder")).toBe(" ");
    expect(wrapper.find("#login-password").prop("name")).toBe("password");
    expect(wrapper.find("#login-password").prop("type")).toBe("password");
    expect(wrapper.find("#login-password").prop("autoComplete")).toBe("off");
    const event = {
      target: {
        name: "password",
        value: "test123"
      }
    };
    wrapper.instance().onChange(event);
    expect(wrapper.state().password).toBe(event.target.value);
    const onChange = jest.spyOn(wrapper.instance(), "onChange");
    wrapper.setState({ password: "test123" });
    expect(wrapper.state().password).toBe("test123");
  });
  it("should check login tab ", () => {
    expect(wrapper.find("#login-tab").text()).toBe("Login");
  });
  it("should check register tab & onClick event", () => {
    // const wrapper1 = shallow(
    //   <MemoryRouter>
    //     <Route>
    //       <Login
    //         onLogin={onLogin}
    //         userLogin={userLogin}
    //         onChange={onChange}
    //         clearReducer={clearReducer}
    //         getIem={getIem}
    //       />
    //     </Route>
    //   </MemoryRouter>
    // );
    expect(wrapper.find("#register-tab").text()).toBe("Register");
    wrapper.find("#register-tab").simulate("click");
    // expect(
    //   wrapper1
    //     .find(Link)
    //     .at(0)
    //     .props().to
    // ).toBe("/register");
  });

  it("should check login button text & onClick event", () => {
    expect(wrapper.find("#login-button").text()).toBe("Login");
    let onLogin = jest.spyOn(wrapper.instance(), "onLogin");
    wrapper.find("#login-button").simulate("click");

    const user = {
      email: "test@test.com",
      password: "test123"
    };

    expect(onLogin).toHaveBeenCalled();
  });
  it("should check login button text & onClick event", () => {
    expect(wrapper.find("#forget-password-link").text()).toBe(
      "Forgot Password?"
    );
  });
  // it("navigates register page  when you clicked on register tab", async => {
  //   // in a real test a renderer like "@testing-library/react"
  //   // would take care of setting up the DOM elements
  //   const root = document.createElement("div");
  //   document.body.appendChild(root);

  //   // Render app
  //   render(
  //     <MemoryRouter initialEntries={["/login"]}>
  //       <Login
  //         onLogin={onLogin}
  //         userLogin={userLogin}
  //         onChange={onChange}
  //         clearReducer={clearReducer}
  //         getIem={getIem}
  //       />
  //     </MemoryRouter>,
  //     root
  //   );

  //   // Interact with page
  //   act(() => {
  //     // Find the link (perhaps using the text content)
  //     const goHomeLink = document.querySelector("#register-tab");
  //     // Click it
  //     goHomeLink.dispatchEvent(new MouseEvent("click"));
  //   });

  //   // Check correct page content showed up
  //   expect(document.body.textContent).toBe("LoginRegisterNameEmail");
  // });
});
