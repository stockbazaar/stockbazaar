import React from "react";
import { shallow } from "enzyme";
import { Tabs } from "../common/Tabs";

// dummy data for rows
// Created: Shweta, 24-11-10 12.45pm
// Modified: Shweta, 24-11-10 12.45pm
const rows = [
  {
    tabName: "Overview"
  },
  {
    tabName: "Performance"
  },
  {
    tabName: "Valuation"
  },
  {
    tabName: "Margin"
  }
];
// dummy data for columns
// Created: Ankit, 07-11-2019 12:00pm
// Modified: Ankit, 13-11-2019 08:23pm

const id = "gainers";

// mocking history.push function
const historyMock = { push: jest.fn() };

// Test suite to check the props passed to the Tables component
// Created: Ankit, 07-11-2019 12:10pm
// Modified: Ankit, 08-11-2019 13:17pm
describe("checks for the props data which is mapped in component", () => {
  // Snapshot testing
  it("render the component", () => {
    // creates a shallow wrapper of Tables Component
    const wrapper = shallow(
      <Tabs id={id} rowsData={rows} history={historyMock} />
    );
    expect(wrapper).toMatchSnapshot();
  });

  it("checks for id, columnNames and rows length to be same as array size", () => {
    // creates a shallow wrapper of Tables Component
    const wrapper = shallow(
      <Tabs id={id} rowsData={rows} history={historyMock} />
    );

    // checks if table id is same as passed in props
    // expect(wrapper.find("table").prop("id")).toBe(tableId);

    // checks the no. of column names to be same as size of
    // column names array passed in columns props
    expect(wrapper.find("th").length).toBe(Object.keys(tabName).length);

    // checks the text of column names to be same as the column names passed
    // in the props and finding by id by converting it to lowercase and
    // replacing special characters with hyphen (-)

    // checks for the no. of rows to be same as the size
    // of the rows array
    expect(wrapper.find("tr").length).toBe(wrapper.state().rowsData.length + 1);
  });
});
