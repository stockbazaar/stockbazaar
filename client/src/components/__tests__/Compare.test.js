import React from "react";
import { shallow } from "enzyme";
import { Compare } from "../Compare";

const search = [{ ticker_name: "AAPL" }];

const getStockDetails = jest.fn();
const getFinancialReports = jest.fn();
const getEarningsVsRevenue = jest.fn();

const financialdetails = {
  datesCF: [
    {
      "Abnormal Gains/Losses": 0,
      "Accounts Payable": 30443,
      "Avg Basic Shares Outstanding": 4674071,
      "Avg Diluted Shares Outstanding": 4700646,
      "Book to Market": 0.1214,
      COGS: 36194
    }
  ]
};

// const financialdetails2 = {
//   datesCF: [
//     {
//       COGS: 36194
//     }
//   ]
// };

const financialdetails2 = {
  EPS: 34212
};

const stockdetails = [
  {
    last_date: "2019-07-01",
    last_price: 201,
    change: 1.81,
    changePercentage: 0.9,
    ticker_id: 9,
    ticker_name: "AAPL",
    company_name: "Apple Inc.",
    profile: "Apple Pay",
    ticker_logo: "PHN2Zy"
  }
];

const stockdetails2 = [
  {
    last_date: "2019-07-01",
    last_price: 201,
    change: 1.81,
    changePercentage: 0.9,
    ticker_id: 9,
    ticker_name: "AAPL",
    company_name: "Apple Inc.",
    profile: "Apple Pay",
    ticker_logo: "PHN2Zy"
  }
];

const wrapper = shallow(
  <Compare
    search={search}
    financialdetails={financialdetails}
    financialdetails2={financialdetails2}
    stockdetails={stockdetails}
    stockdetails2={stockdetails2}
    getStockDetails={getStockDetails}
    getFinancialReports={getFinancialReports}
  />
);

describe("test for the text, input, css properties and icons on the navbar and snapshot testing", () => {
  it("renders the component for snapshot testing", () => {
    expect(wrapper).toMatchSnapshot();
  });
});

describe("Testing if the Search Result is Properly Mapped", () => {
  // Testing for Ticker Name in the Search Query - Eli
  it("checks for the search array in search object", () => {
    // Testing the Div with a particular ID and expecting its Text to be the Declared One - Eli
    expect(wrapper.find("#jest-search-list-item-1").text()).toBe("AAPL");
  });

  // Testing for Ticker Name in the Search Query - Eli
  it("checks for the search array in search object", () => {
    // Testing the Div with a particular ID and expecting its Text to be the Declared One - Eli
    expect(wrapper.find("#jest-search-list-item-2").text()).toBe("AAPL");
  });

  describe("Testing for the value of the setState", () => {
    it("checks for the value in the setState object", () => {
      // Testing a Particular Div and checking its Action - Eli
      wrapper.find("#jest-div-1").simulate("click");
      // Expecting a Particular setState Result for the Action/Simulation - Eli
      expect(wrapper.setState).toBe("");
    });

    it("checks for the value in the setState object", () => {
      // Testing a Particular Div and checking its Action - Eli
      wrapper.find("#jest-div-2").simulate("click");
      // Expecting a Particular setState Result for the Action/Simulation - Eli
      expect(wrapper.setState).toBe("");
    });
  });
});

describe("test for the text and snapshot testing", () => {
  it("should have text Ratios, Price, Profit & Loss, Balance Sheet, Cash Flow", () => {
    // checks for the links text
    expect(wrapper.find("#ratios-1").text()).toBe("Ratios");
    expect(wrapper.find("#stock-chart-1").text()).toBe(
      "Stock Chart+ Comparison"
    );
    expect(wrapper.find("#pandl-1").text()).toBe(
      "Profit & Loss<StockSpecificSection />"
    );
    expect(wrapper.find("#balancesheet-1").text()).toBe(
      "Balance Sheet<StockSpecificSection />"
    );
    expect(wrapper.find("#cashflow-1").text()).toBe(
      "Cash Flow<StockSpecificSection />"
    );
  });
});

describe("test for the text and snapshot testing", () => {
  it("should have text Ratios, Price, Profit & Loss, Balance Sheet, Cash Flow", () => {
    // checks for the links text
    expect(wrapper.find("#ratios-2").text()).toBe("Ratios");
    expect(wrapper.find("#stock-chart-2").text()).toBe(
      "Stock Chart+ Comparison"
    );
    expect(wrapper.find("#pandl-2").text()).toBe(
      "Profit & Loss<StockSpecificSection />"
    );
    expect(wrapper.find("#balancesheet-2").text()).toBe(
      "Balance Sheet<StockSpecificSection />"
    );
    expect(wrapper.find("#cashflow-2").text()).toBe(
      "Cash Flow<StockSpecificSection />"
    );
  });
});

// describe("checks for the props data which is mapped in component", () => {
//   const stockdetails1 = [
//     {
//       last_date: "2019-07-01",
//       last_price: 201,
//       change: 1.81,
//       changePercentage: 0.9,
//       ticker_id: 9,
//       ticker_name: "AAPL",
//       company_name: "",
//       profile: "",
//       ticker_logo: ""
//     }
//   ];
//   const wrapper1 = shallow(
//     <Compare
//       stockdetails={stockdetails1}
//       getStockDetails={getStockDetails}
//       getFinancialReports={getFinancialReports}
//       getEarningsVsRevenue={getEarningsVsRevenue}
//       getProfitLoss={getProfitLoss}
//     />
//   );
//   it("If company name is not present in stockdetails, then text should be NA", () => {
//     expect(wrapper1.find("#positivecompanyname").text()).toBe("NA");
//   });

//   it("If ticker_logo is not present in stockdetails, then default image will be displayed", () => {
//     expect(wrapper1.find("img").prop("src")).toEqual(stockBazaarLogo);
//   });
// });
