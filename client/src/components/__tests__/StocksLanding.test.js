import React from "react";
import { shallow } from "enzyme";
import { StocksLanding } from "../StocksLandingPage";

getIndexOHLC = { params: { tickerName: "RUT" } };
getAllIndices = jest.fn();
getOverview = jest.fn();
getPerformance = jest.fn();
getSectors = jest.fn();
getIndustries = jest.fn();

const wrapper = shallow(
  <StocksLanding
    getAllIndices={getAllIndices}
    getIndexOHLC={getIndexOHLC}
    getOverview={getOverview}
    getSectors={getSectors}
    getIndustries={getIndustries}
  />
);

describe("test  Component", () => {
  it("render the component", () => {
    expect(wrapper).toMatchSnapshot();
  });
  it("checks for the getOverview function to be called", () => {
    expect(getOverview).toHaveBeenCalledWith(1);
  });

  it("checks for the getPerformance function to be called", () => {
    expect(getPerformance).toHaveBeenCalledWith(1);
  });
  it("checks for the getAllIndices function to be called", () => {
    expect(getAllIndices).toHaveBeenCalledWith(1);
  });

  it("checks for the getSectors function to be called", () => {
    expect(getSectors).toHaveBeenCalledWith(1);
  });
  it("checks for the getIndustries function to be called", () => {
    expect(getIndustries).toHaveBeenCalledWith(1);
  });
  it("checks for the getIndexOHLC(RUT) function to be called", () => {
    expect(getIndexOHLC("RUT")).toHaveBeenCalledWith(1);
  });
  it("there should be a section", () => {
    expect(wrapper.find("section").length).toBe(4);
  });
  it("there should be a h3", () => {
    expect(wrapper.find("h3").length).toBe(1);
    expect(wrapper.find(marketHeading).text()).toBe("Market Indices");
  });
  it("should have Market, Gainers, Losers, Sector and Industry", () => {
    // checks for the links text
    expect(wrapper.find("#marketIndices").text()).toBe("Market");
    expect(wrapper.find("#gainers").text()).toBe("Gainers");
    expect(wrapper.find("#losers").text()).toBe("Losers");
    expect(wrapper.find("#sectors").text()).toBe("Sector");
    expect(wrapper.find("#industry").text()).toBe("Industry");
  });
});
