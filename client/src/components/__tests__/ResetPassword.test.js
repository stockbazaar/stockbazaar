import React from "react";
import { shallow } from "enzyme";
import { ResetPassword } from "../ResetPassword";

const sendOTPtoRegisteredEmail = jest.fn();
const verifyOTPReset = jest.fn();
const onChange = jest.fn();
const clearReducer = jest.fn();

const wrapper = shallow(
  <ResetPassword
    sendOTPtoRegisteredEmail={sendOTPtoRegisteredEmail}
    verifyOTPReset={verifyOTPReset}
    onChange={onChange}
    clearReducer={clearReducer}
  />
);
describe("reset password component Testing", () => {
  it("should render component", () => {
    expect(wrapper).toMatchSnapshot();
  });

  it("should check if component is mounted and the actions in the component are called", () => {
    const componentDidMount = jest.spyOn(
      ResetPassword.prototype,
      "componentDidMount"
    );

    expect(componentDidMount).toHaveBeenCalled();
    expect(clearReducer).toHaveBeenCalled();
    expect(wrapper.find("h1").text()).toBe("Reset Password");
  });

  it("should check labels text value for login field onchange and state change", () => {
    expect(wrapper.find("#reset-email").length).toBe(1);
    expect(wrapper.find("#reset-email").prop("placeholder")).toBe(" ");
    expect(wrapper.find("#reset-email").prop("name")).toBe("email");
    expect(wrapper.find("#reset-email").prop("type")).toBe("email");
    expect(wrapper.find("#reset-email").prop("autoComplete")).toBe("off");
    const event = {
      target: {
        name: "email",
        value: "test"
      }
    };
    wrapper.instance().onChange(event);
    expect(wrapper.state().email).toBe(event.target.value);
    const onChange = jest.spyOn(wrapper.instance(), "onChange");
    wrapper.setState({ email: "test" });
    expect(wrapper.state().email).toBe("test");
  });
  it("should check labels text value  for password field onchange and state change", () => {
    expect(wrapper.find("#reset-password").length).toBe(1);
    expect(wrapper.find("#reset-password").prop("placeholder")).toBe(" ");
    expect(wrapper.find("#reset-password").prop("name")).toBe("password");
    expect(wrapper.find("#reset-password").prop("type")).toBe("password");
    expect(wrapper.find("#reset-password").prop("autoComplete")).toBe("off");
    const event = {
      target: {
        name: "password",
        value: "test123"
      }
    };
    wrapper.instance().onChange(event);
    expect(wrapper.state().password).toBe(event.target.value);
    const onChange = jest.spyOn(wrapper.instance(), "onChange");
    wrapper.setState({ password: "test123" });
    expect(wrapper.state().password).toBe("test123");
  });
  it("should check labels text value for login field onchange and state change", () => {
    wrapper.setState({ show_otp_field: true });
    expect(wrapper.find("#reset-otp").length).toBe(1);
    expect(wrapper.find("#reset-otp").prop("placeholder")).toBe(" ");
    expect(wrapper.find("#reset-otp").prop("name")).toBe("otp");
    expect(wrapper.find("#reset-otp").prop("type")).toBe("password");
    expect(wrapper.find("#reset-otp").prop("autoComplete")).toBe("off");
    const event = {
      target: {
        name: "otp",
        value: "1234"
      }
    };
    wrapper.instance().onChange(event);
    expect(wrapper.state().otp).toBe(event.target.value);
    const onChange = jest.spyOn(wrapper.instance(), "onChange");
    wrapper.setState({ otp: "1234" });
    expect(wrapper.state().otp).toBe("1234");
  });

  it("should check send otp button text & onClick event", () => {
    expect(wrapper.find("#send-otp-button").text()).toBe("Send OTP");
    wrapper
      .find("#send-otp-button")
      .simulate("click", { preventDefault: () => {} });
    let onSendOTPButtonClick = jest.spyOn(
      wrapper.instance(),
      "onSendOTPButtonClick"
    );
    expect(onSendOTPButtonClick).toHaveBeenCalled();
  });

  it("should check reset button text & onClick event", () => {
    wrapper.setState({ show_otp_field: true });
    expect(wrapper.find("#reset-button").text()).toBe(
      "Verify OTP and Reset Password"
    );
    wrapper
      .find("#reset-button")
      .simulate("click", { preventDefault: () => {} });
    let onResetPasswordButtonClick = jest.spyOn(
      wrapper.instance(),
      "onResetPasswordButtonClick"
    );

    expect(onResetPasswordButtonClick).toHaveBeenCalled();
  });
});
