import React from "react";
import { shallow } from "enzyme";
import { TickersTables } from "../common/TickersTables";

// dummy data for rows
// Created: Ankit, 07-11-2019 12:00pm
// Modified: Ankit, 07-11-2019 12:00pm
const rows = [
  {
    tickerId: 1,
    tickerName: "AAPL",
    tickerCompanyName: "Apple Corporation",
    tickerValue: {
      margin: 46.156,
      dividend: 5654.68,
      price: 564
    }
  },
  {
    tickerId: 2,
    tickerName: "MSFT",
    tickerValue: {
      margin: 476526,
      dividend: 215654.68,
      price: 5686454
    }
  },
  {
    tickerId: 3,
    tickerName: "GOOG",
    tickerValue: {
      margin: 856,
      dividend: 256.68,
      price: 9875
    }
  }
];
// dummy data for columns
// Created: Ankit, 07-11-2019 12:00pm
// Modified: Ankit, 13-11-2019 08:23pm
const columns = {
  tickerName: "Ticker",
  "Share Price": "Last",
  change: "CHG",
  changePercentage: "CHG%",
  ratting: "Rating",
  Volume: "Volume",
  "Market Capitalisation": "MKT CAP",
  pToE: "P/E",
  EPS: "EPS"
};

const tableId = "section1";

// mocking history.push function
const historyMock = { push: jest.fn() };

// Test suite to check the props passed to the Tables component
// Created: Ankit, 07-11-2019 12:10pm
// Modified: Ankit, 08-11-2019 13:17pm
describe("checks for the props data which is mapped in component", () => {
  // Snapshot testing
  it("render the component", () => {
    // creates a shallow wrapper of Tables Component
    const wrapper = shallow(
      <TickersTables
        id={tableId}
        rowsData={rows}
        columnNames={columns}
        linesType="light"
        onClickBehaviour={true}
        isIndex={false}
        history={historyMock}
      />
    );
    expect(wrapper).toMatchSnapshot();
  });

  it("checks for id, columnNames and rows length to be same as array size", () => {
    // creates a shallow wrapper of Tables Component
    const wrapper = shallow(
      <TickersTables
        id={tableId}
        rowsData={rows}
        columnNames={columns}
        linesType="light"
        onClickBehaviour={true}
        isIndex={false}
        history={historyMock}
      />
    );

    // checks if table id is same as passed in props
    // expect(wrapper.find("table").prop("id")).toBe(tableId);

    // checks the no. of column names to be same as size of
    // column names array passed in columns props
    expect(wrapper.find("th").length).toBe(Object.keys(columns).length);

    // checks the text of column names to be same as the column names passed
    // in the props and finding by id by converting it to lowercase and
    // replacing special characters with hyphen (-)

    expect(
      wrapper
        .find(
          "#" +
            Object.keys(columns)[0]
              .toLowerCase()
              .replace(/[^a-zA-Z0-9]/g, "-")
        )
        .text()
    ).toBe(Object.values(columns)[0]);
    expect(
      wrapper
        .find(
          "#" +
            Object.keys(columns)[1]
              .toLowerCase()
              .replace(/[^a-zA-Z0-9]/g, "-")
        )
        .text()
    ).toBe(Object.values(columns)[1]);
    expect(
      wrapper
        .find(
          "#" +
            Object.keys(columns)[2]
              .toLowerCase()
              .replace(/[^a-zA-Z0-9]/g, "-")
        )
        .text()
    ).toBe(Object.values(columns)[2]);

    // checks for the no. of rows to be same as the size
    // of the rows array
    expect(wrapper.find("tr").length).toBe(wrapper.state().rowsData.length + 1);
  });

  it("checks for functions to be called with correct params if any and branches", () => {
    // creates a shallow wrapper of Tables Component with
    // isIndex false and onClickBehaviour true
    const wrapper1 = shallow(
      <TickersTables
        id={tableId}
        rowsData={rows}
        columnNames={columns}
        linesType="light"
        onClickBehaviour={true}
        isIndex={false}
        history={historyMock}
      />
    );

    // checks for history.push function to be called
    wrapper1
      .find("#" + wrapper1.state().rowsData[0].tickerName)
      .simulate("click");
    expect(historyMock.push).toBeCalledWith(
      "/stockspecific/" + wrapper1.state().rowsData[0].tickerId
    );

    const spySortData = jest.spyOn(wrapper1.instance(), "sortData");
    wrapper1
      .find(
        "#" +
          Object.keys(columns)[2]
            .toLowerCase()
            .replace(/[^a-zA-Z0-9]/g, "-")
      )
      .simulate("click");
    expect(spySortData).toBeCalledWith(Object.keys(columns)[2]);

    // // checks for onClick attribute value when isIndex is false
    // // and onClickBehaviour is true
    // expect(wrapper1.find("#" + wrapper1.state().rowsData[0].tickerName).prop("onClick")).toBe(
    //   expect.any(Function)
    // );

    // expect "table-border and table-wrapper" class is present for
    // div when isIndex is false
    expect(wrapper1.find("#" + tableId + "-div").prop("className")).toBe(
      "table-wrapper table-border"
    );

    // expect "table-background" class is present for first
    // tr tag when isIndex is false
    expect(
      wrapper1
        .find("tr")
        .at(0)
        .prop("className")
    ).toBe("table-background");

    // creates a shallow wrapper of Tables Component with
    // isIndex true and onClickBehaviour true
    const getStockChartMock = jest.fn();
    const wrapper2 = shallow(
      <TickersTables
        id={tableId}
        rowsData={rows}
        columnNames={columns}
        linesType="light"
        onClickBehaviour={true}
        isIndex={true}
        history={historyMock}
        getStockChart={getStockChartMock}
      />
    );

    // expect getStockChart function to be called
    wrapper2
      .find("#" + wrapper2.state().rowsData[0].tickerName)
      .simulate("click");
    expect(getStockChartMock).toBeCalled();

    // checks for onClick attribute value when isIndex is true
    // and onClickBehaviour is true
    expect(
      wrapper2
        .find("#" + wrapper2.state().rowsData[0].tickerName)
        .prop("onClick")
    ).toBe(getStockChartMock);

    // expect "table-wrapper" class is present for
    // div when isIndex is true
    expect(wrapper2.find("#" + tableId + "-div").prop("className")).toBe(
      "table-wrapper"
    );

    // expect "" class is present for first
    // tr tag when isIndex is true
    expect(
      wrapper2
        .find("tr")
        .at(0)
        .prop("className")
    ).toBe("");

    // creates a shallow wrapper of Tables Component with
    // isIndex true and onClickBehaviour false
    const wrapper3 = shallow(
      <TickersTables
        id={tableId}
        rowsData={rows}
        columnNames={columns}
        linesType="light"
        onClickBehaviour={false}
        isIndex={true}
        history={historyMock}
      />
    );

    // checks for onClick attribute value when isIndex is true
    // and onClickBehaviour is false
    expect(
      wrapper3
        .find("#" + wrapper3.state().rowsData[0].tickerName)
        .prop("onClick")
    ).toBe(null);

    // creates a shallow wrapper of Tables Component with
    // isIndex false and onClickBehaviour false
    const wrapper4 = shallow(
      <TickersTables
        id={tableId}
        rowsData={rows}
        columnNames={columns}
        linesType="light"
        onClickBehaviour={false}
        isIndex={false}
        history={historyMock}
      />
    );

    // checks for onClick attribute value when isIndex is false
    // and onClickBehaviour is false
    expect(
      wrapper4
        .find("#" + wrapper4.state().rowsData[0].tickerName)
        .prop("onClick")
    ).toBe(null);
  });
});
