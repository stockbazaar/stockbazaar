import React from "react";
import { shallow, mount } from "enzyme";
import SelectOptions from "../SelectOptions";

const handleCheckboxChange = jest.fn();

const industriesBySector = [
  {
    sector: {
      name: "Consumer Cyclical",
      isChecked: false
    },
    industries: [
      {
        name: "Entertainment",
        isChecked: false
      },
      {
        name: "Homebuilding & Construction",
        isChecked: false
      }
    ]
  }
];

const filteredTickers = [
  {
    _id: "5dc940fc03db8b61309e2eeb",
    sector: "Technology",
    industry: "Computer Hardware",
    ticker_data: {
      ticker_name: "AAPL",
      ticker_id: 9,
      secondLast: {
        "Share Price": 199.74,
        "Market Capitalisation": 919018.7205,
        date: "2019-06-27T00:00:00.000Z"
      },
      last: {
        "Share Price": 201.55,
        "Market Capitalisation": 927346.6663,
        date: "2019-07-01T00:00:00.000Z"
      },
      change: 1.81,
      changePercentage: 0.9
    }
  }
];
const checkboxIndustryData = industriesBySector[0].industries[0];
const checkboxSectorData = industriesBySector[0].sector;

const title = "Industry";
const searchFilterInput = "";
const handleBackButton = jest.fn();
const handleCloseButton = jest.fn();
const handleSearchInputChange = jest.fn();
const handleApplyClick = jest.fn();
const selectWrapper = mount(
  <SelectOptions
    title={title}
    searchFilterInput={searchFilterInput}
    checkboxData={checkboxIndustryData}
    handleBackButton={handleBackButton}
    handleCloseButton={handleCloseButton}
    handleSearchInputChange={handleSearchInputChange}
    handleCheckboxChange={handleCheckboxChange}
    handleApplyClick={handleApplyClick}
    industriesBySector={industriesBySector}
  />
);

describe("test for SelectOptions function", () => {
  it("checks for filter header text and functions", () => {
    expect(selectWrapper.find("#filter-title").text()).toBe(title);

    selectWrapper.find("#filter-back-icon").simulate("click");
    expect(handleBackButton).toBeCalled();

    selectWrapper.find("#filter-close-icon").simulate("click");
    expect(handleCloseButton).toBeCalled();
  });

  it("checks for the filter items div", () => {
    expect(selectWrapper.find("#search-filter-input")).toBeTruthy();
    expect(selectWrapper.find("#search-filter-input").props().value).toBe(
      searchFilterInput
    );
    expect(selectWrapper.find("#search-filter-input").props().placeholder).toBe(
      "Search for " + title
    );

    const event = {
      target: {
        name: "searchFilterInput",
        value: "AAPL"
      }
    };
    selectWrapper.find("#search-filter-input").simulate("change", event);
    expect(handleSearchInputChange).toBeCalled();

    expect(selectWrapper.find("#apply-button").text()).toBe("Apply");

    selectWrapper.find("#apply-button").simulate("click");
    expect(handleApplyClick).toBeCalled();

    expect(selectWrapper.contains("Checkbox"));
  });

  it("checks for absence of search for sector", () => {
    const sectorTitle = "Sector";
    const selectWrapper1 = mount(
      <SelectOptions
        title={sectorTitle}
        searchFilterInput={searchFilterInput}
        checkboxData={checkboxSectorData}
        handleBackButton={handleBackButton}
        handleCloseButton={handleCloseButton}
        handleSearchInputChange={handleSearchInputChange}
        handleApplyClick={handleApplyClick}
        industriesBySector={industriesBySector}
      />
    );
    expect(selectWrapper1.find("#search-filter-input").length).toBe(0);
  });
});

// describe("test for checkbox component", () => {
//   it("checks for the function to be called with checkbox value and checked property", () => {
//     expect(
//       selectWrapper
//         .find(
//           "#checkbox-" +
//             checkboxIndustryData.name
//               .toLowerCase()
//               .replace(/[^a-zA-Z0-9]/g, "-")
//         )
//         .text()
//     ).toBe(checkboxIndustryData.name);
//     expect(
//       selectWrapper
//         .find(
//           "#checkbox-" +
//             checkboxIndustryData.name
//               .toLowerCase()
//               .replace(/[^a-zA-Z0-9]/g, "-")
//         )
//         .props().value
//     ).toBe(checkboxIndustryData.name);
//     expect(
//       selectWrapper
//         .find(
//           "#checkbox-" +
//             checkboxIndustryData.name
//               .toLowerCase()
//               .replace(/[^a-zA-Z0-9]/g, "-")
//         )
//         .props().checked
//     ).toBe(checkboxIndustryData.isChecked);

//     selectWrapper
//       .find(
//         "#checkbox-" +
//           checkboxIndustryData.name.toLowerCase().replace(/[^a-zA-Z0-9]/g, "-")
//       )
//       .simulate("click");
//     expect(handleCheckboxChange).toBeCalled();
//   });
// });
