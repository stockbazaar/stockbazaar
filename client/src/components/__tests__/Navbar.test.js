import React from "react";
import { shallow, mount } from "enzyme";
import { Navbar } from "../common/Navbar";

const search = [{ ticker_name: "AAPL" }];

const wrapper = shallow(<Navbar search={search} />);

describe("test for the text, input, css properties and icons on the navbar and snapshot testing", () => {
  it("renders the component for snapshot testing", () => {
    expect(wrapper).toMatchSnapshot();
  });

  it("should have text brand stocks, compare, screener", () => {
    // checks for the links text
    expect(wrapper.find("#stocks").text()).toBe("Stocks");
    expect(wrapper.find("#compare").text()).toBe("Compare");
    expect(wrapper.find("#screener").text()).toBe("Screener");
    expect(wrapper.find("#login").text()).toBe("Login");
    expect(wrapper.find("#register").text()).toBe("Register");
  });
  it("testing for search input field", () => {
    // checks for the presence of search input field
    expect(wrapper.find("#searchInput").length).toBe(1);

    // checks placeholder of the input field
    expect(wrapper.find("#searchInput").prop("placeholder")).toBe(
      "Search for Companies"
    );
    // checks for the input value to be same as state while inserting text - Eli
    const inputSearch = jest.spyOn(wrapper.instance(), "inputSearch");
    const e = {
      target: {
        name: "searchInput",
        value: "aapl"
      }
    };
    wrapper.instance().handleSearchInputChange(e);
    expect(wrapper.state().searchInput).toBe(e.target.value);
    // inputSearch field to be Called - Eli
    expect(inputSearch).toBeCalled();
  });
});

describe("test for functions -> toogleChange, updateDimensions, logout, renderRedirect", () => {
  // toggleChange function to be called
  it("checks for the toggleChange function to be called when menu-btn and nav-brand is clicked and when clicked on li links", () => {
    const spyToggleChange = jest.spyOn(wrapper.instance(), "toggleChange");

    // expects toggleChange to be called on clicking menu-btn
    wrapper.find("#menu-btn").simulate("click");
    expect(spyToggleChange).toHaveBeenCalledWith(wrapper.state().pageLink);

    // expects toggleChange to be called on clicking nav-brand
    wrapper.find("#nav-brand").simulate("click");
    expect(spyToggleChange).toHaveBeenCalledWith("");

    // expects toggleChange to be called on clicking stocks-li
    wrapper.find("#stocks-li").simulate("click");
    expect(spyToggleChange).toHaveBeenCalledWith("stocks");

    // expects toggleChange to be called on clicking compare-li
    wrapper.find("#compare-li").simulate("click");
    expect(spyToggleChange).toHaveBeenCalledWith("compare");

    // expects toggleChange to be called on clicking screener-li
    wrapper.find("#screener-li").simulate("click");
    expect(spyToggleChange).toHaveBeenCalledWith("screener");
  });

  // updateDimensions function to be called
  it("checks for the updateDimensions function to be called on window size change", () => {
    const spyUpdateDimensions = jest.spyOn(
      wrapper.instance(),
      "updateDimensions"
    );

    // Triggers the window resize event.
    global.addEventListener("resize", spyUpdateDimensions);
    global.dispatchEvent(new Event("resize"));
    // expects updateDimensions to be called on clicking men link
    // wrapper.find("#menu-btn").simulate("click");
    expect(spyUpdateDimensions).toBeCalled();
  });

  // renderRedirect function to be called
  it("checks for renderRedirect function to be called when page renders", () => {
    const spyRenderRedirect = jest.spyOn(wrapper.instance(), "renderRedirect");

    // expect(spyRenderRedirect).toBeCalled();
  });

  // // logout function to be called
  // it("checks for logout function to be called when clicked on logout button", () => {
  //   const spyLogout = jest.spyOn(wrapper.instance(), "logout");
  //   // console.log("localstorage", window.localStorage.__proto__);
  //   // const token = "sf";
  //   // localStorage.setItem("token", token);
  //   // const spyRemoveItem = jest.spyOn(
  //   //   window.localStorage.__proto__,
  //   //   "removeItem"
  //   // );
  //   // const spyGetItem = jest.spyOn(window.localStorage.__proto__, "getItem");
  //   // expect(spyGetItem).toHaveBeenCalledWith("token");

  //   wrapper.find("#logout").simulate("click");
  //   expect(spyLogout).toBeCalled();
  //   // expect(spyRemoveItem).toHaveBeenCalledWith("token");
  //   wrapper.setState({ redirect: true });
  // });
});

describe("Testing if the Search Result is Properly Mapped", () => {
  // Testing for Ticker Name in the Search Query - Eli
  it("checks for the search array in search object", () => {
    // Testing the Div with a particular ID and expecting its Text to be the Declared One - Eli
    expect(wrapper.find("#jest-search-list-item").text()).toBe("AAPL");
  });
});

describe("Testing for the value of the setState", () => {
  it("checks for the value in the setState object", () => {
    // Testing a Particular Div and checking its Action - Eli
    wrapper.find("#jest-div").simulate("click");
    // Expecting a Particular setState Result for the Action/Simulation - Eli
    expect(wrapper.setState).toBe("");
  });
});
