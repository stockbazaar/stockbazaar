import React from "react";
import { shallow } from "enzyme";
import { ReusableSection } from "../common/ReusableSection";

// dummy data for rows
// Created: Shweta, 22-11-2019 03:30pm
// Modified: Shweta, 22-11-2019 03:30pm

const rows = [
  {
    tickerName: "AVXL",
    tickerValue: {
      last: 3.84,
      marketCap: 179,
      change: 1,
      changePercentage: 26.0417
    }
  },
  {
    tickerName: "PRPO",
    tickerValue: {
      last: 4.08,
      marketCap: 9,
      change: 1,
      changePercentage: 24.5098
    }
  },
  {
    tickerName: "HPJ",
    tickerValue: {
      last: 4.38,
      marketCap: 68,
      change: 1,
      changePercentage: 22.8311
    }
  }
];
// dummy data for columns
// Created: Shweta, 22-11-2019 03:30pm
// Modified: Shweta, 13-11-2019 08:23pm
const columns = {
  tickerName: "Ticker",
  last: "Last Price($)",
  marketCap: "Market Cap(M)",
  change: "CHG",
  changePercentage: "CHG%"
};

// const tableId = "section1";
const section_id = "gainers";
const table_id = "gainers-table";
const title = "Gainers";
const tab_id = "overview";

// mocking history.push function
const historyMock = { push: jest.fn() };

// Test suite to check the props passed to the Tables component
// Created: Shweta, 22-11-2019 12:10pm
// Modified: Shweta, 08-11-2019 13:17pm
describe("checks for the props data which is mapped in component", () => {
  // Snapshot testing
  it("render the component", () => {
    // creates a shallow wrapper of Tables Component
    const wrapper = shallow(
      <ReusableSection
        section_id={section_id}
        table_id={table_id}
        title={title}
        tab_id={tab_id}
        sectionData={[
          {
            tabName: "Overview",
            tabFunction: this.onClickGainersTab
          }
        ]}
        tableData={rows}
        tableHeadings={columns}
        linesType="light"
        onClickBehaviour={true}
        isIndex={false}
        history={historyMock}
      />
    );
    expect(wrapper).toMatchSnapshot();
  });

  it("checks for id, columnNames and rows length to be same as array size", () => {
    // creates a shallow wrapper of Tables Component
    const wrapper = shallow(
      <ReusableSection
        section_id={section_id}
        table_id={table_id}
        title={title}
        tab_id={tab_id}
        sectionData={[
          {
            tabName: "Overview",
            tabFunction: this.onClickGainersTab
          }
        ]}
        tableData={rows}
        tableHeadings={columns}
        linesType="light"
        onClickBehaviour={true}
        isIndex={false}
        history={historyMock}
      />
    );

    // checks if table id is same as passed in props
    // expect(wrapper.find("table").prop("id")).toBe(tableId);

    // checks the no. of column names to be same as size of
    // column names array passed in columns props
    expect(wrapper.find("tableHeadings").length).toBe(
      Object.keys(columns).length
    );

    // checks the text of column names to be same as the column names passed
    // in the props and finding by id by converting it to lowercase and
    // replacing special characters with hyphen (-)

    // checks for the no. of rows to be same as the size
    // of the rows array
    expect(wrapper.find("tr").length).toBe(wrapper.state().rowsData.length + 1);
  });

  it("checks for functions to be called with correct params if any and branches", () => {
    // creates a shallow wrapper of Tables Component with
    // isIndex false and onClickBehaviour true
    const wrapper1 = shallow(
      <ReusableSection
        section_id={section_id}
        table_id={table_id}
        title={title}
        tab_id={tab_id}
        sectionData={[
          {
            tabName: "Overview",
            tabFunction: this.onClickGainersTab
          }
        ]}
        tableData={rows}
        tableHeadings={columns}
        linesType="light"
        onClickBehaviour={true}
        isIndex={false}
        history={historyMock}
      />
    );

    // checks for history.push function to be called
    wrapper1
      .find("#" + wrapper1.state().tableData[0].tickerName)
      .simulate("click");
    expect(historyMock.push).toBeCalledWith(
      "/stockspecific/" + wrapper1.state().tableData[0].tickerId
    );
  });
});
