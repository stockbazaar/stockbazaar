import React, { Component } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Register from "./components/Register";
import Login from "./components/Login";
import Reset from "./components/ResetPassword";
import Navbar from "./components/common/Navbar";

//Rohan
import StockSpecific from "./components/StockSpecific";

// Ankit: 07-11-2019, 10:15am
import Screener from "./components/Screener";
import Chart from "./components/ChartComponent";
// import TestTables from "./components/extras/TestTables";

import Forbidden from "./components/common/Forbidden";

import Compare from "./components/Compare";

import StocksLanding from "./components/StocksLandingPage";
import Apps from "./components/ReusableSection";
import UploadFile from "./components/Upload";

//Eli
import Indices from "./components/common/Indices";

export default class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <div className="nav-fixed">
            <Indices />
            <Navbar />
          </div>
          {/* Ankit - 06-11-2019,10:18 */}
          {/* Added route for tables common component */}
          {/* <Route path="/tables" component={TestTables}></Route> */}
          <Route path="/screener" component={Screener}></Route>
          <Route path="/chart" component={Chart}></Route>

          <Route exact path="/register" component={Register}></Route>
          <Route exact path="/login" component={Login}></Route>
          <Route exact path="/reset" component={Reset}></Route>

          {/* kavita route for forbidden page */}
          <Route exact path="/forbidden" component={Forbidden}></Route>

          <Route exact path="/upload" component={UploadFile}></Route>

          <Route exact path="/compare" component={Compare}></Route>

          <Route exact path="/" component={StocksLanding}></Route>
          <Route exact path="/app" component={Apps}></Route>
          <Route
            exact
            path="/stockspecific/:ticker_id"
            component={StockSpecific}
          ></Route>
        </div>
      </Router>
    );
  }
}
