import os
import requests
import pandas as pd
from flask import Flask
from flask import request
from flask_cors import CORS
from flask_pymongo import PyMongo
import chart_studio
import plotly.graph_objs as go
import chart_studio.plotly as py
py.sign_in(os.getenv("stocksPlotlyId"), os.getenv("stocksPlotlyKey"))

app = Flask(__name__)
app.config['MONGO_URI'] = os.getenv("stocksMongoURL")
CORS(app)
mongo = PyMongo(app)
star = mongo.db.stocks_data_2
sp100 = mongo.db.S100
#stock chart
@app.route("/stockchart/<ticker_id>", methods=["GET"])
def stockChart(ticker_id):
    print(ticker_id)
    company = pd.DataFrame(list(star.find({"ticker_id":int(ticker_id)},{"ticker_dates"})))
    print(company.keys());
    
    stockDate=[]
    stockPrice=[]
    for val in company['ticker_dates'][0]:
        stockDate.append(val['date'])
#     print(type(val))
        if ('Share Price' in val):
            stockPrice.append(val['Share Price'])
        else:
             stockPrice.append("-")
                
    sharePrice = pd.DataFrame({"date":stockDate,"sharePrice":stockPrice})
    print(sharePrice)
    trace2= go.Scatter(
        x= sharePrice['date'],
        y= sharePrice['sharePrice'],text='Share Price'
    , marker= dict(size=2,
                    color='#ff6e85'
                   ),name='DIFFERENCE')

    layout=dict(
    title= dict(text='Stock Chart',font=dict(color="white")),xaxis=dict(showgrid=False, zeroline=False, color="white",title="Date", rangeselector=dict(
            buttons=list([
                dict(count=1,
                     label="1D",
                     step="day",
                     stepmode="backward",),
                dict(count=7,
                     label="1W",
                     step="day",
                     stepmode="backward"),
                dict(count=1,
                     label="1M",
                     step="month",
                     stepmode="backward"),
                 dict(count=3,
                     label="3M",
                     step="month",
                     stepmode="backward"),
                dict(count=6,
                     label="6M",
                     step="month",
                     stepmode="backward"),
#                 dict(count=1,
#                      label="YTD",
#                      step="year",
#                      stepmode="todate"),
                dict(count=1,
                     label="1Y",
                     step="year",
                     stepmode="backward"),
                dict(count=5,
                     label="5Y",
                     step="year",
                     stepmode="backward"),
                dict(label="ALL",
                     step="all")
            ]),activecolor="#ff6e85",
            x=0.11,
            xanchor="left",
            y=1.1,
            yanchor="top"
            ),showline=True, linewidth=2, linecolor='#4b527b'),yaxis=dict(color='white',zeroline=False,gridwidth=0.1, gridcolor='#4b527b',title="Share Price",showline=True, linewidth=2, linecolor='#4b527b'),
            paper_bgcolor='#262b4a',    
            plot_bgcolor='#262b4a'
    )

    data = [trace2]
    fig = dict(data=data,layout=layout)
    url=py.plot(fig)
#     print(url)
    print(url+".embed?autosize=true&modebar=false&link=false")
    url1Text=url
    head, sep, tail = url1Text.partition('.e')
    print("removed embed part", head+".embed?autosize=true&modebar=false&link=false")
    return (head+".embed?autosize=true&modebar=false&link=false")

#earnings vs revenue
@app.route("/earningsvsrevenue/<ticker_id>", methods=["GET"])
def earningsVsRevenue(ticker_id):
    print(ticker_id)
    response = requests.get(os.getenv("stocksNodeURL")+"/api/stockspecific/download/allreports/" + ticker_id)
    res = response.json()
    Revenues = []
    Dates = []
    Earnings = []
    for val in res['data']:
        Revenues.append(val['Revenues'])
    # for val in res['data']:
        Dates.append(val['date'])
    # for val in res['data']:
        if ('Retained Earnings' in val):
            Earnings.append(val['Retained Earnings'])
        else:
            Earnings.append("-")
    print(Revenues)
    print(Earnings)
    print(Dates)
    EarningsRevenues = pd.DataFrame({"date":Dates,"revenue":Revenues,"earnings":Earnings})
    print(EarningsRevenues)
    trace2= go.Scatter(
            x= EarningsRevenues['date'],
            y= EarningsRevenues['revenue'],text='Revenues'
        , marker= dict(size=2,
                       color='#00ccdd'
                       ),name='REVENUES',  fill='tozeroy')

    layout=dict(
        title= dict(text='Earnings Vs Revenue Chart',font=dict(color="white")),xaxis=dict(showgrid=False, zeroline=False, color="white",title="Date", rangeselector=dict(
                buttons=list([
                    dict(count=1,
                         label="1D",
                         step="day",
                         stepmode="backward",),
                    dict(count=7,
                         label="1W",
                         step="day",
                         stepmode="backward"),
                    dict(count=1,
                         label="1M",
                         step="month",
                         stepmode="backward"),
                     dict(count=3,
                         label="3M",
                         step="month",
                         stepmode="backward"),
                    dict(count=6,
                         label="6M",
                         step="month",
                         stepmode="backward"),
                    dict(count=1,
                         label="1Y",
                         step="year",
                         stepmode="backward"),
                    dict(count=5,
                         label="5Y",
                         step="year",
                         stepmode="backward"),
                    dict(label="ALL",
                         step="all")
                ]),activecolor="#ff6e85",
                x=0.11,
                xanchor="left",
                y=1.1,
                yanchor="top"
                ),showline=True, linewidth=2, linecolor='#4b527b'),yaxis=dict(color='white',zeroline=False,gridwidth=0.1, gridcolor='#4b527b',title="Revenues",showline=True, linewidth=2, linecolor='#4b527b'),
                paper_bgcolor='#262b4a',    
                plot_bgcolor='#262b4a'
        )

    trace3= go.Scatter(
            x= EarningsRevenues['date'],
            y= EarningsRevenues['earnings'],text='Earnings'
        , marker= dict(size=2,
                        color='#ff6e85'
                       ),name='EARNINGS', fill='tonexty')

    layout=dict(
        title= dict(text='Earnings Vs Revenue Chart',font=dict(color="white")),xaxis=dict(showgrid=False, zeroline=False, color="white",title="Date", rangeselector=dict(
                buttons=list([
                    dict(count=1,
                         label="1D",
                         step="day",
                         stepmode="backward",),
                    dict(count=7,
                         label="1W",
                         step="day",
                         stepmode="backward"),
                    dict(count=1,
                         label="1M",
                         step="month",
                         stepmode="backward"),
                     dict(count=3,
                         label="3M",
                         step="month",
                         stepmode="backward"),
                    dict(count=6,
                         label="6M",
                         step="month",
                         stepmode="backward"),
                    dict(count=1,
                         label="1Y",
                         step="year",
                         stepmode="backward"),
                    dict(count=5,
                         label="5Y",
                         step="year",
                         stepmode="backward"),
                    dict(label="ALL",
                         step="all")
                ]),activecolor="#ff6e85",
                x=0.11,
                xanchor="left",
                y=1.1,
                yanchor="top"
                ),showline=True, linewidth=2, linecolor='#4b527b'),yaxis=dict(color='white',zeroline=False,gridwidth=0.1, gridcolor='#4b527b',title="Earnings",showline=True, linewidth=2, linecolor='#4b527b'),
                paper_bgcolor='#262b4a',    
                plot_bgcolor='#262b4a',   legend=go.layout.Legend(
            font=dict(
                family="sans-serif",
                size=12,
                color="white"
            )
        )
        )



    data = [trace2, trace3]
    fig = dict(data=data,layout=layout)
    url=py.plot(fig)
    print(url)
    print(url+".embed?autosize=true&modebar=false&link=false")
    url1Text=url
    head, sep, tail = url1Text.partition('.e')
    print("removed embed part", head+".embed?autosize=true&modebar=false&link=false")
    return (head+".embed?autosize=true&modebar=false&link=false")

#profit and loss
@app.route("/profitloss/<ticker_id>", methods=["GET"])
def ProfitLoss(ticker_id):
    print(ticker_id)
    response = requests.get(os.getenv("stocksNodeURL")+"/api/stockspecific/download/allreports/" + ticker_id)
    res = response.json()
    Revenues = []
    EBITDA = []
    CashFromOperatingActivities = []
    NetProfit = []
    GrossProfit = []
    Dates= []
    for val in res['data'][:5]:
        Revenues.append(val['Revenues'])
        EBITDA.append(val['EBITDA'])
        CashFromOperatingActivities.append(val['Cash From Operating Activities'])
        NetProfit.append(val['Net Profit'])
        GrossProfit.append(val['Gross Profit'])
        Dates.append(val['date'])
    ProfitLoss = pd.DataFrame({"date":Dates,"revenue":Revenues,"ebitda":EBITDA,"cfoa":CashFromOperatingActivities,"netprofit":NetProfit,"grossprofit":GrossProfit})
    trace1= go.Bar(
            x= ['Mar 2018','Jul 2018','Sep 2018','Jan 2019','Mar 2019'],
            y= ProfitLoss['revenue'],text='Revenues',name='Revenues', marker_color='#ff6e85'
    )
    trace2= go.Bar(
            x= ['Mar 2018','Jul 2018','Sep 2018','Jan 2019','Mar 2019'],
            y= ProfitLoss['ebitda'],text='EBITDA', name='EBITDA', marker_color='#20d38f'
    )
    trace3= go.Bar(
            x= ['Mar 2018','Jul 2018','Sep 2018','Jan 2019','Mar 2019'],
            y= ProfitLoss['cfoa'],text='Cash From Operating Activities', name='Cash From Operating Activities', marker_color='#465ff3'
    )
    trace4= go.Bar(
            x= ['Mar 2018','Jul 2018','Sep 2018','Jan 2019','Mar 2019'],
            y= ProfitLoss['netprofit'],text='Net Profit', name='Net Profit', marker_color='#f18019'
    )
    trace5= go.Bar(
            x= ['Mar 2018','Jul 2018','Sep 2018','Jan 2019','Mar 2019'],
            y= ProfitLoss['grossprofit'], text='Gross Profit', name='Gross Profit', marker_color='#00ccdd'
    )
    layout=dict(
            title= dict(text='Profit And Loss Chart',font=dict(color="white")),legend=go.layout.Legend(
                font=dict(
                    family="sans-serif",
                    size=12,
                    color="white"
                )
            ),
            barmode='group',
            plot_bgcolor='#262b4a',
            paper_bgcolor='#262b4a',

            xaxis=dict(
                showgrid=False,
                color="white",
                title="Date",
                showline=True,
                linewidth=2,
                linecolor='#4b527b',
                zeroline=False
            ),
            yaxis=dict(
                color='white',showgrid=True, gridwidth=0.1, gridcolor='#4b527b',title="Price",showline=True, linewidth=2, linecolor='#4b527b', zeroline=False)
    )
    data = [trace1, trace2, trace3, trace4, trace5]
    fig = dict(data=data, layout=layout)
    url=py.plot(fig)
    print(url+".embed?autosize=true&modebar=false&link=false")
    url1Text=url
    head, sep, tail = url1Text.partition('.e')
    print("removed embed part", head+".embed?autosize=true&modebar=false&link=false")
    return (head+".embed?autosize=true&modebar=false&link=false")

#assets
@app.route("/assets/<ticker_id>", methods=["GET"])
def Assets(ticker_id):
    print(ticker_id)
    response = requests.get(os.getenv("stocksNodeURL")+"/api/stockspecific/download/allreports/" + ticker_id)
    res = response.json()
    CashCashEquivalentsAndShortTermInvestments = []
    Receivables = []
    OtherShortTermAssets = []
    NetPPAndE = []
    LongTermInvestmentsAndReceivables = []
    Dates= []
    for val in res['data'][:5]:
        CashCashEquivalentsAndShortTermInvestments.append(val['Cash, Cash Equivalents & Short Term Investments'])
        Receivables.append(val['Receivables'])
        OtherShortTermAssets.append(val['Other Short Term Assets'])
        NetPPAndE.append(val['Net PP&E'])
        LongTermInvestmentsAndReceivables.append(val['Long Term Investments & Receivables'])
        Dates.append(val['date'])
    Assets = pd.DataFrame({"date":Dates,"CashCashEquivalentsAndShortTermInvestments":CashCashEquivalentsAndShortTermInvestments,"Receivables":Receivables,"OtherShortTermAssets":OtherShortTermAssets,"NetPPAndE":NetPPAndE,"LongTermInvestmentsAndReceivables":LongTermInvestmentsAndReceivables})
    trace1= go.Bar(
            x= ['Mar 2018','Jul 2018','Sep 2018','Jan 2019','Mar 2019'],
            y= Assets['CashCashEquivalentsAndShortTermInvestments'],text='CCSTI',name='CCSTI', marker_color='#ff6e85'
    )
    trace2= go.Bar(
            x= ['Mar 2018','Jul 2018','Sep 2018','Jan 2019','Mar 2019'],
            y= Assets['Receivables'],text='Receivables', name='Receivables', marker_color='#20d38f'
    )
    trace3= go.Bar(
            x= ['Mar 2018','Jul 2018','Sep 2018','Jan 2019','Mar 2019'],
            y= Assets['OtherShortTermAssets'],text='Other Short Term Assets', name='Other Short Term Assets', marker_color='#465ff3'
    )
    trace4= go.Bar(
            x= ['Mar 2018','Jul 2018','Sep 2018','Jan 2019','Mar 2019'],
            y= Assets['NetPPAndE'],text='Net PP&E', name='Net PP&E', marker_color='#f18019'
    )
    trace5= go.Bar(
            x= ['Mar 2018','Jul 2018','Sep 2018','Jan 2019','Mar 2019'],
            y= Assets['LongTermInvestmentsAndReceivables'], text='LTIR', name='LTIR', marker_color='#00ccdd'
    )
    layout=dict(
            title= dict(text='Assets Chart',font=dict(color="white")),legend=go.layout.Legend(
                font=dict(
                    family="sans-serif",
                    size=12,
                    color="white"
                )
            ),barmode='group',plot_bgcolor='#262b4a',paper_bgcolor='#262b4a',xaxis=dict(showgrid=False, color="white",title="Date", showline=True, linewidth=2, linecolor='#4b527b', zeroline=False),yaxis=dict(color='white',showgrid=True, gridwidth=0.1, gridcolor='#4b527b',title="Price",showline=True, linewidth=2, linecolor='#4b527b', zeroline=False)
    )
    data = [trace1, trace2, trace3, trace4, trace5]
    fig = dict(data=data, layout=layout)
    url=py.plot(fig)
    print(url+".embed?autosize=true&modebar=false&link=false")
    url1Text=url
    head, sep, tail = url1Text.partition('.e')
    print("removed embed part", head+".embed?autosize=true&modebar=false&link=false")
    return (head+".embed?autosize=true&modebar=false&link=false")

#liabilities
@app.route("/liabilities/<ticker_id>", methods=["GET"])
def Liabilities(ticker_id):
    print(ticker_id)
    response = requests.get(os.getenv("stocksNodeURL")+"/api/stockspecific/download/allreports/" + ticker_id)
    res = response.json()
    Shorttermdebt = []
    CurrentLiabilities = []
    LongTermDebt = []
    RetainedEarnings = []
    EquityBeforeMinorities = []
    Dates= []
    for val in res['data'][:5]:
        Shorttermdebt.append(val['Short term debt'])
        EquityBeforeMinorities.append(val['Receivables'])
        CurrentLiabilities.append(val['Current Liabilities'])
        LongTermDebt.append(val['Long Term Debt'])
        RetainedEarnings.append(val['Long Term Investments & Receivables'])
        Dates.append(val['date'])
    Liabilities = pd.DataFrame({"date":Dates,"Shorttermdebt":Shorttermdebt,"CurrentLiabilities":CurrentLiabilities,"LongTermDebt":LongTermDebt,"RetainedEarnings":RetainedEarnings,"EquityBeforeMinorities":EquityBeforeMinorities})
    trace1= go.Bar(
            x= ['Mar 2018','Jul 2018','Sep 2018','Jan 2019','Mar 2019'],
            y= Liabilities['Shorttermdebt'],text='Short term debt',name='Short Term Debt', marker_color='#ff6e85'
    )
    trace2= go.Bar(
            x= ['Mar 2018','Jul 2018','Sep 2018','Jan 2019','Mar 2019'],
            y= Liabilities['CurrentLiabilities'],text='Current Liabilities', name='Current Liabilities', marker_color='#20d38f'
    )
    trace3= go.Bar(
            x= ['Mar 2018','Jul 2018','Sep 2018','Jan 2019','Mar 2019'],
            y= Liabilities['LongTermDebt'],text='Long Term Debt', name='Long Term Debt', marker_color='#f18019'
    )
    trace4= go.Bar(
            x= ['Mar 2018','Jul 2018','Sep 2018','Jan 2019','Mar 2019'],
            y= Liabilities['RetainedEarnings'],text='Retained Earnings', name='Retained Earnings', marker_color='#465ff3'
    )
    trace5= go.Bar(
            x= ['Mar 2018','Jul 2018','Sep 2018','Jan 2019','Mar 2019'],
            y= Liabilities['EquityBeforeMinorities'], text='Equity Before Minorities', name='Equity Before Minorities', marker_color='#00ccdd'
    )
    layout=dict(
            title= dict(text='Liabilities Chart',font=dict(color="white")),legend=go.layout.Legend(
                font=dict(
                    family="sans-serif",
                    size=12,
                    color="white"
                )
            ),barmode='group',plot_bgcolor='#262b4a',paper_bgcolor='#262b4a',xaxis=dict(showgrid=False, color="white",title="Date", showline=True, linewidth=2, linecolor='#4b527b', zeroline=False),yaxis=dict(color='white',showgrid=True, gridwidth=0.1, gridcolor='#4b527b',title="Price",showline=True, linewidth=2, linecolor='#4b527b', zeroline=False)
    )
    data = [trace1, trace2, trace3, trace4, trace5]
    fig = dict(data=data, layout=layout)
    url=py.plot(fig)
    print(url+".embed?autosize=true&modebar=false&link=false")
    url1Text=url
    head, sep, tail = url1Text.partition('.e')
    print("removed embed part", head+".embed?autosize=true&modebar=false&link=false")
    return (head+".embed?autosize=true&modebar=false&link=false")

#cash flow
@app.route("/cashflow/<ticker_id>", methods=["GET"])
def CashFlow(ticker_id):
    print(ticker_id)
    response = requests.get(os.getenv("stocksNodeURL")+"/api/stockspecific/download/allreports/" + ticker_id)
    res = response.json()
    DepreciationAndAmortisation = []
    ChangeinWorkingCapital = []
    NetChangeinPPAndEAndIntangibles = []
    Dividends = []
    CashFromFinancingActivities = []
    Dates= []
    for val in res['data'][:5]:
        DepreciationAndAmortisation.append(val['Depreciation & Amortisation'])
        ChangeinWorkingCapital.append(val['Change in Working Capital'])
        NetChangeinPPAndEAndIntangibles.append(val['Net Change in PP&E & Intangibles'])
        Dividends.append(val['Dividends'])
        CashFromFinancingActivities.append(val['Cash From Financing Activities'])
        Dates.append(val['date'])
    CashFlow = pd.DataFrame({"date":Dates,"DepreciationAndAmortisation":DepreciationAndAmortisation,"ChangeinWorkingCapital":ChangeinWorkingCapital,"NetChangeinPPAndEAndIntangibles":NetChangeinPPAndEAndIntangibles,"Dividends":Dividends,"CashFromFinancingActivities":CashFromFinancingActivities})
    trace1= go.Bar(
            x= ['Mar 2018','Jul 2018','Sep 2018','Jan 2019','Mar 2019'],
            y= CashFlow['DepreciationAndAmortisation'],text='Depreciation & Amortisation',name='Depreciation & Amortisation', marker_color='#ff6e85'
    )
    trace2= go.Bar(
            x= ['Mar 2018','Jul 2018','Sep 2018','Jan 2019','Mar 2019'],
            y= CashFlow['ChangeinWorkingCapital'],text='Change in Working Capital', name='Change in Working Capital', marker_color='#20d38f'
    )
    trace3= go.Bar(
            x= ['Mar 2018','Jul 2018','Sep 2018','Jan 2019','Mar 2019'],
            y= CashFlow['NetChangeinPPAndEAndIntangibles'],text='Net Change in PP&E & Intangibles', name='Net Change in PP&E & Intangibles', marker_color='#465ff3'
    )
    trace4= go.Bar(
            x= ['Mar 2018','Jul 2018','Sep 2018','Jan 2019','Mar 2019'],
            y= CashFlow['Dividends'],text='Dividends', name='Dividends', marker_color='#f18019'
    )
    trace5= go.Bar(
            x= ['Mar 2018','Jul 2018','Sep 2018','Jan 2019','Mar 2019'],
            y= CashFlow['CashFromFinancingActivities'], text='Cash From Financing Activities', name='Cash From Financing Activities', marker_color='#00ccdd'
    )
    layout=dict(
            title= dict(text='Cash Flow Chart',font=dict(color="white")),legend=go.layout.Legend(
                font=dict(
                    family="sans-serif",
                    size=12,
                    color="white"
                )
            ),barmode='group',plot_bgcolor='#262b4a',paper_bgcolor='#262b4a',xaxis=dict(showgrid=False, color="white",title="Date", showline=True, linewidth=2, linecolor='#4b527b', zeroline=False),yaxis=dict(color='white',showgrid=True, gridwidth=0.1, gridcolor='#4b527b',title="Price",showline=True, linewidth=2, linecolor='#4b527b', zeroline=False)
    )
    data = [trace1, trace2, trace3, trace4, trace5]
    fig = dict(data=data, layout=layout)
    url=py.plot(fig)
    print(url+".embed?autosize=true&modebar=false&link=false")
    url1Text=url
    head, sep, tail = url1Text.partition('.e')
    print("removed embed part", head+".embed?autosize=true&modebar=false&link=false")
    return (head+".embed?autosize=true&modebar=false&link=false")


@app.route("/montecarlo/<ticker_name>", methods=["GET"])
def monte_carlo(ticker_name):
    response = requests.get(os.getenv("stocksMonteCarloURL")+"/api/stocks/predict/"+ ticker_name)
    # print(response.json())

    monteCarlo = pd.DataFrame();
    monteCarlo=response.json();

    sliced_arr=[]
    sliced_arr1=[]
    sliced_arr2=[]
    sliced_arr3=[]
    sliced_arr4=[]
    sliced_arr5=[]
    sliced_arr6=[]
    sliced_arr7=[]
    sliced_arr8=[]
    sliced_arr9=[]

    sliced_arr = monteCarlo[0:10]
    sliced_arr1= monteCarlo[10:20]
    sliced_arr2= monteCarlo[20:30]
    sliced_arr3 = monteCarlo[30:40]
    sliced_arr4= monteCarlo[40:50]
    sliced_arr5= monteCarlo[50:60]
    sliced_arr6 = monteCarlo[60:70]
    sliced_arr7= monteCarlo[70:80]
    sliced_arr8= monteCarlo[80:90]
    sliced_arr9 = monteCarlo[90:100]

    trace1= go.Scatter( y=sliced_arr,
                        mode='lines+markers')
    trace2= go.Scatter( y=sliced_arr1,
                        mode='lines+markers')
    trace3= go.Scatter( y=sliced_arr2,
                        mode='lines+markers')
    trace4= go.Scatter( y=sliced_arr3,
                        mode='lines+markers')
    trace5= go.Scatter( y=sliced_arr4,
                        mode='lines+markers')
    trace6= go.Scatter( y=sliced_arr5,
                        mode='lines+markers')
    trace7= go.Scatter( y=sliced_arr6,
                        mode='lines+markers')
    trace8= go.Scatter( y=sliced_arr7,
                        mode='lines+markers')
    trace9= go.Scatter( y=sliced_arr8,
                        mode='lines+markers')
    trace10= go.Scatter( y=sliced_arr9,
                        mode='lines+markers')


    data=[trace1,trace2,trace3,trace4,trace5,trace6,trace7,trace8,trace9,trace10]
    layout=dict(
            showlegend=False,
            title= dict(text='Prediction of Stock Price',
                        font=dict(color="white")
                        ),
            xaxis=dict(showgrid=False, 
                       color="white",
                       title="Day",
                       linewidth=2, linecolor='#4b527b',zeroline=False),
            yaxis=dict(color='white',showgrid=True, gridwidth=0.1,
               gridcolor='#4b527b',title="Stock Price", linewidth=2, linecolor='#4b527b'),
            paper_bgcolor='#262b4a',    
            plot_bgcolor='#262b4a'
    )
    fig=dict(data=data,layout=layout)

    
# py.plot(fig)
    url =py.plot(fig)
    print(url+".embed?autosize=true&modebar=false&link=false")
    url1Text=url
    head, sep, tail = url1Text.partition('.e')
    print("removed embed part", head+".embed?autosize=true&modebar=false&link=false")
    return (head+".embed?autosize=true&modebar=false&link=false")

@app.route("/ohlc/<ticker_name>", methods=["GET"])
def get_ohlc_graph(ticker_name):
    indices = pd.DataFrame(list(star.find({"ticker_name":ticker_name},{"ticker_dates"})))
    dates=[]
    opening=[]
    high=[]
    low=[]
    close=[]
    volume=[]
    data=[]


    for i in indices.ticker_dates.item():
        dates.append(i["date"])
        opening.append(i["opening"])
        high.append(i["high"])
        low.append(i["low"])
        close.append(i["closing"])
        volume.append(i["volume"])
    


# indexDate = pd.DataFrame(dates)
# indexOpen = pd.DataFrame(open)
    dft20 = pd.DataFrame({"Date":dates,"Open":opening,"Close":close,"Low":low,"High":high,"Volume":volume})
    print(dft20);


    layout = dict(
#     title='Time series with range slider and selectors',
#         yaxis=dict(color='white',showgrid=True, gridwidth=0.1, gridcolor='#4b527b',title="Share Price",showline=True, linewidth=2, linecolor='#4b527b'),

    xaxis=dict(
        rangeselector=dict(
            buttons=list([
                 dict(count=1,
                     label="1d",
                     step="day",
                     stepmode="backward"),
                  dict(count=7,
                     label="1w",
                     step="day",
                     stepmode="backward"),
                dict(count=1,
                     label="1m",
                     step="month",
                     stepmode="backward"),
                    dict(count=3,
                     label="3m",
                     step="month",
                     stepmode="backward"),
                dict(count=6,
                     label="6m",
                     step="month",
                     stepmode="backward"),
                dict(count=1,
                     label="YTD",
                     step="year",
                     stepmode="todate"),
                dict(count=1,
                     label="1y",
                     step="year",
                     stepmode="backward"),
                 dict(count=5,
                     label="5y",
                     step="year",
                     stepmode="backward"),
                dict(step="all")
            ]),activecolor="#ff6e85",
            x=0.11,
            xanchor="left",
            y=1.1,
            yanchor="top"
        ),
        rangeslider=dict(
            visible = True
            
        ),
        type='date',showgrid=False,title="Date",showline=False, linewidth=2, linecolor='#4b527b',color='white'
    ),yaxis=dict(color='white',showgrid=True, gridwidth=0.1, gridcolor='#4b527b',title="Share Price",showline=True),
    paper_bgcolor='#262b4a',    
    plot_bgcolor='#262b4a'
    
)
# 87    )

    fig=go.Figure(data=go.Ohlc(x=dft20['Date'],
                    open=dft20['Open'],
                    high=dft20['High'],
                    low=dft20['Low'],
                    close=dft20['Close']),layout=layout)
    url =py.plot(fig)
<<<<<<< HEAD
    print(url+".embed?autosize=true&modebar=false&link=false")
    url1Text=url
    head, sep, tail = url1Text.partition('.e')
    print("removed embed part", head+".embed?autosize=true&modebar=false&link=false")
    return (head+".embed?autosize=true&modebar=false&link=false")
=======
    print(url)
    print(url)
#     return url
    urlText=url
    #seperating every content after.e in order to remove the .embeded part from the url as it is not applicable if we pass it in the get_embed function
    head, sep, tail = urlText.partition('.e')
    print("removed embed part",head)
    #passing the head which doesnot consist of .embed part and it will generate the iframe code for the graph
    
   
    #sends the data in object with key data and value coming from database
    return (url)
>>>>>>> 442631bbcf22e3233d36b5ca79ee9d4c78edc593


if __name__ == '__main__':
    app.run(debug=False,port=5003)
