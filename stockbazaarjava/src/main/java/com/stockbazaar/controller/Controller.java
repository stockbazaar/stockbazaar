package com.stockbazaar.controller;

import com.stockbazaar.model.stocks_data_2;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.beans.factory.annotation.Value;

@CrossOrigin(origins = "*")
@RestController
@org.springframework.transaction.annotation.Transactional
@RequestMapping("/api/stocks")

public class Controller {
    // @Autowired
    // Environment env;
    public static final String DB_NAME = "stockbazaar";
    public static final String stocks_data_2_COLLECTION = "stocks_data_2";
    @Value("${stocksMongoURL}")
    public String MONGO_URI;
    // public static final String MONGO_HOST = env.getProperty("mongoIP");
    // public static final int MONGO_PORT = 27017;

    // [kavita] route for getting predicted for a given ticker id
    @GetMapping("/predict/{ticker_id}")
    public List<Double> monteCarlo(@PathVariable int ticker_id) throws UnknownHostException {
        // creates a mongo client connection to the specified host and port
        MongoClientURI uri = new MongoClientURI(MONGO_URI);

        MongoClient mongoClient = new MongoClient(uri);
        // MongoClient mongo = new MongoClient(MONGO_HOST, MONGO_PORT);
        // creates template to perform operations on the specified database
        MongoOperations mongoOps = new MongoTemplate(mongoClient, DB_NAME);
        // query the specified collection with ticker id provided in the path variable
        // with return
        // type as stocks_data_2.class
        stocks_data_2 p1 = mongoOps.findOne(new Query(Criteria.where("ticker_id").is(ticker_id)), stocks_data_2.class,
                stocks_data_2_COLLECTION);

        List<Double> arr = new ArrayList<>();
        List<Double> change = new ArrayList<>();
        List<Double> normal = new ArrayList<>();
        List<Double> price = new ArrayList<>();
        List<Double> price1 = new ArrayList<>();
        List<Double> predicted_price = new ArrayList<>();
        Integer num_simulations = 10, num_days = 10;
        double last_price = 0.0, sum = 0.0, mean = 0.0, standardDeviation = 0.0;
        int count = 0;

        // stores share prices in a arraylist of type double for all dates
        for (HashMap<?, ?> item : p1.getTicker_dates()) {

            if (item.containsKey("Share Price")) {
                if (item.get("Share Price").getClass().getName() == "java.lang.Integer") {
                    arr.add(((Integer) item.get("Share Price")).doubleValue());

                } else {
                    arr.add(((Double) item.get("Share Price")));
                }
            }

        }
        // takes last price as the share price of the last date
        last_price = arr.get(arr.size() - 1);

        // calculates change in share price w.r.t share price of previous day
        for (int i = 1; i < arr.size(); i++) {
            change.add(arr.get(i) - arr.get(i - 1));
        }

        int length = change.size();
        // sum of change in price
        for (double num : change) {
            sum += num;
        }
        // calculates mean for change in share price
        mean = sum / length;
        // calculate standard deviation for the share price
        for (double num : change) {
            standardDeviation += Math.pow(num - mean, 2);
        }
        // calculates normal distribution for the change in share price
        for (int i = 0; i < arr.size(); i++) {
            normal.add((arr.get(i) - mean) / standardDeviation);
        }
        // predicts share price
        for (int i = 0; i < num_simulations; i++) {
            count = 0;
            Integer ran = (int) ((Math.random() * ((normal.size() - 1 - 0) + 1)) + 0);
            price.add(last_price * (1 + normal.get(ran)));
            price1.add(price.get(i));

            for (int j = 0; j < num_days; j++) {
                Integer ran1 = (int) ((Math.random() * ((normal.size() - 1 - 0) + 1)) + 0);
                predicted_price.add(price1.get(count) * ((1 + normal.get(ran1))));

            }
        }
        // closes the mongo client connection
        mongoClient.close();
        return predicted_price;

    }

}